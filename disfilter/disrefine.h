#ifndef _DISREFINE_H
#define _DISREFINE_H

#include "Msg.h"

#define DISTIMEWINLEN  6

#define EFFECTIVE_BEACON_NUM 9


typedef enum{
	    OK=1,
		doEKF=2,
		Bad=3,
		INIT=4	
}DisStat_t;


typedef struct {
    uint8_t BeaconID;
	volatile uint32_t TimeStamp[DISTIMEWINLEN];
	volatile uint16_t Distance[DISTIMEWINLEN];
	volatile uint16_t DisSum;
        volatile uint16_t TimeWid[DISTIMEWINLEN];
	volatile uint16_t TimeWidSum;
	volatile uint8_t  CurPtr;
	volatile uint16_t FullTimes;
} UsDisTab_t;



//public fun

void upDateDisMeasurement(Msg_t*);
DisStat_t checkNewDisMeasurement(Msg_t*);







//uint32_t getTimeWidfromDis(float Dis);







#endif