#include "stm324xg_eval.h"

#include "Msg.h"
#include "arm_math.h"
#include "math.h"
#include "disrefine.h"


extern UsDisTab_t UsDisList[EFFECTIVE_BEACON_NUM];


DisStat_t checkNewDisMeasurement(Msg_t* pMsg){
		uint16_t val1,val2;
		uint32_t val3;
		uint8_t idx=pMsg->ID-1;
		uint8_t DisTimeWidIdx=UsDisList[idx].CurPtr-1;
		uint16_t newDis=(pMsg->risingEdge[0]*34/250);

		uint32_t newTimeStamp=pMsg->TimerTick;

		if(DisTimeWidIdx>=DISTIMEWINLEN)DisTimeWidIdx=DISTIMEWINLEN-1;// overflow check, CurPtr-1=255 when CurPtr=0; 
		val1=abs(newDis-UsDisList[idx].Distance[DisTimeWidIdx]);
		val3=newTimeStamp-UsDisList[idx].TimeStamp[DisTimeWidIdx];

		if(UsDisList[idx].FullTimes>0){
			if(val1>200)return Bad;
			if(val1>30){ // greater than 1m
				if(val3<5000){ // 250 per sec, 100~0.4sec
					return doEKF;
				}else{
					return OK;
					}
				}else{return OK;}
		}else{
			return INIT;
			}
}


void upDateDisMeasurement(Msg_t* pMsg){

		uint8_t idx=pMsg->ID-1;
		uint8_t DisTimeWidIdx=UsDisList[idx].CurPtr;
		uint16_t distCM=(pMsg->risingEdge[0]*34/250);

		
		if(pMsg->ID<EFFECTIVE_BEACON_NUM){
			
			UsDisList[idx].BeaconID=idx+1;


			UsDisList[idx].TimeStamp[DisTimeWidIdx]=pMsg->TimerTick;
			
			//UsDisList[idx].DisSum-=UsDisList[idx].Distance[DisTimeWidIdx];
			//UsDisList[idx].DisSum+=distCM;		
			UsDisList[idx].Distance[DisTimeWidIdx]=distCM;

			//UsDisList[idx].TimeWidSum-=UsDisList[idx].TimeWid[DisTimeWidIdx];
			//UsDisList[idx].TimeWidSum+=pMsg->fallingEdge[0]-pMsg->risingEdge[0];
			UsDisList[idx].TimeWid[DisTimeWidIdx]=pMsg->fallingEdge[0]-pMsg->risingEdge[0];
			
			//meanval1=UsDisList[idx].DisSum/DISTIMEWINLEN;
			//meanval1=abs(meanval1-distCM);//
			//meanval2=UsDisList[idx].TimeWidSum/DISTIMEWINLEN;
			//meanval2=abs(meanval2-UsDisList[idx].TimeWid[DisTimeWidIdx]);//
			DisTimeWidIdx++;			
			if(DisTimeWidIdx>=DISTIMEWINLEN){
				DisTimeWidIdx=0;
				UsDisList[idx].FullTimes++;
				}
			UsDisList[idx].CurPtr=DisTimeWidIdx;				
			}
		
		//printf("###Dis=%3.2f,meanDis=%d,meanTW=%d\n",distCM,meanval1,meanval2);
}



















/*
uint32_t getTimeWidfromDis(float Dis){

		
		float y;
		y=-0.007*Dis*Dis+1.6104*Dis+3138.6;
		
		return (uint32_t) y;
}
*/
