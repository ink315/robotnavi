/**
 ******************************************************************************
 * @file    Project/STM32F4xx_StdPeriph_Templates/main.c
 * @author  MCD Application Team
 * @version V1.1.0
 * @date    18-January-2013
 * @brief   Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
 *
 * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        http://www.st.com/software_license_agreement_liberty_v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm324xg_eval.h"
#include "stm32f4xx_conf.h"
#include <stdio.h>
#include "stm32f4xx_it.h"
#include "MPU6050.h"
#include "yaEkf6.hh"
#include <stdlib.h>
//#include <string.h>
#include "main.hh"
#include "Msg.hh"
#include "arm_math.h"
#include "math.h"
#include "attitudeSolution.c"
#include "linefitting.h"   
/*
   using namespace std;
   using std::size_t;
   using namespace techsoft;

   using techsoft::epsilon;
   using techsoft::isVecEq;
   */
//using std::exception;

#define arm_matrix_instance_f32 arm_matrix_instance_f32

/** @addtogroup STM32F4xx_StdPeriph_Templates
 * @{
 */

/* Private typedef -----------------------------------------------------------*/
typedef union uFloatInt{
    uint32_t ui;
    float fl;
} floatInt;

typedef union uMsgBuf{
    Msg_t msg;
    uint8_t buf[48];
} MsgBuf;

typedef union uPointMsgBuf{
    Point_msg_t msg;
    uint8_t buf[17];
} PointMsgBuf;


/* Private define ------------------------------------------------------------*/
#define TX_BUF_LEN 256
#define RX_BUF_LEN 256
#define DATA_BUFFER_SIZE 512
#define __IO volatile
#define SZ_BUF_LEN 100
#define SMSG_BUF_LEN 50
#define UART_BUF_SIZE 100
#define MAX_MSG_CNT 10
#define RANGE_FILTER_THRESHOLD 40


#if defined (USE_STM324xG_EVAL)
#define MESSAGE1   "     STM32F40xx     "
#define MESSAGE2   " Device running on  "
#define MESSAGE3   "   STM324xG-EVAL    "

#else /* USE_STM324x7I_EVAL */
#define MESSAGE1   "     STM32F427x     "
#define MESSAGE2   " Device running on  "
#define MESSAGE3   "   STM324x7I-EVAL   "
#endif

#define WINDOW_SIZE 7
/* Private macro -------------------------------------------------------------*/
uint8_t range_data[DATA_BUFFER_SIZE];
//CircularBuffer rangeBuffer(DATA_BUFFER_SIZE,range_data);

/* Private variables ---------------------------------------------------------*/
uint8_t FlagRxUart1;
uint8_t FlagRxUart2;
volatile uint8_t RxBufferOfUart1[BUFFER_SIZE];

volatile uint8_t ID,Seq,pktReady = 0,wrongPkt = 0;
volatile uint8_t flushToFlash,wrongCoordPkt = 0,CoordPktReady =0,offsetPktReady =0 ,ADCReady =0;
volatile uint8_t IMUReady;
float distCM,beaconX,beaconY,beaconZ,thisOffset;
volatile uint16_t firstRisingEdge;
/*public valurable-------------------------------------------------------------*/
float beaconCoordinFlash[MAX_NUM_BEACON*4];
__IO uint8_t numBeacon ;
float offset[];
__IO uint8_t numBeaconToWrite;
float beaconCoord[MAX_NUM_BEACON*3];
float offset[MAX_NUM_BEACON];//{16,36,45,45,45,45,0,0,0,0,0,0};
sRecvStage curStage,uart1Stage;
/* Private variables ---------------------------------------------------------*/
static __IO uint32_t uwTimingDelay;
RCC_ClocksTypeDef    RCC_Clocks;
TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
TIM_OCInitTypeDef  TIM_OCInitStructure;
//volatile uint16_t CCR1_Val = 40961;
/* Private function prototypes -----------------------------------------------*/
static void Delay(__IO uint32_t nTime);
uint8_t lastSeq;
float distV[MAX_NUM_BEACON];
uint16_t edgeV[MAX_NUM_BEACON];
extern volatile uint32_t timeStamp;

__IO uint16_t rawUSValue[ADC_VALUE_LEN];
float lastPos[2][3] ={{0,0,0},{0,0,0}};
float targetPos[] ={100.0f,100.0f,100.0f};
#define MAX_SPI_BUF_LEN 100
#define MAX_USART_BUF_LEN 40
__IO uint8_t spiBuf[2][MAX_SPI_BUF_LEN];
__IO uint8_t *spiInBuf;
__IO uint8_t *spiOutBuf;
__IO uint16_t spiInBufLen,spiOutBufLen;


__IO uint8_t USARTBuf[2][MAX_USART_BUF_LEN];
__IO uint8_t *USARTInBuf;
__IO uint8_t *USARTOutBuf;
__IO uint8_t USARTInBufLen,USARTOutBufLen;
__IO uint8_t USARTpktReady;
__IO uint8_t badSPI;
__IO floatInt converter;


arm_matrix_instance_f32 xl;
float32_t xlm[3];
float32_t residue[WINDOW_SIZE];
uint32_t v;
uint8_t p;
uint8_t EKFInit = 0;
int simID =0;
int simCount = 0;
float32_t threshold = 300; 
float32_t JUMPTHRESHOLD = 40;
uint8_t MOVESIZE = 1; 
uint32_t ttime = 0;
uint32_t lasttime = 0;
/* Private functions ---------------------------------------------------------*/
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#endif

#ifdef _SIMULATOR_
#define PUTCHAR_PROTOTYPE int hello(int ch, FILE *f)
#else
//#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

void updatePositionBuffer(float x, float y);
void updateYawBuffer(float yaw);

uint32_t flashStartSectorAddr = 0;

float Xdata[10] = {0,0,0,0,0,0,0,0,0,0};
float Ydata[10] = {0,0,0,0,0,0,0,0,0,0};
float Yawbuffer[10] ={0,0,0,0,0,0,0,0,0,0};
int yawbufferindex = 0;
int yawbufferfullindicator = 0;
int yawbuffersize = 3; 
int bufferindex = 0;
int fullindicator = 0;
int buffersize = 6;

float slope;
float intercept;
float fitresidue;
float fityaw;
float linearyaw= 0.0;
int yawmeasured = 0;
float32_t att_euler[3],acc[3],pal[3];
int main(void){
 // while(1){};
  


    
    uint16_t loop;
    uint8_t beaconID[MAX_NUM_BEACON];
    for(loop=0;loop<0xffff;loop++){
        uint16_t loop2;
        for(loop2=0;loop2<0xf;loop2++){
            asm("nop");
        }
    }
    curStage = WAITING;
    uart1Stage = WAITING;
    spiInBufLen = 0;
    spiOutBufLen = 0;
    STM_EVAL_LEDInit(LED1);
    STM_EVAL_LEDInit(LED2);
    arm_mat_init_f32(&xl,3,1,xlm);
    //load beaconCoord from the flash
    /* Enable the flash control register access */

    uint32_t ReadAddr = ADDR_FLASH_SECTOR_4;
    numBeacon = (uint8_t)(*(volatile uint32_t*)ReadAddr);
    ReadAddr += 4;
    if(numBeacon!=255){
    uint8_t i,j;
    for(i=0;i<numBeacon;i++){
        beaconID[i] = (uint8_t)(*(volatile uint32_t*)ReadAddr);
        ReadAddr += 4;
        for(j=0;j<3;j++){
            converter.ui=*(volatile uint32_t*)ReadAddr;
            beaconCoord[i*3+j] = converter.fl;
            ReadAddr += 4;
        }
        converter.ui = *(volatile uint32_t*)ReadAddr;
        offset[i]=converter.fl;
        ReadAddr += 4;
    }
    }else{
      numBeacon = 0;
    }
    numBeaconToWrite = 0;
    SPI_Config();
    SPI_Cmd(EVAL_SPI,ENABLE);
    SPI_I2S_ITConfig(EVAL_SPI,SPI_I2S_IT_RXNE,ENABLE);
    spiInBuf =&(spiBuf[0][0]);
    spiOutBuf =&(spiBuf[1][0]) ;
    USARTInBuf = &(USARTBuf[0][0]);
    USARTOutBuf = &(USARTBuf[1][0]);
    //memset((void*)spiBuf,0,2*MAX_SPI_BUF_LEN);
    /*
       spiInBuf = spiBuf[0];
       spiOutBuf = spiBuf[1];
       */
    SPI2_Init();
    if(SysTick_Config(SystemCoreClock/1000)){
	while(1);
    }
#ifdef ENABLE_ADC
    usADCInit();
    
#endif
    USART_Config(COM1);
    USART_NVIC_Config(COM1);
    USART_ITConfig(EVAL_COM1,USART_IT_RXNE,ENABLE);
#ifdef DEBUG_FLASH
    printf("numBeaconInFlash = %u\n\r",numBeacon);
    for(int k=0;k<numBeacon;k++){
        printf("%u:%.1f %.1f %.1f %.1f\n\r",beaconID[k],beaconCoord[k*3],beaconCoord[k*3+1],beaconCoord[k*3+2],offset[k]);
    }
#endif
    
    int16_t abias[3] ={0,-250,250};
    int16_t wbias[3] ={-50,390,100};
    int16_t hbias[3] ={40, -30, -15};

/*    float ab[9][3]=
                {
                  {60,180,285},
                  {-120,180,285},
                  {0,0,285},
                  {120,0,285},
                  {-120,0,285},
                  {0,-180,285},
                  {220,183,245},
                  {196,27,245},
                  {280,105,245}
                };
    */

      float ab[9][3]=
            {
                {0,172,245},
                {0,0,245},
                {78,138,245},
                {77,60,245},
                {154,172,245},
                {144,0,245},
                {220,183,245},
                {196,27,245},
                {280,105,245}
            };

    float o[9]={-0,-0,-0,-0,-0,-0,-0,-0,-0};

    volatile unsigned char szBuffer[SZ_BUF_LEN];
    volatile uint8_t sMsgBuffer[SMSG_BUF_LEN];
    volatile uint16_t sBufferSize = 0;
    volatile uint8_t hasNew = 0;
    //volatile uint8_t IMUReady = 0;

    volatile uint8_t coordBuffer[SZ_BUF_LEN];
    volatile uint16_t coordBufLen = 0;

    printf("LBS ver 1.0\n\r");

    extern int confidence;
    extern int N;
    extern int windex;

    extern int NBeacon;
    extern int confidence;
    extern arm_matrix_instance_f32 z;
    extern arm_matrix_instance_f32 R;
    extern arm_matrix_instance_f32 Q;
    extern arm_matrix_instance_f32 P;
    extern arm_matrix_instance_f32 X;


    float step=3.0;
    uint8_t roundCount =0;
    
    int16_t rawData[6];

    pktReady = 0;
#ifdef ENABLE_IMU
    uint16_t PrescalerValue = (uint16_t) ((SystemCoreClock / 2) / 6000000) - 1;
#endif


    //init the MPU6050
#ifdef ENABLE_IMU
    MPU6050_I2C_Init();
    if(!MPU6050_TestConnection() ){
        while(1);//connection failed
    }
    MPU6050_Initialize();
    printf("IMU ready\n\r");
#endif
    initBeacons(numBeacon,beaconCoord,3);
    initTimer();

    lastSeq = 0;
    uint16_t cumulatedPos =0,filted_cnt =0;
    for(int k = 0;k<MAX_NUM_BEACON;k++){
        edgeV[k] = 0;
        distV[k] = -1.0f;
    }
    STM_EVAL_LEDOn(LED1);
    STM_EVAL_LEDOn(LED2);

    MsgBuf thisMsgBuf;
    PointMsgBuf thisPointMsgBuf;
    badSPI = 0;

    setBeaconCoordinates(9, ab);
    setNumberOfBeacons(9);
    setRangeOffset(9, o);
    attitudeInit();
    setImuBiasOffset(abias, wbias, hbias);
    /* Infinite loop */
    while (1){
     //   float ADC_ConvertedValue=(float)rawUSValue[0]/4096*3.3;
      //  printf("%.2f\n\r",ADC_ConvertedValue);
       /* uint8_t outBuf[10];
        outBuf[0]=0x01;
        outBuf[1]=0x02;
        SPI2_Putsn((char*)outBuf,10);*/
        //handle the rangebuffer
      IMUReady=1;

        if(IMUReady){
            IMUReady = 0;

            MPU6050_GetRawAccelGyro(rawData);
            if(timeStamp - lasttime >10 || timeStamp - lasttime <500)
            {
                attitudeSolution(rawData, 0);
                lasttime = timeStamp;
            }
            //printf("time %d", timeStamp);
            //TODO
        }
        if(badSPI){
            badSPI = 0;
            SPI_Cmd(EVAL_SPI,DISABLE);
            SPI_Config();
            SPI_Cmd(EVAL_SPI,ENABLE);
            SPI_I2S_ITConfig(EVAL_SPI,SPI_I2S_IT_RXNE,ENABLE);
        }
        if(ADCReady){
            uint32_t USSum = 0;
            uint16_t i;
            for(i= 0;i<ADC_VALUE_LEN;i++){
                USSum += rawUSValue[i];
            }
            USSum/= i;
            if(USSum >3000){
                //if capture us intensity then toggle the led;
                //TODO replace the led toggle with sync algorthm
                //TODO fifo quee in the usar
                //STM_EVAL_LEDToggle(LED2);
            }
        }
#ifdef _SIMULATOR_
        if(1)
        {
#else
        if(pktReady){
            pktReady = 0;
            uint16_t i,j;
            j=0;
            for(i=0;i<spiOutBufLen-1;i++){
                if(spiOutBuf[i]==0x7E)
                    continue;
                else if(spiOutBuf[i]==0x7D&&spiOutBuf[i+1]==0x5D){
                    thisMsgBuf.buf[j++] = 0x7D;
                    i++;
                }else if(spiOutBuf[i]==0x7D&&spiOutBuf[i+1]==0x5E){
                    thisMsgBuf.buf[j++] = 0x7E;
                    i++;
                }else{
                    thisMsgBuf.buf[j++] = spiOutBuf[i];
                }
            }
            if(j!=sizeof(Msg_t)){
                printf("wrong spi pkt %u\n\r",j);
                continue;
            }
#endif            
            //distCM = (thisMsgBuf.msg.risingEdge[0]*34/250)+offset[thisMsgBuf.msg.ID-1];
            
#ifdef _SIMULATOR_
            
            targetPos[0] = (int)(targetPos[0]+2)%500;
            targetPos[1] = (int)(targetPos[1]+2)%500;
            if(timeStamp - lasttime >1)
            {
              simID = (simID +1) %numBeacon;              // 1-9;
              simCount = simCount + 1;
              if(simID == 0)
                simID = numBeacon;
              //if(rand()%100 <10 )
              //{
               // printf("large error input\n");
              //  distCM = calDisFromBeacon(simID-1, targetPos) + (float)(rand()%200)-100.0 ;
              //}
              //else
                distCM = calDisFromBeacon(simID-1, targetPos) + (float)(rand()%21)-10.0 ;
            printf("Pos:x=%.2f\ty=%.2f\tz=%.2f\t\n\r", targetPos[0], targetPos[1], targetPos[2]);
            
            //updateTargetPos(targetPos, simCount);
            ID = simID;                           //1-9
            lasttime = timeStamp;
#else
            distCM = (thisMsgBuf.msg.risingEdge[0]*34/250);
            ID = thisMsgBuf.msg.ID;
            //printf("%u,%.2fcm\n\r",ID,distCM);
#endif            
            

#ifdef DEBUG_LSQ_DIS_TAB
            if(Seq!=lastSeq){
                printf("%03u ",lastSeq);
                for(int k =1;k<MAX_NUM_BEACON;k++){
                    if(distV[k]>0.0f){
                        sprintf(txBuf,"%02u:%04.2f ",k,distV[k]);
                        printf(txBuf,"%02u:%06u ",k,edgeV[k]);
                        print(txBuf);
                    }else{
                        print("          ");
                    }
                }
                print("\n\r");
            }
            lastSeq = Seq;
            edgeV[ID] = firstRisingEdge;
            distV[ID] = distCM;

#endif

            if(distCM >35 && distCM <1000){
                STM_EVAL_LEDToggle(LED1);
                distBufferFilter(&distCM, ttime, JUMPTHRESHOLD, MOVESIZE, ID-1); 
             //   printf("%u,%.2fcm,%u\n\r",ID,distCM,thisMsgBuf.msg.TimerTick);

                z.pData[0]=distCM;
                updateWindows(ID-1,distCM, step);
                //CheckSingularGenerateVirtualDis();
                
                
                if(windex == WINDOW_SIZE && EKFInit ==0){
                    //roundCount++;
                    NonLinearLSQ(&xl, residue);
                    initEKF(&xl);  //initialize EKF
                    EKFInit = 1;
                }
                else if(windex == WINDOW_SIZE) {
                    LSQwithNLOS(&xl, residue, threshold, &v, &p); 
                    if(p == 0)
                        removeFromWindows(v);
                    else {
                        updateEKF(&z, ID-1, step);

//                    updatePos(&xl);
                    float ekf_x = X.pData[0];//p(0,0);
                    float ekf_y = X.pData[1];//(1,0);
                    float ekf_z = X.pData[2];//(2,0);
                   // printf("location calculated %f %f %f \n", ekf_x, ekf_y, ekf_z);

                    updatePositionBuffer(ekf_x, ekf_y);
                    if(fullindicator == 1)
                    {
                          float yawdiffer = 0;
                          Line_Fit(&slope, &intercept, &fitresidue, &fityaw, Xdata, Ydata, 10);
                          updateYawBuffer(fityaw);
                          if(yawbufferfullindicator == 1)
                          {
                            yawdiffer = (fabs(Yawbuffer[0]-Yawbuffer[1]) + fabs(Yawbuffer[1]-Yawbuffer[2]) +fabs(Yawbuffer[0]-Yawbuffer[2]) )/3;
                          }
                          float xGap = fabs(Xdata[0]-Xdata[buffersize-1]);
                          float yGap = fabs(Ydata[0]-Ydata[buffersize-1]);
                          if(fitresidue <180 && (xGap > 50 || yGap >50) && yawdiffer < 0.1)
                          {
                            //linearyaw = fityaw;
                            //yawmeasured = 1;
                            //printf("linearYaw= %f, yawmeasured = %d, xGap=%f, yGap=%f \n", linearyaw, yawmeasured, xGap, yGap);
                          }
                            
                    }

                    //printf("call imu update \n");
#ifdef LSQ_RAW_RESULT
                    uint8_t confidence = 128;
                    rawPrint(0,(float)ekf_x,(float)ekf_y,(float)ekf_z,confidence);

#endif
#ifdef LSQ_ASCII_RESULT
//                    printf("lsq:x= %.2f\ty= %.2f\tz=%.2f\t\n\r", lsq_x,lsq_y,lsq_z);
           //         printf("ekf:x= %.2f\ty= %.2f\tz=%.2f\t\n\r", ekf_x,ekf_y,ekf_z);
                    printf("1 %8.2f %8.2f %8.2f %8.2f %8.2f %d %8.2f %8.2f %8.2f\n",ekf_x,ekf_y,ekf_z,0.0,0.0,timeStamp,att_euler[0],att_euler[1],att_euler[2]);
#endif
                }
              }
            }
#ifdef _SIMULATOR_
        }
#endif
        }
        if(USARTpktReady){
            USARTpktReady = 0;
            uint8_t i,j=0;
            for(i=0;i<USARTOutBufLen-1;i++){
                if(USARTOutBuf[i]==0x7E)
                    continue;
                else if(USARTOutBuf[i]==0x7D && USARTOutBuf[i+1]==0x5D){
                    thisPointMsgBuf.buf[j++] = 0x7D;
                    i++;
                }else if(USARTOutBuf[i]==0x7D && USARTOutBuf[i+1]==0x5E){
                    thisPointMsgBuf.buf[j++] = 0x7E;
                    i++;
                }else{
                    thisPointMsgBuf.buf[j++] = USARTOutBuf[i];
                }
            }
            if(j!=sizeof(Point_msg_t)){
                printf("wrong usart pkt %u %u\n\r",j,sizeof(Point_msg_t));
                continue;
            }

            if(thisPointMsgBuf.msg.ID!=255){
                beaconID[numBeaconToWrite] = thisPointMsgBuf.msg.ID;
                offset[numBeaconToWrite] = thisPointMsgBuf.msg.offset;
                uint8_t ptr = numBeaconToWrite*3;
                beaconCoord[ptr++] = thisPointMsgBuf.msg.x;
                beaconCoord[ptr++] = thisPointMsgBuf.msg.y;
                beaconCoord[ptr++] = thisPointMsgBuf.msg.z;
                numBeaconToWrite++;
            }
            else if(thisPointMsgBuf.msg.ID==255 && numBeaconToWrite>0){
                printf("numBeaconToWrite=%u\n" , numBeaconToWrite);
                uint8_t i,j;
                for(i=0;i<numBeaconToWrite;i++){
                    printf("%u:%.1f %.1f %.1f %.1f\n\r",beaconID[i],beaconCoord[i*3],beaconCoord[i*3+1],beaconCoord[i*3+2],offset[i]);
                }
                FLASH_Unlock();
                FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR|FLASH_FLAG_PGSERR);
                if(FLASH_EraseSector(FLASH_Sector_4,VoltageRange_3)!=FLASH_COMPLETE){
                    while(1){}
                }
                uint32_t writeAddr = ADDR_FLASH_SECTOR_4;
                //write numbeacon
                if(FLASH_ProgramWord(writeAddr,(uint32_t)numBeaconToWrite)!=FLASH_COMPLETE){
                    while(1){}
                }
                writeAddr+= 4;
                for(i=0;i<numBeaconToWrite;i++){
                    //write ID
                    if(FLASH_ProgramWord(writeAddr,(uint32_t)beaconID[i])!=FLASH_COMPLETE){
                        while(1){}
                    }
                    writeAddr+= 4;
                    //write coord
                    for(j=0;j<3;j++){
                        converter.fl=beaconCoord[i*3+j];
                        if(FLASH_ProgramWord(writeAddr,converter.ui)!=FLASH_COMPLETE){
                            while(1){}
                        }
                        writeAddr+= 4;
                    }
                    converter.fl=offset[i];
                    if(FLASH_ProgramWord(writeAddr,converter.ui)!=FLASH_COMPLETE){
                        while(1){}
                    }
                    writeAddr+= 4;
                }
                numBeacon = numBeaconToWrite;
                numBeaconToWrite = 0;
                FLASH_Lock();
            }
        }
    }//finish infinit loop
}

/**
 * @brief  Inserts a delay time.
 * @param  nTime: specifies the delay time length, in milliseconds.
 * @retval None
 */
void Delay(__IO uint32_t nTime) {
    uwTimingDelay = nTime;
    while(uwTimingDelay != 0);
}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void){
    if (uwTimingDelay != 0x00){
        uwTimingDelay--;
    }
}

void updatePositionBuffer(float x, float y)
{
  if(bufferindex < buffersize-1)
  {  
    Xdata[bufferindex] = x;
    Ydata[bufferindex] = y;
    bufferindex = (bufferindex + 1)%buffersize;
  }
  else
  {
    fullindicator = 1;
    for(int i=0; i< buffersize-1; i++)
    {
       Xdata[i] = Xdata[i+1];
       Ydata[i] = Ydata[i+1];
    }
    Xdata[bufferindex] = x;
    Ydata[bufferindex] = y;
  }
    
}

void updateYawBuffer(float yaw)
{
  if(yawbufferindex < yawbuffersize-1)
  {  
    Yawbuffer[bufferindex] = yaw;
    yawbufferindex = (yawbufferindex + 1)%yawbuffersize;
  }
  else
  {
    yawbufferfullindicator = 1;
    for(int i=0; i< yawbuffersize-1; i++)
    {
       Yawbuffer[i] = Yawbuffer[i+1];
    }
    Yawbuffer[yawbufferindex] = yaw;
  }
    
}



static void rawPrint(uint8_t id, float x,float y,float z,uint8_t sConfidence){
    union{
#pragma pack(1)
        struct {
            uint8_t id;
            uint8_t sConfidence;
            float x;
            float y;
            float z;

        } a;
#pragma pack()
        struct {
            uint8_t data[14];
        } b;
    } shared;
    shared.a.id = id;
    shared.a.x = x;
    shared.a.y = y;
    shared.a.z = z;
    shared.a.sConfidence = sConfidence;
    uint8_t outBuf[40];
    uint8_t inIdx = 0,outIdx = 0;
    outBuf[outIdx++] = 0x7E;
    for(inIdx=0;inIdx<14;inIdx++){
        if(shared.b.data[inIdx]==0x7E){
            outBuf[outIdx++] = 0x7D;
            outBuf[outIdx++] = 0x5E;

        }else if(shared.b.data[inIdx]==0x7D){
            outBuf[outIdx++] = 0x7D;
            outBuf[outIdx++] = 0x5D;

        }else{
            outBuf[outIdx++] = shared.b.data[inIdx];

        }
    }
    outBuf[outIdx++] = 0x7E;
#ifdef __IAR_SYSTEMS_ICC__
    //printf("%s",outBuf);
    USART1_Putsn((char*)outBuf,outIdx);
    SPI2_Putsn((char*)outBuf,outIdx);
#else
    write(stdout,outBuf,outIdx);
#endif
}




/** @addtogroup Template_Project
 * @{
 */

PUTCHAR_PROTOTYPE
{
    /* Place your implementation of fputc here */
    /* e.g. write a character to the USART */
    USART_SendData(EVAL_COM1, (uint8_t) ch);

    /* Loop until the end of transmission */
    while (USART_GetFlagStatus(EVAL_COM1, USART_FLAG_TC) == RESET)
    {}

    return ch;
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif
/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
