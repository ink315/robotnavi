#ifndef __ENCODE232_HH_
#define __ENCODE232_HH_

#include <Msg.hh>
#include <stdint.h>
namespace decodeRange{
    uint16_t capacity();
    int8_t decode(Msg_t* msg);
    void push(uint8_t data);
    void reset();
    uint16_t getLength();
}
#endif /*__ENCODE232_HH_*/


