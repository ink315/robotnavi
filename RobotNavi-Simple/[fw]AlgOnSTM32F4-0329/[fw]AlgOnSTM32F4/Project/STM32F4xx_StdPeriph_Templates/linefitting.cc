
#include "linefitting.h"
#include "arm_math.h"
#include "math.h"

//#define N 10             //N为要拟合的数据的个数
 
//float X[10] = {1.9,0.8,1.1,0.1,-0.1,4.4,4.6,1.6,5.5,3.4};
//float Y[10] = {0.7,-1.0,-0.2,-1.2,-0.1,3.4,0.0,0.8,3.7,2.0};



void Line_Fit(float *K, float *R, float *resi, float *fitYaw, float X[], float Y[], int N)
{
  float YE[20];
  
  float x_sum_average=0;   //数组 X[N] 个元素求和 并求平均值
  float y_sum_average=0;   //数组 Y[N] 个元素求和 并求平均值
  float x_square_sum=0;    //数组 X[N] 个个元素的平均值
  float x_multiply_y=0;    //数组 X[N]和Y[N]对应元素的乘机

  x_sum_average= Sum_Average(X, N);
  y_sum_average= Sum_Average(Y, N);
  x_square_sum = Squre_sum(X, N);
  x_multiply_y = X_Y_By(X,Y, N);
  *K = ( x_multiply_y - N * x_sum_average * y_sum_average)/( x_square_sum - N * x_sum_average*x_sum_average );
  *R = y_sum_average - *K * x_sum_average;
  float temp = 0;
  for(int i=0; i<N; i++)
  {
     YE[i] = *K*X[i] + *R;
     temp += fabs(Y[i]-YE[i]); 
  }
     *resi = temp;

      *fitYaw = (float)atan2((double)*K,1.0);

 // printf("K = %f\n",*K);
  //printf("R = %f\n",*R);
 // printf("Residue = %f\n", *resi);
 // printf("fitYaw = %f\n", *fitYaw);
  
}

float Sum_Average(float d[], int N)
{
unsigned int i=0;
float z=0;
for(i=0;i<N;i++)
{
 z = z + d[i];
}
z = z/N;
return z;
}
float X_Y_By(float m[],float n[], int N)
{
unsigned int i=0;
float z=0;
for(i=0;i<N;i++)
{
 z = z + m[i]*n[i];
}
return z;
}

float Squre_sum(float c[], int N)
{
unsigned int i=0;
float z=0;
for(i=0;i<N;i++)
{
 z = z + c[i]*c[i];
}
return z;
}

