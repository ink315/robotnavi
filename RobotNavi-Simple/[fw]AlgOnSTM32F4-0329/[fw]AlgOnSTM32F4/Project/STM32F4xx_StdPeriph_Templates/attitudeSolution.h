#include "arm_math.h"
#include "stdint.h"


//Attitude solution value
//#define PI 3.14159
#define RATE_D2R 0.0174533 
#define RATE_R2D 57.2957

extern float32_t att_euler[3],acc[3],pal[3];

void attitudeInit(void);

void attitudeSolution(int16_t rawData[],uint8_t mode);

void setImuBiasOffset(int16_t abias[], int16_t wbias[], int16_t hbias[]);