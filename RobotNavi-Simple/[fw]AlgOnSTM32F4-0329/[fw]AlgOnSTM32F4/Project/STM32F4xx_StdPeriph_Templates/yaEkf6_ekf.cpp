
/* ------------------------------------------------------------------------------------------------
 *                                     Copyright (2014-2024)
 *  All source is private properties of Beijing Ganweizhizhu Co. Lt.
 * @file    yaEkf.cpp
 * @author  Lei Song
 * @version V4.5.0
 * @date    07-March-2011
 * @brief   This is another implementation of the EKF
 * ------------------------------------------------------------------------------------------------
 */


/* ------------------------------------------------------------------------------------------------ *                                     Includes
 * ------------------------------------------------------------------------------------------------
 */
#include "stm324xg_eval.h"
#include "stdint.h"
#include "yaEkf6.hh"
#include <stdio.h>
#include "arm_math.h"
#include <stdint.h>
/* ------------------------------------------------------------------------------------------------
 *                                     Defs
 * ------------------------------------------------------------------------------------------------
 */

/* ------------------------------------------------------------------------------------------------
 *                                     External functions
 * ------------------------------------------------------------------------------------------------
 */

/* ------------------------------------------------------------------------------------------------
 *                                     External variables
 * ------------------------------------------------------------------------------------------------
 */

/* ------------------------------------------------------------------------------------------------
 *                                     Local functions
 * ------------------------------------------------------------------------------------------------
 */


extern __IO uint8_t numBeacon;

arm_matrix_instance_f32 z;
float32_t zm[OB_VEC_LEN];

//dMatrix X;
arm_matrix_instance_f32 X;
float32_t Xm[STATE_VEC_LEN];

arm_matrix_instance_f32 R;
float32_t Rm[OB_VEC_LEN][OB_VEC_LEN];

arm_matrix_instance_f32 Q;
float32_t Qm[STATE_VEC_LEN][STATE_VEC_LEN];

arm_matrix_instance_f32 P;
float32_t Pm[STATE_VEC_LEN][STATE_VEC_LEN];


const float r= 1;
const float q= 20;


static float B[MAX_NUM_BEACON][3];

static float distWindow[WINDOW_SIZE]={0.0,0.0,0.0,0.0,0.0,0.0,0.0};
static int beaconWindow[WINDOW_SIZE]={-1,-1,-1,-1,-1,-1,-1};
float estPosWindow[WINDOW_SIZE][3]={{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0}};
float vWindow[WINDOW_SIZE][3]={{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0}};


static int confidence=0;
static float tWindow[WINDOW_SIZE] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0};

char outBuf[100];


/* ------------------------------------------------------------------------------------------------
 *                                     Local variables
 * ------------------------------------------------------------------------------------------------
 */

/* ------------------------------------------------------------------------------------------------
 *                                     Global functions
 * ------------------------------------------------------------------------------------------------
 */

/* ------------------------------------------------------------------------------------------------
 *                                     Global variables
 * ------------------------------------------------------------------------------------------------
 */



uint8_t initBeacons (uint16_t numRx,const float coordRX[],float mStep){
    //step = mStep;
    for (int i=0; i< numRx; i++)
        for (int j =0; j<3; j++)
            B[i][j]=coordRX[i*3+j];

    arm_mat_init_f32(&z,OB_VEC_LEN,1,&(zm[0]));
    return 0;
}

/*float calDis(float targetPos[], int currentID, float B[][3]){
    float dsquare = 0.0;
    for (int i = 0; i<3; i++)
        dsquare= dsquare+ (targetPos[i]-B[currentID][i])*(targetPos[i]-B[currentID][i]);
    return sqrt(dsquare);
}*/


#define initSpeed 0.1f
uint8_t initEKF(arm_matrix_instance_f32 *initPos){
    arm_mat_init_f32(&R,OB_VEC_LEN,OB_VEC_LEN,&(Rm[0][0]));
    R.pData[0] = r*r;
    setMatToUnit(Qm,STATE_VEC_LEN,q*q);
    arm_mat_init_f32(&Q,STATE_VEC_LEN,STATE_VEC_LEN,&(Qm[0][0]));
    setMatToUnit(Pm,STATE_VEC_LEN,1);
    arm_mat_init_f32(&P,STATE_VEC_LEN,STATE_VEC_LEN,&(Pm[0][0]));
    arm_mat_init_f32(&X,STATE_VEC_LEN,1,&(Xm[0]));
    //arm_mat_init_f32(&z,OB_VEC_LEN,1,&(zm[0]));


    /*
       Q.resize(STATE_VEC_LEN,STATE_VEC_LEN);
       Q.unit();
       P.resize(STATE_VEC_LEN,STATE_VEC_LEN); // 6*6
       P.unit();
       P*=1;
       X.resize(STATE_VEC_LEN,1); //init x  //6*1
       X.null();
       int i,j;
       Q *=(q*q);
       */
    uint8_t i,k;
    for(i = 0;i<3;i++)
        X.pData[i] = initPos->pData[i];
    for(i=3;i<6;i++)
        X.pData[i] = initSpeed;

    for(k= 0; k< WINDOW_SIZE; k++){
        estPosWindow[k][0] = initPos->pData[0];
        estPosWindow[k][1] = initPos->pData[1];
        estPosWindow[k][2] = initPos->pData[2];
    }

    return 0;
}

bool updateEKF(arm_matrix_instance_f32 z[], int i, float step){
    //dMatrix x1(STATE_VEC_LEN,1);
    arm_matrix_instance_f32 x1,A,At,H,K,I,z1,P12,tH,tMat,tMat2,tMat3;   //声明 arm_matrix_instance_f32
    arm_matrix_instance_f32 tR,itR,tMat5,tMat6,tMat8;

    // the memory storage space for the matrix
    float32_t x1m[STATE_VEC_LEN],Am[STATE_VEC_LEN][STATE_VEC_LEN], Im[STATE_VEC_LEN][STATE_VEC_LEN], Atm[STATE_VEC_LEN][STATE_VEC_LEN], Km[STATE_VEC_LEN][OB_VEC_LEN];
    float32_t tMatm[STATE_VEC_LEN][STATE_VEC_LEN], tMat2m[STATE_VEC_LEN][STATE_VEC_LEN];
    float32_t Hm[OB_VEC_LEN][STATE_VEC_LEN], z1m[OB_VEC_LEN], P12m[STATE_VEC_LEN][OB_VEC_LEN], tHm[STATE_VEC_LEN][OB_VEC_LEN],tMat3m[OB_VEC_LEN][OB_VEC_LEN];
    float32_t tRm[OB_VEC_LEN][OB_VEC_LEN];
    float32_t itRm[OB_VEC_LEN][OB_VEC_LEN];
    float32_t tMat5m[OB_VEC_LEN];               //memory space
    float32_t tMat6m[STATE_VEC_LEN][OB_VEC_LEN];
    float32_t tMat8m[STATE_VEC_LEN][STATE_VEC_LEN];


    memset(x1m,0,STATE_VEC_LEN*STATE_VEC_LEN);
    // init a all zero matrix size:
    arm_mat_init_f32(&x1,STATE_VEC_LEN,1,x1m);              //将memory space赋给声明的矩阵变量 arm_mat_init_f32
    arm_mat_init_f32(&A,STATE_VEC_LEN,STATE_VEC_LEN,&(Am[0][0]));
    arm_mat_init_f32(&I,STATE_VEC_LEN,STATE_VEC_LEN,&(Im[0][0]));
    arm_mat_init_f32(&At,STATE_VEC_LEN,STATE_VEC_LEN,&(Atm[0][0]));
    arm_mat_init_f32(&tMat,STATE_VEC_LEN,STATE_VEC_LEN,&(tMatm[0][0]));
    arm_mat_init_f32(&tMat2,STATE_VEC_LEN,STATE_VEC_LEN,&(tMat2m[0][0]));
    arm_mat_init_f32(&H,OB_VEC_LEN,STATE_VEC_LEN,&(Hm[0][0]));
    arm_mat_init_f32(&K,STATE_VEC_LEN,OB_VEC_LEN,&(Km[0][0]));
    arm_mat_init_f32(&z1,OB_VEC_LEN,1,&(z1m[0]));
    arm_mat_init_f32(&P12,STATE_VEC_LEN,OB_VEC_LEN,&(P12m[0][0]));
    arm_mat_init_f32(&tH,STATE_VEC_LEN,OB_VEC_LEN,&(tHm[0][0]));
    arm_mat_init_f32(&tMat3,OB_VEC_LEN,OB_VEC_LEN,&(tMat3m[0][0]));
    arm_mat_init_f32(&tR,OB_VEC_LEN,OB_VEC_LEN,&(tRm[0][0]));
    arm_mat_init_f32(&itR,OB_VEC_LEN,OB_VEC_LEN,&(itRm[0][0]));
    arm_mat_init_f32(&tMat5,OB_VEC_LEN,1,&(tMat5m[0]));
    arm_mat_init_f32(&tMat6,STATE_VEC_LEN,OB_VEC_LEN,&(tMat6m[0][0]));
    arm_mat_init_f32(&tMat8,STATE_VEC_LEN,STATE_VEC_LEN,&(tMat8m[0][0]));


    setMatToUnit(Im,STATE_VEC_LEN,1);

    buildA(step*0.8,&A);  //6*6
//    dfStateVec(X, step,&x1);   //6*1
    arm_mat_mult_f32(&A,&X,&x1);  //6*1    x1=AX
    //P = A*P*~A+Q;
    arm_mat_trans_f32(&A,&At);  //6*6
    arm_mat_mult_f32(&A,&P,&tMat);  //6*6
    arm_mat_mult_f32(&tMat,&At,&tMat2);           //6*6  tMat2=A*P*~A
    arm_mat_add_f32(&tMat2,&Q,&P);                //6*6  P = A*P*~A+Q;

    //dMatrix H = jaccsd(hmeasVec,x1,i);
    //cout<< "H" <<H <<endl;

    buildH(&H,&x1,i);  //1*6
    dhmeasVec(&z1,x1,i);  //1*1
    //arm_mat_mult_f32(&H,&x1,&z1);                   //1*1  z1=Hx1


    arm_mat_trans_f32(&H,&tH);  //6*1
    arm_mat_mult_f32(&P,&tH,&P12);  //6*1           P12 = P*~H;
    arm_mat_mult_f32(&H,&P12,&tMat3);  //1*1        tMat3 = H*P*~H;
    arm_mat_add_f32(&tMat3,&R,&tR);  //1*1          tR = H*P*~H+R;
    //define tR
    // P12 = H*P*~H;
    //   tR = H*P*~H+R;
     //  tR.chold();
/****** cholesky的实现方式
    arm_matrix_instance_f32 tRs;
    float32_t tRsm[OB_VEC_LEN][OB_VEC_LEN];
    arm_mat_init_f32(&tRs,OB_VEC_LEN,OB_VEC_LEN,&(tRsm[0][0]));

    cholesky(&tR,&tRs);  //  1*1     tRs=chold(H*P12+R);
    arm_mat_inverse_f32(&tRs,&itR); // 1*1  itR = 1/tRs

       //U = P12/tR;
       //temp = ~R;
       //temp.inv();

    arm_mat_mult_f32(&P12,&itR,&U);  //6*1 U=P12/tRs
    arm_mat_trans_f32(&tRs,&temp);   //1*1
    arm_mat_inverse_f32(&temp,&tMat4);   //1*1  tMat4=inv(~tRs);

    //tMat4 = U*temp;
    arm_mat_mult_f32(&U,&tMat4,&tMat6);  //6*1  tMat6=U*inv(~tRs);
    //tMat5 = z-z1;
    arm_mat_sub_f32(z,&z1,&tMat5);       //1*1   tMat5=z-z1;
    arm_mat_mult_f32(&tMat6,&tMat5,&tMat9);  //6*1  tMat9= U*inv(~tRs)*(z-z1)
    arm_mat_add_f32(&x1,&tMat9,&X);  //6*1  X=x1+tMat9


      // X = x1+U* temp * (z-z1);
       //P = P-U*~U;

    arm_mat_trans_f32(&U,&tMat7);  // 1*6, tMat7=~U;
    arm_mat_mult_f32(&U,&tMat7,&tMat8); //6*6   tMat8=U*~U;
    arm_mat_sub_f32(&P,&tMat8,&P); //6*6 P=P-U*~U;

*/
/************采用增益矩阵的实现方式****/
    arm_mat_inverse_f32(&tR,&itR);       //1*1   itR=inv(R+H*P*~H)
    arm_mat_mult_f32(&P12,&itR,&K);      //6*1   K=P*~H*inv(R+H*P*~H)
    arm_mat_sub_f32(z,&z1,&tMat5);       //1*1   tMat5=z-z1;
    arm_mat_mult_f32(&K,&tMat5,&tMat6);  //1*1   tMat6=K*(z-z1);
    arm_mat_add_f32(&x1,&tMat6,&X);      //6*1    X=x1+K*(z-z1);

    arm_mat_add_f32(&K,&H,&tMat8);       //6*6   tMat8=KH;
    arm_mat_sub_f32(&I,&tMat8,&tMat2);   //6*6    tMat2 = I-KH
    arm_mat_sub_f32(&tMat2,&P,&P);       //6*6   P=[I-KH]*P
}

bool CheckSingularGenerateVirtualDis()
{

    while(confidence < 4)
    {
        int idx = 0;
        int newdisInx = 0;
        while(newdisInx < 1){
            idx = rand() % numBeacon;
            if(isMember(idx, beaconWindow) == 0)
                newdisInx = newdisInx +1;
        }
        float predictPos[3] ={0.0f,0.0f,0.0f};
        for(int k=0; k<3; k++)
        {
            predictPos[k] = estPosWindow[0][k] + 0.5*(estPosWindow[0][k] - estPosWindow[0][k]);
        }
        float32_t sdis = calDisFromBeacon(idx, predictPos);

        updateWindows(idx, sdis, 3.0f);

    }
    return 0;

}

int isMember(int a, int A[])
{
    for(int i=0; i<WINDOW_SIZE; i++)
        if(a == A[i])
            return 1;

    return 0;
}

int updateWindows(int b, float d, float t){

    uint8_t i,j,k;
    for(j=WINDOW_SIZE-1; j>0; j--)
    {
        beaconWindow[j] = beaconWindow[j-1];
        distWindow[j] = distWindow[j-1];
        tWindow[j] = tWindow[j-1];
    }
    beaconWindow[0] = b;
    distWindow[0] = d;
    tWindow[0] = 1.0;


    // update confidence
    k = WINDOW_SIZE;
    for(i=0;i<WINDOW_SIZE-1;i++)
        for(j=i+1;j<WINDOW_SIZE;j++)
        {
            if(beaconWindow[i]==beaconWindow[j])
            {
                k--;
                break;
            }
        }
    confidence = k;

    for(i=0; i<WINDOW_SIZE; i++)
        if(beaconWindow[i] == -1)
        {
            confidence = confidence-1;
            break;
        }
    return 1;
}

int updatePos(arm_matrix_instance_f32 *x)
{
    for(int j=WINDOW_SIZE-1; j>0; j--)
    {
        for(int k=0; k<3; k++)
        {
            estPosWindow[j][k] =estPosWindow[j-1][k];
            vWindow[j][k] = vWindow[j-1][k];
        }
    }
    estPosWindow[0][0] = x->pData[0];
    estPosWindow[0][1] = x->pData[1];
    estPosWindow[0][2] = x->pData[2];

    for (int k=0; k<3; k++) {
        vWindow[0][k] = 0.1*(estPosWindow[0][k]-estPosWindow[1][k]);
    }
    return 0;
}


float calDisFromBeacon(int i, float y[])
{
    float dis = 0.0;
#ifdef DEBUG_LSQ
    printf("i= %d, targetpos=%f %f %f\n\r", i, y[0], y[1], y[2]);
#endif

    dis = sqrt((B[i][0]-y[0])*(B[i][0]-y[0]) + (B[i][1]-y[1])*(B[i][1]-y[1]) + (B[i][2]-y[2])*(B[i][2]-y[2]));
#ifdef DEBUG_LSQ
    printf("pos:(%f,%f,%f), dis= %f\n\r", B[i][0],B[i][1],B[i][2],dis);
#endif
    return dis;
}


int updateTargetPos(float *targetPos, uint8_t t){


    *targetPos =(targetPos[0]+10)/10.0;
    *(targetPos+1) =(targetPos[1]+10)/10.0;
    *(targetPos+2) = 100.0f;

    return 0;
}


//dMatrix NonLinearLSQ(int beaconWindow[], int eventTimeWindow[], float** vWindow, float distWindow[])
uint8_t NonLinearLSQ(arm_matrix_instance_f32 *myx){

    //printf("b= %d t= %f v= %f d= %f\n\r",beaconWindow[0], tWindow[0], vWindow[0][0], distWindow[0]);
    //printf("%f %f %f \n\r",distWindow[0],distWindow[1],distWindow[2]);
    //printf("%f %f %f \n \r", B[beaconWindow[0]-1][0], B[beaconWindow[0]-1][1], B[beaconWindow[0]-1][2]);


    //printf("%d\t%d\t%d\t%d\t%d\t%d\t%d\t\n\r", beaconWindow[0], beaconWindow[1], beaconWindow[2],beaconWindow[3],beaconWindow[4],beaconWindow[5],beaconWindow[6]);
    //printf("%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t\n\r", distWindow[0],distWindow[1],distWindow[2],distWindow[3],distWindow[4],distWindow[5],distWindow[6]);
    //printf("%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t\n\r", B[beaconWindow[0]-1][0], B[beaconWindow[1]-1][0], B[beaconWindow[2]-1][0],B[beaconWindow[3]-1][0],B[beaconWindow[4]-1][0],B[beaconWindow[5]-1][0], B[beaconWindow[6]-1][0]);
    //printf("%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t\n\r", B[beaconWindow[0]-1][1], B[beaconWindow[1]-1][1], B[beaconWindow[2]-1][1],B[beaconWindow[3]-1][1],B[beaconWindow[4]-1][1],B[beaconWindow[5]-1][1], B[beaconWindow[6]-1][1]);
    //printf("%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t\n\r", B[beaconWindow[0]-1][2], B[beaconWindow[1]-1][2], B[beaconWindow[2]-1][2],B[beaconWindow[3]-1][2],B[beaconWindow[4]-1][2],B[beaconWindow[5]-1][2], B[beaconWindow[6]-1][2]);

    int n = WINDOW_SIZE;
    int m = 2;
    arm_matrix_instance_f32 b,s;

    float32_t bm[WINDOW_SIZE][2];
    float32_t sm[WINDOW_SIZE][2];

    arm_mat_init_f32(&b, WINDOW_SIZE,2, &(bm[0][0]));
    arm_mat_init_f32(&s, WINDOW_SIZE,2, &(sm[0][0]));

    uint8_t i,j,t;

    for(i=0; i<n; i++){
        t = beaconWindow[i];
        for(j=0; j<m; j++){
            bm[i][j]=B[t][j];
        }

//        arm_matrix_instance_f32 temp;
//       float32_t tempm[2]={0,0};
//       arm_mat_init_f32(&temp, 2,1, tempm);
        float temp[2] = {0.0,0.0};
        for(int j=1; j<i; j++)
        {
            for(int k=0; k<m; k++)
                temp[k] = temp[k] + tWindow[j-1]*vWindow[j-1][k];
        }
        for (int k=0; k<m; k++)
            sm[i][k] = bm[i][k] + temp[k];
    }

    int sizeA = 21;
    arm_matrix_instance_f32 A, C;
    float32_t Am[21][2];
    float32_t Cm[21];
    arm_mat_init_f32(&A, sizeA, m, &(Am[0][0]));
    arm_mat_init_f32(&C, sizeA, 1, &(Cm[0]));


    int index = 0;
    uint8_t k;
    for( i=0; i<n; i++){
        for( j=i+1; j<n; j++)
        {
            for( k=0; k<m; k++){
                Am[index][k] = sm[j][k] - sm[i][k] + sm[j][k] - sm[i][k];
            }
            Cm[index] = distWindow[i]*distWindow[i] -distWindow[j]*distWindow[j] -sm[i][0]*sm[i][0] -sm[i][1]*sm[i][1] + sm[j][0]*sm[j][0] + sm[j][1]*sm[j][1];
            index = index + 1;
        }
    }
   /* printf("A=\n\r");
    for(i = 0;i<A.numRows;i++){
        for(j=0;j<A.numCols;j++)
            printf("%f ",A.pData[i*A.numCols+j]);
        printf("\n\r");
    }
    printf("C=\n\r");
    for(i = 0;i<C.numRows;i++){
        for(j=0;j<C.numCols;j++)
            printf("%f ",C.pData[i*A.numCols+j]);
        printf("\n\r");
    }

   */
    arm_matrix_instance_f32 x1, At, temp, tempi, temp2, tz;
    float32_t x1m[2][1];
    float32_t Atm[2][21];
    float32_t tempm[2][2];
    float32_t tempim[2][2];
    float32_t temp2m[2][21];
    float32_t tzm[WINDOW_SIZE][1];


    arm_mat_init_f32(&x1, m, 1, &(x1m[0][0]));
    arm_mat_init_f32(&At, m, sizeA, &(Atm[0][0]));
    arm_mat_init_f32(&temp, m, m, &(tempm[0][0]));
    arm_mat_init_f32(&tempi, m, m, &(tempim[0][0]));
    arm_mat_init_f32(&temp2, m, sizeA, &(temp2m[0][0]));
    arm_mat_init_f32(&tz, n, 1, &(tzm[0][0]));

    //dMatrix x1(m, 1);
    arm_mat_trans_f32(&A, &At);
    arm_mat_mult_f32(&At, &A, &temp);
    //dMatrix temp = ~A * A;

    arm_mat_inverse_f32(&temp, &tempi);
    //temp.inv();
    arm_mat_mult_f32(&tempi, &At, &temp2);
    //dMatrix temp2 = tempi * ~A;
    arm_mat_mult_f32(&temp2, &C, &x1);
    //x1 = temp2 * C;

    //dMatrix tz(n,1);

    for(int i=0; i<n; i++)
    {
        float temp3 = distWindow[i]*distWindow[i]-(x1m[0][0]-sm[i][0])*(x1m[0][0]-sm[i][0])-(x1m[1][0]-sm[i][1])*(x1m[1][0]-sm[i][1]);
        int t=beaconWindow[i];

        if(temp3 < 0)
            tzm[i][0] = B[t][2];
        else
            tzm[i][0] = B[t][2]-sqrt(temp3);

    }
    float temp4=0;
    for(int i=0; i<n; i++)
    {
        temp4= temp4 + tzm[i][0];
    }
    temp4 = temp4/n;

    float tempDouble;
    tempDouble = x1m[0][0];
    myx->pData[0]  = tempDouble;
    tempDouble = x1m[1][0];
    myx->pData[1] = tempDouble;

    myx->pData[2] = temp4;
    return 0;

}

uint8_t cholesky(arm_matrix_instance_f32 *A, arm_matrix_instance_f32 *L) {
    if (A == NULL||A->numRows != A->numCols||A->numRows != L->numRows){
        //wrong input matrix
        return 0;
    }
    uint16_t n = A->numRows;
    for (uint16_t i = 0; i < n; i++)
        for (uint16_t j = 0; j < (i+1); j++) {
            float s = 0;
            for (uint16_t k = 0; k < j; k++)
                s += L->pData[i * n + k] * L->pData[j * n + k];
            L->pData[i * n + j] = (i == j) ?
                sqrt(A->pData[i * n + i] - s) :
                (1.0 / L->pData[j * n + j] * (A->pData[i * n + j] - s));
        }
    return 1;
}

void buildA(float32_t dt,arm_matrix_instance_f32* cA)
{

    //dMatrix cA(STATE_VEC_LEN,STATE_VEC_LEN);
    uint8_t i,j;
    for(i=0; i<STATE_VEC_LEN; i++)
    {
        for(j=0; j<STATE_VEC_LEN; j++)
        {
            cA->pData[i*cA->numCols+j] = 0;
        }
        cA->pData[i*cA->numCols+i] = 1;
    }
    cA->pData[0*cA->numCols+3] = dt;
    cA->pData[1*cA->numCols+4] = dt;
    cA->pData[2*cA->numCols+5] = dt;
}

void dfStateVec(arm_matrix_instance_f32 x, float step,arm_matrix_instance_f32* x1){
    x1->pData[0]=x.pData[0]+step*x.pData[3];
    x1->pData[1]=x.pData[1]+step*x.pData[4];
    x1->pData[2]=x.pData[2]+step*x.pData[5];
    x1->pData[3]=x.pData[3];
    x1->pData[4]=x.pData[4];
    x1->pData[5]=x.pData[5];
}

void buildH(arm_matrix_instance_f32 *cH,arm_matrix_instance_f32 *x, int i)
{
    float dis = sqrt((x->pData[0]-B[i][0])*(x->pData[0]-B[i][0])+(x->pData[1]-B[i][1])*(x->pData[1]-B[i][1])+(x->pData[2]-B[i][2])*(x->pData[2]-B[i][2]));
    cH->pData[0] = (x->pData[0]-B[i][0])/dis;
    cH->pData[1] = (x->pData[1]-B[i][1])/dis;
    cH->pData[2] = (x->pData[2]-B[i][2])/dis;
    cH->pData[3] = 0.0;
    cH->pData[4] = 0.0;
    cH->pData[5] = 0.0;
}

void dhmeasVec(arm_matrix_instance_f32 *dz,arm_matrix_instance_f32 x, int i){
    //dComplex xinput[6] = {x(0,0),x(1,0),x(2,0),x(3,0),x(4,0), x(5,0)};
    float dis = 0;
    for (int k=0; k<3; k++)
    {
        dis += (x.pData[k]-B[i][k])*(x.pData[k]-B[i][k]);
    }
    dis = sqrt(dis);
    //double a = 2*(B[t_minus_1][0]-B[t][0])*y1+2*(B[t_minus_1][1]-B[t][1])*y2+ B[t][0]*B[t][0] -B[t_minus_1][0]*B[t_minus_1][0]+ B[t][1]*B[t][1] -B[t_minus_1][1]*B[t_minus_1][1];
    dz->pData[0]= dis;
}
