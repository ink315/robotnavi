/*
 * =====================================================================================
 *
 *       Filename:  main.hh
 *
 *    Description:  header file for main.cc 
 *
 *        Version:  1.0
 *        Created:  9/24/2015 9:53:13 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lei Song
 *   Organization:  Beijing GWZZ 
 *
 * =====================================================================================
 */
#ifndef __MAIN_HH__
#define __MAIN_HH__

#ifdef __cplusplus
extern "C" {
#endif

static uint8_t pop8();
static int32_t pop32();
static double popDouble();
void ClearRxUart1();
void ClearRxUart2();
void USART1_Puts(const char * str);
void USART2_Puts(char * str);
void initUSART();
void USART1_NVIC_Configuration();
void USART2_NVIC_Configuration();
static void print(const char* output);
static void rawPrint(uint8_t id, float x,float y,float z);
#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //__MAIN_HH__
