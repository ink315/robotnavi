/* ------------------------------------------------------------------------------------------------
 *                                     Copyright (2014-2024)
 *  All source is private properties of Beijing Ganweizhizhu Co. Lt.  * ------------------------------------------------------------------------------------------------
 */

/* Includes ------------------------------------------------------------------*/
//#include "stm32f10x.h"
//#include "stm32_eval.h"
#include "CircularBuffer.hh"
//#include "usBoard.hh"
#include "yaEkf6.hh"

#include <stdio.h>
#include <iostream> 
#include <string.h>
//#include "main.hh"
//#include "Msg.hh"
using namespace std;
using std::size_t;
using namespace techsoft;

using techsoft::epsilon;
using techsoft::isVecEq;
//using std::exception;

/** @addtogroup STM32F10x_StdPeriph_Template
 * @{
 */
#define DATA_BUFFER_SIZE 512

/* Global variables ---------------------------------------------------------*/

uint8_t range_data[DATA_BUFFER_SIZE];
CircularBuffer rangeBuffer(DATA_BUFFER_SIZE,range_data);

/* Private variables ---------------------------------------------------------*/
#define BufferSize 28
uint8_t FlagRxUart1;
uint8_t FlagRxUart2;
volatile uint8_t RxCounter1;
volatile uint8_t RxCounter2;
volatile uint8_t RxBufferOfUart1[BufferSize];
volatile uint8_t RxBufferOfUart2[BufferSize]; 	




int main(void){

    double targetPos[3] ={100,100,100};
    extern int NBeacon;
    extern double beaconCoord[];
    extern int confidence;
 
    extern dMatrix z;
    extern dMatrix X;
    extern dMatrix xl;    //3*1
    extern dMatrix R;
    extern dMatrix Q;
    extern dMatrix P;


    uint16_t dwBytesRead = 0;
    int id = 0;

    initBeacons(NBeacon,beaconCoord,3);

    for(int roundCount=0; roundCount<100; roundCount++){
    		updateTargetPos(targetPos, double(roundCount));
//			usleep(50000);
			id = roundCount % NBeacon;  // current ID, 0, 1, 2, 3, ..., Nbeacon-1
			double distMM = calDisFromBeacon(id, targetPos)+rand()%10-5;
            z(0,0) = distMM;
			updateWindows(id, distMM, step);

			if(roundCount == WINDOW_SIZE){
			    xl= NonLinearLSQ();
			    updatePos(xl);
			    initEKF(xl);  //initialize EKF
			}
			else if(roundCount > WINDOW_SIZE) {
			    updateEKF(z, id, step);
			    xl= NonLinearLSQ();
			    updatePos(xl);
			    //cout <<"targetPos =" <<targetPos[0] <<endl<< targetPos[1] <<endl<<targetPos[2]<<endl;

			    //fprintf(fr, "%d\t %f\t \t %lf\t %lf\t %lf\t \t %lf\t %lf\t %lf\t \t %lf\t %lf\t %lf\n", beaconWindow[0], distWindow[0], targetPos[0], targetPos[1], targetPos[2], ekf_x, ekf_y, ekf_z, lsq_x, lsq_y, lsq_z);

			    double ekf_x = X(0,0);
			    double ekf_y = X(1,0);
			    double ekf_z = X(2,0);
			    double lsq_x = xl(0,0);
			    double lsq_y = xl(1,0);
			    double lsq_z = xl(2,0);
			    printf("ekf:x= %lf y= %lf z= %lf\n\r", ekf_x,ekf_y,ekf_z);
			    printf("lsq:x= %lf y= %lf z= %lf\n\r", lsq_x,lsq_y,lsq_z);
			    printf("grt:x= %lf y= %lf z= %lf\n\r", targetPos[0],targetPos[1],targetPos[2]);    

		}
		    
	}	
	    
} 
    


static uint8_t pop8(){
    return rangeBuffer.pop();
}

static int16_t pop16() {
    union {
	int16_t a;
	struct {
	    uint8_t data[2];
	} b;
    } shared;
    shared.b.data[0] = rangeBuffer.pop();
    shared.b.data[1] = rangeBuffer.pop();

    return shared.a;
}

static int32_t pop32() {
    union {
	int32_t a;
	struct {
	    uint8_t data[4];
	} b;
    } shared;
    shared.b.data[0] = rangeBuffer.pop();
    shared.b.data[1] = rangeBuffer.pop();
    shared.b.data[2] = rangeBuffer.pop();
    shared.b.data[3] = rangeBuffer.pop();
    return shared.a;
}

static double popDouble() {
    union {
	double a;
	struct {
	    uint8_t data[8];
	} b;
    } shared;
    for(int j =0;j<8;j++)
	shared.b.data[j] = rangeBuffer.pop();
    return shared.a;
}

//function : clear RxBuffer
#ifdef __IAR_SYSTEMS_ICC__
void ClearRxUart1(void){
    memset((void*)RxBufferOfUart1,0,BufferSize);
    RxCounter1 = 0;                    
}   
void ClearRxUart2(void){
    memset((void*)RxBufferOfUart2,0,BufferSize);
    RxCounter2 = 0;                    
}
//function : send str 
void USART1_Puts(const char * str)
{    
    while(*str){        
	USART_SendData(USART1, *str++);  		         
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);    
    }
}

void USART1_Putsn(const char * str,uint8_t n)
{    
    for(int i=0;i<n;i++){
	USART_SendData(USART1, str[i]);  		         
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);    
    }
}
void USART2_Puts(char * str)
{    
    while(*str){        
	USART_SendData(USART2, *str++);  		          
	while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);    
    }
}

void initUSART(){
    USART_InitTypeDef USART_InitStructure;
    USART_InitStructure.USART_BaudRate = bdrate;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

    STM_EVAL_COMInit(COM2, &USART_InitStructure); // Com2 for terminal
    USART_ITConfig(USART2,USART_IT_RXNE,ENABLE);


    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    STM_EVAL_COMInit(COM1, &USART_InitStructure); // Com2 for terminal
    USART_ITConfig(USART1,USART_IT_RXNE,ENABLE);

}

void USART1_NVIC_Configuration(void) {
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}
void USART2_NVIC_Configuration(void) {
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
} 
#endif

static void print(const char* output){
#ifdef __IAR_SYSTEMS_ICC__
    USART1_Puts(output);
#else
    printf("%s",output);
#endif
}

#ifdef __IAR_SYSTEMS_ICC__
static void rawPrint(uint8_t id, float x,float y,float z){
    union{
	struct {
	    uint8_t id;
	    float x;
	    float y;
	    float z;
	} a;
	struct {
	    uint8_t data[13];
	} b;
    } shared;
    shared.a.id = id;
    shared.a.x = x;
    shared.a.y = y;
    shared.a.z = z;
    uint8_t outBuf[40];
    uint8_t inIdx = 0,outIdx = 0;
    outBuf[outIdx++] = 0x7E;
    for(inIdx=0;inIdx<13;inIdx++){
	if(shared.b.data[inIdx]==0x7E){
	    outBuf[outIdx++] = 0x7D;
	    outBuf[outIdx++] = 0x5E;
	}else if(shared.b.data[inIdx]==0x7D){
	    outBuf[outIdx++] = 0x7D;
	    outBuf[outIdx++] = 0x5D;
	}else{
	    outBuf[outIdx++] = shared.b.data[inIdx];
	}
    }
    outBuf[outIdx++] = 0x7E;
    USART1_Putsn((char*)outBuf,outIdx);
}
#endif
