#ifndef __yaEkf_hh__
#define __yaEkf_hh__


#define STATE_VEC_LEN 6

#define OB_VEC_LEN 1

#define NUM_BEACON 6
#define WINDOW_SIZE 7


#include <cmatrix>
#include <stdint.h>
using namespace techsoft;

static double step = 1.0;



typedef techsoft::matrix<float>        fMatrix;
typedef std::valarray<float>           fVector;

typedef techsoft::matrix<double>       dMatrix;
typedef std::valarray<double>          dVector;

typedef techsoft::matrix<long double>  ldMatrix;
typedef std::valarray<long double>     ldVector;

typedef std::complex<float>            fComplex;
typedef techsoft::matrix<fComplex>     cfMatrix;
typedef std::valarray<fComplex>        cfVector;

typedef std::complex<double>           dComplex;
typedef techsoft::matrix<dComplex>     cdMatrix;
typedef std::valarray<dComplex>        cdVector;

typedef std::complex<long double>      ldComplex;
typedef techsoft::matrix<ldComplex>    cldMatrix;
typedef std::valarray<ldComplex>       cldVector;

uint8_t initEKF(dMatrix initPos);
uint8_t initBeacons (uint16_t numRx,const double coordRX[],double mStep); 
bool updateEKF(dMatrix z,int i, double step);

dMatrix jaccsd(cdMatrix (*fun)(cdMatrix, int), dMatrix x, int i);
cdMatrix fState(cdMatrix x, double step);
cdMatrix hmeas(cdMatrix x, int i);

dMatrix dfState(dMatrix x, double step);
dMatrix dhmeas(dMatrix x, int i);

dMatrix jaccsd(cdMatrix (*fun)(cdMatrix, double), dMatrix x, double step);
double calDisFromBeacon(int i,  double x[]);
dMatrix NonLinearLSQ();
bool CheckSingularGenerateVirtualDis();

int updateWindows(int b, double d, int t);
int updatePos(dMatrix x);
cdMatrix fStateVec(cdMatrix x, double step);
dMatrix dfStateVec(dMatrix x, double step);
cdMatrix fStateVecShort(cdMatrix x, double step);
dMatrix dfStateVecShort(dMatrix x, double step);
cdMatrix hmeasVec(cdMatrix x, int i);
dMatrix dhmeasVec(dMatrix x, int i);
int isMember(int a, int A[]);
int updateTargetPos(double targetPos[], double t);


#endif
