/*
 * =====================================================================================
 *
 *       Filename:  Msg.hh
 *
 *    Description:  message format for the msg_t
 *
 *        Version:  1.0
 *        Created:  9/24/2015 3:26:09 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#define MAX_EDGE_NUM 10

typedef struct {
    uint8_t ID;
    volatile uint8_t Seq;
    volatile uint8_t numRising;
    volatile uint8_t numFalling;
    volatile uint16_t risingEdge[MAX_EDGE_NUM];
    volatile uint16_t fallingEdge[MAX_EDGE_NUM];
    volatile uint32_t TimerTick;
} Msg_t;

