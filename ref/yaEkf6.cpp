
/* ------------------------------------------------------------------------------------------------
 *                                     Copyright (2014-2024)
 *  All source is private properties of Beijing Ganweizhizhu Co. Lt.
 * @file    yaEkf.cpp
 * @author  Lei Song
 * @version V4.5.0
 * @date    07-March-2011
 * @brief   This is another implementation of the EKF
 * ------------------------------------------------------------------------------------------------
 */


/* ------------------------------------------------------------------------------------------------ *                                     Includes
 * ------------------------------------------------------------------------------------------------
 */
#include "cmatrix"
#include "stdint.h"
#include "yaEkf6.hh"
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <complex> 


using namespace std;

/* ------------------------------------------------------------------------------------------------
 *                                     Defs
 * ------------------------------------------------------------------------------------------------
 */
#define DEBUG

/* ------------------------------------------------------------------------------------------------
 *                                     External functions
 * ------------------------------------------------------------------------------------------------
 */

/* ------------------------------------------------------------------------------------------------
 *                                     External variables
 * ------------------------------------------------------------------------------------------------
 */

/* ------------------------------------------------------------------------------------------------
 *                                     Local functions
 * ------------------------------------------------------------------------------------------------
 */


int NBeacon = NUM_BEACON;

dMatrix z(1,1);
dMatrix X;
dMatrix xl;    //3*1
dMatrix R;
dMatrix Q;
dMatrix P;


const double eps = std::numeric_limits<double>::epsilon();
const double r= 2;
const double q= 0.5;


static double B[NUM_BEACON][3];

static double distWindow[WINDOW_SIZE]={0.0,0.0,0.0,0.0,0.0,0.0,0.0};
static int beaconWindow[WINDOW_SIZE]={-1,-1,-1,-1,-1,-1,-1};
static double estPosWindow[WINDOW_SIZE][3]={{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0}};
static double vWindow[WINDOW_SIZE][3]={{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0}};


static int confidence=0;
//static double step = 1.0;
static int tWindow[WINDOW_SIZE] = {0,0,0,0,0,0,0};

double beaconCoord[] = {
    169,   0, 245, 
    0,   0, 245, 
    134,  76, 245, 
    56,  76, 245, 
    174, 146, 245,
    0, 146, 245, 
    -70, 52, 254, 
    139, 19, 254, 
    78, 137, 254};





/* ------------------------------------------------------------------------------------------------
 *                                     Local variables
 * ------------------------------------------------------------------------------------------------
 */

/* ------------------------------------------------------------------------------------------------
 *                                     Global functions
 * ------------------------------------------------------------------------------------------------
 */

/* ------------------------------------------------------------------------------------------------
 *                                     Global variables
 * ------------------------------------------------------------------------------------------------
 */



uint8_t initBeacons (uint16_t numRx,const double coordRX[],double mStep){
    //cout<< "Init rx coord"<<endl;
    //B.resize(numRx,3);
    //B = dMatrix(numRx,3,coordRX);
    step = mStep;
    for (int i=0; i< numRx; i++)
	for (int j =0; j<3; j++)
	    B[i][j]=coordRX[i*3+j];

    return 0;
} 

double calDis(double targetPos[], int currentID, double B[][3])
{
    double dsquare = 0.0;
    for (int i = 0; i<3; i++)
	dsquare= dsquare+ (targetPos[i]-B[currentID][i])*(targetPos[i]-B[currentID][i]);
    return sqrt(dsquare);
} 



uint8_t initEKF(dMatrix initPos){

#ifdef DEBUG_ON_PC
    cout<< "Init EKF parameters..."<<endl;
#endif
    R.resize(OB_VEC_LEN,OB_VEC_LEN);
    R = r*r;
    Q.resize(STATE_VEC_LEN,STATE_VEC_LEN);
    Q.unit();
    P.resize(STATE_VEC_LEN,STATE_VEC_LEN); // 6*6
    P.unit();
    P*=1;
    X.resize(STATE_VEC_LEN,1); //init x  //6*1
    X.null();
    int i,j;
    Q *=(q*q);


    double s[] ={initPos(0,0), initPos(1,0), initPos(2,0), 0.1, 0.1, 0.1};
    X = dMatrix(STATE_VEC_LEN,1,s);

    for(int k= 0; k< WINDOW_SIZE; k++) 
    {   
    	estPosWindow[k][0] = initPos(0,0);
    	estPosWindow[k][1] = initPos(1,0);
    	estPosWindow[k][2] = initPos(2,0);
    }

#ifdef DEBUG_ON_PC
    cout << "initPos" << "estPosWindow[2][0] = " << estPosWindow[2][0] <<  "estPosWindow[2][0] = "  << estPosWindow[2][1] <<endl;  
    cout << Q << R <<endl;
#endif
    return 0;
}

bool updateEKF(dMatrix z,int i, double step){
    cout << "z=" << z <<endl;
    dMatrix x1(STATE_VEC_LEN,1);
    x1.null();
    dMatrix A; 

    A = jaccsd(fStateVec,X,step);
    //cout<< "A" <<A <<endl;
    x1 = dfStateVec(X, step);
    P = A*P*~A+Q;
    dMatrix H = jaccsd(hmeasVec,x1,i);
    //cout<< "H" <<H <<endl;
    dMatrix z1 = dhmeasVec(x1, i);
    cout<< "z= " << z <<" z1=" <<z1<<endl;    
    dMatrix P12 = P*~H;
    dMatrix tR = H*P12+R;
    tR.chold();

    dMatrix U = P12/tR;
    dMatrix temp = ~R; 
    temp.inv();
    X = x1+U* temp * (z-z1);
    P = P-U*~U;
    return true;
}

dMatrix jaccsd(cdMatrix (*fun)(cdMatrix, int),dMatrix x, int i){
    cdMatrix z = fun((cdMatrix)x, i);
    int n = x.size();
    int m = z.size();
    dMatrix A(m,n);
    std::complex<double> mycomplex(0,1.0);
    dComplex h = n*eps*mycomplex;

    dMatrix cA(m,n);
    for (int k = 0;k<n;k++){
	cdMatrix x1 = x;
	x1[k][0] = x1[k][0]+h;
	cdMatrix temp;
	temp = fun(x1, i);
	for(int j=0;j<m;j++){
	    dComplex tt = temp(j,0);
	    cA(j,k)=tt.imag()/n/eps;
	}
    }
    return cA;
}

dMatrix jaccsd(cdMatrix (*fun)(cdMatrix, double),dMatrix x, double step){
    cdMatrix z = fun((cdMatrix)x, step);
    int n = x.size();
    int m = z.size();
    dMatrix A(m,n);
    std::complex<double> mycomplex(0,1.0);
    dComplex h = n*eps*mycomplex;
    dMatrix cA(m,n);
    for (int k = 0;k<n;k++){
	cdMatrix x1 = x;
	x1[k][0] = x1[k][0]+h;
	cdMatrix temp;
	temp = fun(x1, step);
	for(int j=0;j<m;j++){
	    dComplex tt = temp(j,0);
	    cA(j,k)=tt.imag()/n/eps;
	}
    }
    A.free();
    return cA;
}
bool CheckSingularGenerateVirtualDis()
{

    while(confidence < 4)
    {
	int idx = 0;
	int newdisInx = 0;
	while(newdisInx < 1){
	    idx = rand() % NBeacon;
	    if(isMember(idx, beaconWindow) == 0)
		newdisInx = newdisInx +1;
	}
	double predictPos[3] ={0,0,0};
	for(int k=0; k<3; k++)
	{
#ifdef DEBUG
	    printf("k = %u\n",k);
#endif
	    predictPos[k] = estPosWindow[0][k] + 0.5*(estPosWindow[0][k] - estPosWindow[0][k]);
	}
	double sdis = calDisFromBeacon(idx, predictPos);

	updateWindows(idx, sdis, step);

    }    
    return 0;

}

int isMember(int a, int A[])
{
    for(int i=0; i<WINDOW_SIZE; i++)
	if(a == A[i])
	    return 1;

    return 0;    
}

int updateWindows(int b, double d, int t){

 //   printf("%d %d %d \n",beaconWindow[0],beaconWindow[1],beaconWindow[2]);
 //   printf("%f %f %f \n",distWindow[0],distWindow[1],distWindow[2]);

    for(int j=WINDOW_SIZE-1; j>0; j--)
    {
	beaconWindow[j] = beaconWindow[j-1];
	distWindow[j] = distWindow[j-1];
	tWindow[j] = tWindow[j-1];
    }
    beaconWindow[0] = b;
    distWindow[0] = d;
    tWindow[0] = t;


    // update confidence
    int k = WINDOW_SIZE;
    for(int i=0;i<WINDOW_SIZE-1;i++)
	for(int j=i+1;j<WINDOW_SIZE;j++)
	{
	    if(beaconWindow[i]==beaconWindow[j])
	    {
		k--;
		break;
	    }
	}
    confidence = k; 

    for(int i=0; i<WINDOW_SIZE; i++)
	if(beaconWindow[i] == -1)
	{
	    confidence = confidence-1;
	    break;
	}

    return 1;
}

int updatePos(dMatrix x)
{

    for(int j=WINDOW_SIZE-1; j>0; j--)
    {
	for(int k=0; k<3; k++)
	{
	    estPosWindow[j][k] =estPosWindow[j-1][k];

	    vWindow[j][k] = vWindow[j-1][k];

	}
    }
    estPosWindow[0][0] = x(0,0);
    estPosWindow[0][1] = x(1,0);
    estPosWindow[0][2] = x(2,0);

    for (int k=0; k<3; k++)
    {
	vWindow[0][k] = 0.3*(estPosWindow[0][k]-estPosWindow[1][k]);
    }

    /*    x(0,0) = (estPosWindow[0][0] + estPosWindow[1][0]) /2;
	  x(1,0) = (estPosWindow[0][1] + estPosWindow[1][1]) /2;
	  x(2,0) = (estPosWindow[0][2] + estPosWindow[1][2]) /2;


	  estPosWindow[0][0] = x(0,0);
	  estPosWindow[0][1] = x(1,0);
	  estPosWindow[0][2] = x(2,0);*/
    return 0;
}


cdMatrix fStateVec(cdMatrix x, double step){
    //  cout << "step" << step <<endl;
    dComplex xinput[STATE_VEC_LEN] = {x(0,0),x(1,0),x(2,0),x(3,0),x(4,0),x(5,0)};
    dComplex ret[] = {xinput[0]+step*xinput[3],xinput[1]+step*xinput[4],xinput[2]+step*xinput[5], xinput[3],xinput[4],xinput[5]};
    //cout << "xinput" << xinput[0] <<" " <<xinput[1]<<endl;
    cdMatrix retM(STATE_VEC_LEN,1,ret);
    //cout << "retM" << retM <<endl;
    return retM;
}

dMatrix dfStateVec(dMatrix x, double step){
    double xinput[STATE_VEC_LEN] = {x(0,0),x(1,0),x(2,0),x(3,0),x(4,0),x(5,0)};
    double ret[] = {xinput[0]+step*xinput[3],xinput[1]+step*xinput[4],xinput[2]+step*xinput[5], xinput[3],xinput[4],xinput[5]};
    dMatrix retM(STATE_VEC_LEN,1,ret);
    return retM;
}

cdMatrix fStateVecShort(cdMatrix x, double step){
    dComplex xinput[STATE_VEC_LEN] = {x(0,0),x(1,0),x(2,0),x(3,0),x(4,0),x(5,0)};
    dComplex ret[] = {xinput[0],xinput[1],xinput[2],xinput[3],xinput[4],xinput[5]};
    cdMatrix retM(STATE_VEC_LEN,1,ret);
    return retM;
}

dMatrix dfStateVecShort(dMatrix x, double step){
    double xinput[STATE_VEC_LEN] = {x(0,0),x(1,0),x(2,0),x(3,0),x(4,0),x(5,0)};
    double ret[] = {xinput[0],xinput[1],xinput[2],xinput[3],xinput[4],xinput[5]};
    dMatrix retM(STATE_VEC_LEN,1,ret);
    return retM;
}

cdMatrix hmeasVec(cdMatrix x, int i){        // Bi=Bt-1; Bj = Bt  
    //dComplex xinput[6] = {x(0,0),x(1,0),x(2,0),x(3,0),x(4,0), x(5,0)};
    dComplex dis = 0.0;
    for (int k=0; k<3; k++)
    {
	dis = dis + (x(k,0)-B[i][k])*(x(k,0)-B[i][k]);
    }
    dis = sqrt(dis);
    //    dComplex a[] = {(u1[0]-u2[0])*xinput[0] +(u1[0]-u2[0])*xinput[0]  + (u1[1]-u2[1])*xinput[1] + (u1[1]-u2[1])*xinput[1]+u2[0]*u2[0]-u1[0]*u1[0]+u2[1]*u2[1]-u1[1]*u1[1]};

    cdMatrix  retM(1,1,dis);
//    cout << "retM= " << retM <<endl;
    return retM;
}

dMatrix dhmeasVec(dMatrix x, int i){
    //dComplex xinput[6] = {x(0,0),x(1,0),x(2,0),x(3,0),x(4,0), x(5,0)};
    dMatrix dz(1,1);
    double dis = 0;
    for (int k=0; k<3; k++)
    {
	dis = dis + (x(k,0)-B[i][k])*(x(k,0)-B[i][k]);
    }
    dis = sqrt(dis);
    //double a = 2*(B[t_minus_1][0]-B[t][0])*y1+2*(B[t_minus_1][1]-B[t][1])*y2+ B[t][0]*B[t][0] -B[t_minus_1][0]*B[t_minus_1][0]+ B[t][1]*B[t][1] -B[t_minus_1][1]*B[t_minus_1][1];
    dz(0,0)= dis;
    return dz;
}


double calDisFromBeacon(int i, double y[])
{
    double dis = 0.0;

    dis = sqrt((beaconCoord[0+i*3]-y[0])*(beaconCoord[i*3+0]-y[0]) + (beaconCoord[i*3+1]-y[1])*(beaconCoord[i*3+1]-y[1]) + (beaconCoord[i*3+2]-y[2])*(beaconCoord[i*3+2]-y[2]));
    return dis;
}


int updateTargetPos(double targetPos[], double t)
{
 //   targetPos[0] = targetPos[0]+rand()%20-10;
 //   targetPos[1] = targetPos[1]+rand()%20-10;
 //   targetPos[2] = targetPos[2]-rand()%10+5;
//     cout << "t = " << t <<endl;
     targetPos[0] = (exp(-t / 200) * cos(t/20)+1)*200;
     targetPos[1]  = (exp(-t / 200) * sin(t/20)+1)*200;
     targetPos[2] = (sin(t/20)/2+1)*100;

     //targetPos[0] = 2*t;
     //targetPos[1]  = 2*t;
     //targetPos[2] = 100;

    return 0;
}


//dMatrix NonLinearLSQ(int beaconWindow[], int eventTimeWindow[], double** vWindow, double distWindow[])
dMatrix NonLinearLSQ()
{
//    printf("b= %d t= %d v= %f d= %f\n",beaconWindow[0], tWindow[0],vWindow[0][0], distWindow[0]);
    
    //printf("%f %f %f \n",distWindow[0],distWindow[1],distWindow[2]);
    int n = WINDOW_SIZE;
    int m = 2;
    dMatrix b(n,2);
    dMatrix s(n,2);
    dMatrix myx(3,1);
    /*
    b.resize(7,2);
    s.resize(7,2);
    myx.resize(3, 1);
    */
    //return b;

    for(int i=0; i<n; i++)
    {
	for(int j=0; j<m; j++)
	{
	    int t = beaconWindow[i];
	    b(i,j)= B[t][j];    
	}
	double temp[2] = {0,0};
	for(int j=1; j<i; j++)
	{
	    for(int k=0; k<m; k++)
		temp[k] = temp[k] + tWindow[j-1]*vWindow[j-1][k];
	}
	for (int k=0; k<m; k++)
	    s(i,k) = b(i,k) + temp[k];
    }

    int sizeA = 0;
    for(int i=0; i<n; i++)
	sizeA = sizeA + i;
    dMatrix A(sizeA, m);
    dMatrix C(sizeA, 1); 
    int index = 0;   
    for(int i=0; i<n; i++){
	for(int j=i+1; j<n; j++)
	{
	    for(int k=0; k<m; k++)
	    {
		A(index, k) = s(j,k) - s(i,k) + s(j,k) - s(i,k); 
	    }
	    C(index, 0) = distWindow[i]*distWindow[i] -distWindow[j]*distWindow[j] -s(i,0)*s(i,0) -s(i,1)*s(i,1) + s(j,0)*s(j,0) + s(j,1)*s(j,1);
	    index = index + 1;
	}
    }

    dMatrix x1(m, 1);
    dMatrix temp = ~A * A;
    temp.inv();
    dMatrix temp2 = temp * ~A;
    x1 = temp2 * C;

    dMatrix tz(n,1);

    for(int i=0; i<n; i++)
    {
	double temp3 = distWindow[i]*distWindow[i]-(x1(0,0)-s(i,0))*(x1(0,0)-s(i,0))-(x1(1,0)-s(i,1))*(x1(1,0)-s(i,1));
	int t=beaconWindow[i];
	if(temp3 < 0)
	    tz(i,0) = B[t][2];
	else
	    tz(i,0) = B[t][2]-sqrt(temp3);

    }
    double temp4=0;
    for(int i=0; i<n; i++)
    {
	temp4= temp4 + tz(i,0);
    }
    temp4 = temp4/n;

    double tempDouble;
    tempDouble = x1(0,0);
    myx(0,0)  = tempDouble;
    tempDouble = x1(1,0);
    myx(1,0) = tempDouble;

    myx(2,0) = temp4;
    

    return myx;


}


