#ifndef __yaEkf_hh__
#define __yaEkf_hh__

#define setMatToUnit(M,col_size,val) {\
    uint8_t tts;\
    memset(&(M[0][0]),0,col_size*col_size);\
    for(tts = 0;tts<col_size;tts++)\
        M[tts][tts] = val;\
}

#define MAX_NUM_BEACON 10

#define STATE_VEC_LEN 6

#define OB_VEC_LEN 1

#define NUM_BEACON 9
#define WINDOW_SIZE 7
#define bool uint8_t


//#include <cmatrix>
#include <arm_math.h>
#include <stdint.h>


//uint8_t initEKF(dMatrix initPos);
uint8_t initEKF(arm_matrix_instance_f32 *initPos);

uint8_t initBeacons (uint16_t numRx,const float coordRX[],float mStep);
//bool updateEKF(dMatrix z,int i, double step);
bool updateEKF(arm_matrix_instance_f32 z[], int i, float step);

float calDisFromBeacon(int i,  float x[]);
uint8_t NonLinearLSQ(arm_matrix_instance_f32 *myx);
bool CheckSingularGenerateVirtualDis();

int updateWindows(int b, float d, float t);
int updatePos(arm_matrix_instance_f32 *x);

void buildA(float32_t dt,arm_matrix_instance_f32* cA);
int isMember(int a, int A[]);
int updateTargetPos(float targetPos[], uint8_t t);

uint8_t cholesky(arm_matrix_instance_f32 *, arm_matrix_instance_f32 *);

void dfStateVec(arm_matrix_instance_f32 x, float step,arm_matrix_instance_f32* x1);

void buildH(arm_matrix_instance_f32 *cH,arm_matrix_instance_f32 *x, int i);

void dhmeasVec(arm_matrix_instance_f32 *dz,arm_matrix_instance_f32 x, int i);
#endif
