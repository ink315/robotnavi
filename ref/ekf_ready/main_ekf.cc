/**
 ******************************************************************************
 * @file    Project/STM32F4xx_StdPeriph_Templates/main.c
 * @author  MCD Application Team
 * @version V1.1.0
 * @date    18-January-2013
 * @brief   Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
 *
 * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        http://www.st.com/software_license_agreement_liberty_v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm324xg_eval.h"
#include "stm32f4xx_conf.h"
#include <stdio.h>
#include "stm32f4xx_it.h"
#include "MPU6050.h"
#include "yaEkf6.hh"
#include <stdlib.h>
//#include <string.h>
#include "main.hh"
#include "Msg.hh"
#include "arm_math.h"
#include "math.h"
#include "attitudeSolution.c"
#include "time.h" 
/*
   using namespace std;
   using std::size_t;
   using namespace techsoft;

   using techsoft::epsilon;
   using techsoft::isVecEq;
   */
//using std::exception;

#define arm_matrix_instance_f32 arm_matrix_instance_f32

/** @addtogroup STM32F4xx_StdPeriph_Templates
 * @{
 */

/* Private typedef -----------------------------------------------------------*/
typedef union uFloatInt{
    uint32_t ui;
    float fl;
} floatInt;

typedef union uMsgBuf{
    Msg_t msg;
    uint8_t buf[48];
} MsgBuf;

typedef union uPointMsgBuf{
    Point_msg_t msg;
    uint8_t buf[17];
} PointMsgBuf;


/* Private define ------------------------------------------------------------*/
#define TX_BUF_LEN 256
#define RX_BUF_LEN 256
#define DATA_BUFFER_SIZE 512
#define __IO volatile
#define SZ_BUF_LEN 100
#define SMSG_BUF_LEN 50
#define UART_BUF_SIZE 100
#define MAX_MSG_CNT 10
#define RANGE_FILTER_THRESHOLD 40


#if defined (USE_STM324xG_EVAL)
#define MESSAGE1   "     STM32F40xx     "
#define MESSAGE2   " Device running on  "
#define MESSAGE3   "   STM324xG-EVAL    "

#else /* USE_STM324x7I_EVAL */
#define MESSAGE1   "     STM32F427x     "
#define MESSAGE2   " Device running on  "
#define MESSAGE3   "   STM324x7I-EVAL   "
#endif

#define WINDOW_SIZE 7
/* Private macro -------------------------------------------------------------*/
uint8_t range_data[DATA_BUFFER_SIZE];
//CircularBuffer rangeBuffer(DATA_BUFFER_SIZE,range_data);

/* Private variables ---------------------------------------------------------*/
uint8_t FlagRxUart1;
uint8_t FlagRxUart2;
volatile uint8_t RxBufferOfUart1[BUFFER_SIZE];

volatile uint8_t ID,Seq,pktReady = 0,wrongPkt = 0;
volatile uint8_t flushToFlash,wrongCoordPkt = 0,CoordPktReady =0,offsetPktReady =0 ,ADCReady =0;
volatile uint8_t IMUReady;
volatile float distCM,beaconX,beaconY,beaconZ,thisOffset;
volatile uint16_t firstRisingEdge;
/*public valurable-------------------------------------------------------------*/
float beaconCoordinFlash[MAX_NUM_BEACON*4];
__IO uint8_t numBeacon = 9;
__IO uint8_t numBeaconToWrite;
float beaconCoord[MAX_NUM_BEACON*3]={0, 0, 280, 100, 0, 300, 0, 100, 300, 200, 0, 300, 0, 200, 300, 100, 100, 300, 200, 100, 300, 300, 100, 300, 200, 300, 300};

float offset[MAX_NUM_BEACON];//{16,36,45,45,45,45,0,0,0,0,0,0};
sRecvStage curStage,uart1Stage;
/* Private variables ---------------------------------------------------------*/
static __IO uint32_t uwTimingDelay;
RCC_ClocksTypeDef    RCC_Clocks;
TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
TIM_OCInitTypeDef  TIM_OCInitStructure;
//volatile uint16_t CCR1_Val = 40961;
/* Private function prototypes -----------------------------------------------*/
static void Delay(__IO uint32_t nTime);
uint8_t lastSeq;
float distV[MAX_NUM_BEACON];
uint16_t edgeV[MAX_NUM_BEACON];

__IO uint16_t rawUSValue[ADC_VALUE_LEN];
float lastPos[2][3] ={{0,0,0},{0,0,0}};
float targetPos[] ={100.0f,100.0f,100.0f};
int simID = 0;
#define MAX_SPI_BUF_LEN 100
#define MAX_USART_BUF_LEN 40
__IO uint8_t spiBuf[2][MAX_SPI_BUF_LEN];
__IO uint8_t *spiInBuf;
__IO uint8_t *spiOutBuf;
__IO uint16_t spiInBufLen,spiOutBufLen;


__IO uint8_t USARTBuf[2][MAX_USART_BUF_LEN];
__IO uint8_t *USARTInBuf;
__IO uint8_t *USARTOutBuf;
__IO uint8_t USARTInBufLen,USARTOutBufLen;
__IO uint8_t USARTpktReady;
__IO uint8_t badSPI;
__IO floatInt converter;


arm_matrix_instance_f32 xl;
float32_t xlm[3];

/* Private functions ---------------------------------------------------------*/
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#endif

#ifdef __SIMULATOR__
#define PUTCHAR_PROTOTYPE int hello(int ch, FILE *f)
#else
//#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

uint32_t flashStartSectorAddr = 0;

int main(void){
    uint16_t loop;
    uint8_t beaconID[MAX_NUM_BEACON];
    for(loop=0;loop<0xffff;loop++){
        uint16_t loop2;
        for(loop2=0;loop2<0xf;loop2++){
            asm("nop");
        }
    }
    curStage = WAITING;
    uart1Stage = WAITING;
    spiInBufLen = 0;
    spiOutBufLen = 0;
    STM_EVAL_LEDInit(LED1);
    STM_EVAL_LEDInit(LED2);
    arm_mat_init_f32(&xl,3,1,xlm);
    //load beaconCoord from the flash
    /* Enable the flash control register access */

    uint32_t ReadAddr = ADDR_FLASH_SECTOR_4;
    //numBeacon = (uint8_t)(*(volatile uint32_t*)ReadAddr);
    ReadAddr += 4;
    uint8_t i,j;
    for(i=0;i<numBeacon;i++){
        beaconID[i] = (uint8_t)(*(volatile uint32_t*)ReadAddr);
        ReadAddr += 4;
        for(j=0;j<3;j++){
            converter.ui=*(volatile uint32_t*)ReadAddr;
            //beaconCoord[i*3+j] = converter.fl;
            ReadAddr += 4;
        }
        converter.ui = *(volatile uint32_t*)ReadAddr;
        offset[i]=converter.fl;
        ReadAddr += 4;
    }
    numBeaconToWrite = 0;
    SPI_Config();
    SPI_Cmd(EVAL_SPI,ENABLE);
    SPI_I2S_ITConfig(EVAL_SPI,SPI_I2S_IT_RXNE,ENABLE);
    spiInBuf =&(spiBuf[0][0]);
    spiOutBuf =&(spiBuf[1][0]) ;
    USARTInBuf = &(USARTBuf[0][0]);
    USARTOutBuf = &(USARTBuf[1][0]);
    //memset((void*)spiBuf,0,2*MAX_SPI_BUF_LEN);
    /*
       spiInBuf = spiBuf[0];
       spiOutBuf = spiBuf[1];
       */
#ifdef ENABLE_ADC
    usADCInit();
#endif
    //SPI_I2S_ITConfig(EVAL_SPI,SPI_I2S_IT_TXE,DISABLE);
    USART_Config(COM1);
    USART_NVIC_Config(COM1);
    USART_ITConfig(EVAL_COM1,USART_IT_RXNE,ENABLE);
#ifdef DEBUG_FLASH
    printf("numBeaconInFlash = %u\n\r",numBeacon);
    for(int k=0;k<numBeacon;k++){
        printf("%u:%.1f %.1f %.1f %.1f\n\r",beaconID[k],beaconCoord[k*3],beaconCoord[k*3+1],beaconCoord[k*3+2],offset[k]);
    }
#endif

    volatile unsigned char szBuffer[SZ_BUF_LEN];
    volatile uint8_t sMsgBuffer[SMSG_BUF_LEN];
    volatile uint16_t sBufferSize = 0;
    volatile uint8_t hasNew = 0;
    volatile uint8_t IMUReady = 0;

    volatile uint8_t coordBuffer[SZ_BUF_LEN];
    volatile uint16_t coordBufLen = 0;

    printf(" LBS ver 1\n\r");

    extern int confidence;
    extern int N;

    extern int NBeacon;
    extern int confidence;
    extern arm_matrix_instance_f32 z;
    extern arm_matrix_instance_f32 R;
    extern arm_matrix_instance_f32 Q;
    extern arm_matrix_instance_f32 P;
    extern arm_matrix_instance_f32 X;


    float step=3.0;
    uint8_t roundCount =0;
    pktReady = 0;
#ifdef ENABLE_IMU
    uint16_t PrescalerValue = (uint16_t) ((SystemCoreClock / 2) / 6000000) - 1;
#endif


#ifdef __SIMULATOR__
    ;
#else

    //init the MPU6050
    
    MPU6050_I2C_Init();
    if(!MPU6050_TestConnection() ){
        while(1);//connection failed
    }
    MPU6050_Initialize();
#endif
    initBeacons(numBeacon,beaconCoord,3);
    initTimer();

    lastSeq = 0;
    uint16_t cumulatedPos =0,filted_cnt =0;
    for(int k = 0;k<MAX_NUM_BEACON;k++){
        edgeV[k] = 0;
        distV[k] = -1.0f;
    }
    STM_EVAL_LEDOn(LED1);
    STM_EVAL_LEDOn(LED2);

    MsgBuf thisMsgBuf;
    PointMsgBuf thisPointMsgBuf;
    badSPI = 0;

    /* Infinite loop */
    while (1){
        //handle the rangebuffer
        if(IMUReady){
            IMUReady = 0;
            int16_t rawData[6];
            MPU6050_GetRawAccelGyro(rawData);
            //TODO
        }
        if(badSPI){
            badSPI = 0;
            SPI_Cmd(EVAL_SPI,DISABLE);
            SPI_Config();
            SPI_Cmd(EVAL_SPI,ENABLE);
            SPI_I2S_ITConfig(EVAL_SPI,SPI_I2S_IT_RXNE,ENABLE);
        }
        if(ADCReady){
            uint32_t USSum = 0;
            uint16_t i;
            for(i= 0;i<ADC_VALUE_LEN;i++){
                USSum += rawUSValue[i];
            }
            USSum/= i;
            if(USSum >3000){
                //if capture us intensity then toggle the led;
                //TODO replace the led toggle with sync algorthm
                //TODO fifo quee in the usar
                //STM_EVAL_LEDToggle(LED2);
            }
        }
#ifdef __SIMULATOR__
        if(1){
#else                
        if(pktReady){

            pktReady = 0;
            uint16_t i,j;
            j=0;
            for(i=0;i<spiOutBufLen-1;i++){
                if(spiOutBuf[i]==0x7E)
                    continue;
                else if(spiOutBuf[i]==0x7D&&spiOutBuf[i+1]==0x5D){
                    thisMsgBuf.buf[j++] = 0x7D;
                    i++;
                }else if(spiOutBuf[i]==0x7D&&spiOutBuf[i+1]==0x5E){
                    thisMsgBuf.buf[j++] = 0x7E;
                    i++;
                }else{
                    thisMsgBuf.buf[j++] = spiOutBuf[i];
                }
            }
            if(j!=sizeof(Msg_t)){
                printf("wrong spi pkt\n\r");
                continue;
            }
            distCM = (thisMsgBuf.msg.risingEdge[0]*34.6/250)+offset[thisMsgBuf.msg.ID-1];//-offset[ID-1];
#endif
            //updateTargetPos(targetPos, roundCount);
            //printf("targetPos= %f, %f, %f %d\n\r", targetPos[0], targetPos[1], targetPos[2], roundCount);
            //distCM = calDisFromBeacon(thisMsgBuf.msg.ID-1, targetPos);
            //targetPos[0] +=0.5;
            //targetPos[1] +=0.5;
#ifdef __SIMULATOR__
            simID = (simID +1) %numBeacon;              // 0-9;
            //if(rand()%11 >9)
            //    distCM = calDisFromBeacon(simID, targetPos) + (float)(rand()%101)-50.0 ;
            //else
            distCM = calDisFromBeacon(simID, targetPos) + (float)(rand()%11)-5.0 ;
            //distCM = calDisFromBeacon(simID, targetPos) ;
            printf("Pos:x=%.2f\ty=%.2f\tz=%.2f\t\n\r", targetPos[0], targetPos[1], targetPos[2]);
            targetPos[0] +=(float)(rand()%15)-7.0;
            targetPos[1] +=(float)(rand()%15)-7.0;

            //printf("dist = %f, ID= %d\n\r", distCM, simID);
            ID = simID;                           //0-9
#else
            ID = thisMsgBuf.msg.ID+1;
#endif
            //STM_EVAL_LEDToggle(LED1);
#ifdef DEBUG_LSQ_DIS_TAB
            if(Seq!=lastSeq){
                printf("%03u ",lastSeq);
                //print(txBuf);
                for(int k =1;k<MAX_NUM_BEACON;k++){
                    if(distV[k]>0.0f){
                        sprintf(txBuf,"%02u:%04.2f ",k,distV[k]);
                        printf(txBuf,"%02u:%06u ",k,edgeV[k]);
                        print(txBuf);
                    }else{
                        print("          ");
                    }
                }
                print("\n\r");
            }
            lastSeq = Seq;
            edgeV[ID] = firstRisingEdge;
            distV[ID] = distCM;
#endif
            //int ri = rand()%15;
            if(distCM >10 && distCM <800){
#ifdef DIST_RAW_RESULT
                rawPrint(1,(float)distCM,(float)0,(float)0,thisMsgBuf.msg.ID);
#endif
/*
                if(filted_cnt >20){
                    filted_cnt = 0;
                    cumulatedPos = 0;
                }
                if(cumulatedPos<10){
                    cumulatedPos ++;
                }
                if(cumulatedPos>=10){
                    float estPos[3];
                    uint8_t k;
                    float temp =0;
                    for(k=0;k<3;k++){
                        estPos[k] = lastPos[1][k]+lastPos[1][k]-lastPos[0][k];
                        temp+=(estPos[k]-beaconCoord[(ID-1)*3+k])*(estPos[k]-beaconCoord[(ID-1)*3+k]);
                    }
                    float estDist = sqrt(temp);
                    if(abs(estDist -distCM)>RANGE_FILTER_THRESHOLD){
                        filted_cnt++;
                        //STM_EVAL_LEDToggle(LED2);
                        continue;
                    }
                    filted_cnt = 0;
                }
 */  

                z.pData[0]=distCM;
                updateWindows(ID,distCM, step);   //0-9
                //CheckSingularGenerateVirtualDis();
                if(roundCount<= WINDOW_SIZE){
                    roundCount++;
                }
                if(roundCount == WINDOW_SIZE ){
                    //roundCount++;
                    NonLinearLSQ(&xl);
                    updatePos(&xl);
                    initEKF(&xl);  //initialize EKF
                }
                else if(roundCount > WINDOW_SIZE) {
                    updateEKF(&z, ID, step);
                    //roundCount++;
                    NonLinearLSQ(&xl);
                    updatePos(&xl);
                    float ekf_x = X.pData[0];//p(0,0);
                    float ekf_y = X.pData[1];//(1,0);
                    float ekf_z = X.pData[2];//(2,0);
                    
                    float lsq_x = xl.pData[0];//(0,0);
                    float lsq_y = xl.pData[1];//(1,0);
                    float lsq_z = xl.pData[2];//(2,0);
                    lastPos[0][0]= lastPos[1][0];
                    lastPos[0][1]= lastPos[1][1];
                    lastPos[0][2]= lastPos[1][2];
                    lastPos[1][0]= (float)lsq_x;
                    lastPos[1][1]= (float)lsq_y;
                    lastPos[1][2]= (float)lsq_z;
#ifdef LSQ_RAW_RESULT
//                    uint8_t confidence = 128;
//                    rawPrint(0,(float)lsq_x,(float)lsq_y,(float)lsq_z,confidence);
#endif
#ifdef LSQ_ASCII_RESULT
                    printf("lsq:x= %.2f\ty= %.2f\tz=%.2f\t\n\r", lsq_x,lsq_y,lsq_z);
                    printf("ekf:x= %.2f\ty= %.2f\tz=%.2f\t\n\r", ekf_x,ekf_y,ekf_z);
#endif
                }
            }
        }
        if(USARTpktReady){
            USARTpktReady = 0;
            uint8_t i,j=0;
            for(i=0;i<USARTOutBufLen-1;i++){
                if(USARTOutBuf[i]==0x7E)
                    continue;
                else if(USARTOutBuf[i]==0x7D && USARTOutBuf[i+1]==0x5D){
                    thisPointMsgBuf.buf[j++] = 0x7D;
                    i++;
                }else if(USARTOutBuf[i]==0x7D && USARTOutBuf[i+1]==0x5E){
                    thisPointMsgBuf.buf[j++] = 0x7E;
                    i++;
                }else{
                    thisPointMsgBuf.buf[j++] = USARTOutBuf[i];
                }
            }
            if(j!=sizeof(Point_msg_t)){
                printf("wrong usart pkt %u %u\n\r",j,sizeof(Point_msg_t));
                continue;
            }

            if(thisPointMsgBuf.msg.ID!=255){
                beaconID[numBeaconToWrite] = thisPointMsgBuf.msg.ID;
                offset[numBeaconToWrite] = thisPointMsgBuf.msg.offset;
                uint8_t ptr = numBeaconToWrite*3;
                beaconCoord[ptr++] = thisPointMsgBuf.msg.x;
                beaconCoord[ptr++] = thisPointMsgBuf.msg.y;
                beaconCoord[ptr++] = thisPointMsgBuf.msg.z;
                numBeaconToWrite++;
            }
            else if(thisPointMsgBuf.msg.ID==255 && numBeaconToWrite>0){
                printf("numBeaconToWrite=%u\n" , numBeaconToWrite);
                uint8_t i,j;
                for(i=0;i<numBeaconToWrite;i++){
                    printf("%u:%.1f %.1f %.1f %.1f\n\r",beaconID[i],beaconCoord[i*3],beaconCoord[i*3+1],beaconCoord[i*3+2],offset[i]);
                }
                FLASH_Unlock();
                FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR|FLASH_FLAG_PGSERR);
                if(FLASH_EraseSector(FLASH_Sector_4,VoltageRange_3)!=FLASH_COMPLETE){
                    while(1){
                    }
                }
                uint32_t writeAddr = ADDR_FLASH_SECTOR_4;
                //write numbeacon
                if(FLASH_ProgramWord(writeAddr,(uint32_t)numBeaconToWrite)!=FLASH_COMPLETE){
                    while(1){}
                }
                writeAddr+= 4;
                for(i=0;i<numBeaconToWrite;i++){
                    //write ID
                    if(FLASH_ProgramWord(writeAddr,(uint32_t)beaconID[i])!=FLASH_COMPLETE){
                        while(1){}
                    }
                    writeAddr+= 4;
                    //write coord
                    for(j=0;j<3;j++){
                        converter.fl=beaconCoord[i*3+j];
                        if(FLASH_ProgramWord(writeAddr,converter.ui)!=FLASH_COMPLETE){
                            while(1){}
                        }
                        writeAddr+= 4;
                    }
                    converter.fl=offset[i];
                    if(FLASH_ProgramWord(writeAddr,converter.ui)!=FLASH_COMPLETE){
                        while(1){}
                    }
                    writeAddr+= 4;
                }
                numBeacon = numBeaconToWrite;
                numBeaconToWrite = 0;
                FLASH_Lock();
            }
        }
    }//finish infinit loop
}

/**
 * @brief  Inserts a delay time.
 * @param  nTime: specifies the delay time length, in milliseconds.
 * @retval None
 */
void Delay(__IO uint32_t nTime) {
    uwTimingDelay = nTime;
    while(uwTimingDelay != 0);
}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void){
    if (uwTimingDelay != 0x00){
        uwTimingDelay--;
    }
}



static void rawPrint(uint8_t id, float x,float y,float z,uint8_t sConfidence){
    union{
#pragma pack(1)
        struct {
            uint8_t id;
            uint8_t sConfidence;
            float x;
            float y;
            float z;

        } a;
#pragma pack()
        struct {
            uint8_t data[14];
        } b;
    } shared;
    shared.a.id = id;
    shared.a.x = x;
    shared.a.y = y;
    shared.a.z = z;
    shared.a.sConfidence = sConfidence;
    uint8_t outBuf[40];
    uint8_t inIdx = 0,outIdx = 0;
    outBuf[outIdx++] = 0x7E;
    for(inIdx=0;inIdx<14;inIdx++){
        if(shared.b.data[inIdx]==0x7E){
            outBuf[outIdx++] = 0x7D;
            outBuf[outIdx++] = 0x5E;

        }else if(shared.b.data[inIdx]==0x7D){
            outBuf[outIdx++] = 0x7D;
            outBuf[outIdx++] = 0x5D;

        }else{
            outBuf[outIdx++] = shared.b.data[inIdx];

        }
    }
    outBuf[outIdx++] = 0x7E;
#ifdef __IAR_SYSTEMS_ICC__
    //printf("%s",outBuf);
    USART1_Putsn((char*)outBuf,outIdx);
#else
    write(stdout,outBuf,outIdx);
#endif
}




/** @addtogroup Template_Project
 * @{
 */

PUTCHAR_PROTOTYPE
{
    /* Place your implementation of fputc here */
    /* e.g. write a character to the USART */
    USART_SendData(EVAL_COM1, (uint8_t) ch);

    /* Loop until the end of transmission */
    while (USART_GetFlagStatus(EVAL_COM1, USART_FLAG_TC) == RESET)
    {}

    return ch;
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif
/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
