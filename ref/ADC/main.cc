/**
 ******************************************************************************
 * @file    Project/STM32F4xx_StdPeriph_Templates/main.c
 * @author  MCD Application Team
 * @version V1.1.0
 * @date    18-January-2013
 * @brief   Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
 *
 * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        http://www.st.com/software_license_agreement_liberty_v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm324xg_eval.h"
#include "stm32f4xx_conf.h"
#include <stdio.h>
#include "CircularBuffer.hh"
#include "decodeRange.hh"
#include "stm32f4xx_it.h"
#include "yaEkf6.hh"
#include <stdlib.h>
//#include <string.h>
#include "main.hh"
#include "Msg.hh"
//#include "arm_math.h"
#include "math.h"
#include "BSP_ADC2DMA.h"

using namespace std;
using std::size_t;
using namespace techsoft;

using techsoft::epsilon;
using techsoft::isVecEq;

//using std::exception;


/** @addtogroup STM32F4xx_StdPeriph_Templates
 * @{
 */

/* Private typedef -----------------------------------------------------------*/
typedef struct {
    uint8_t uid;
    uint8_t gid;
    float coord[3];
} coordPack;

typedef union uMsgBuf{
    Msg_t msg;
    uint8_t buf[48];
} MsgBuf;



/* Private define ------------------------------------------------------------*/
#define TX_BUF_LEN 256
#define RX_BUF_LEN 256
#define DATA_BUFFER_SIZE 512
#define __IO volatile
#define SZ_BUF_LEN 100
#define SMSG_BUF_LEN 50
#define UART_BUF_SIZE 100
#define MAX_MSG_CNT 10
#define RANGE_FILTER_THRESHOLD 40



#if defined (USE_STM324xG_EVAL)
#define MESSAGE1   "     STM32F40xx     "
#define MESSAGE2   " Device running on  "
#define MESSAGE3   "   STM324xG-EVAL    "

#else /* USE_STM324x7I_EVAL */
#define MESSAGE1   "     STM32F427x     "
#define MESSAGE2   " Device running on  "
#define MESSAGE3   "  STM324x7I-EVAL    "
#endif

#define MAX_NUM_BEACON 10
/* Private macro -------------------------------------------------------------*/
uint8_t range_data[DATA_BUFFER_SIZE];
//CircularBuffer rangeBuffer(DATA_BUFFER_SIZE,range_data);

/* Private variables ---------------------------------------------------------*/
uint8_t FlagRxUart1;
uint8_t FlagRxUart2;
volatile uint8_t RxBufferOfUart1[BUFFER_SIZE];

volatile uint8_t ID,Seq,pktReady = 0,wrongPkt = 0;
volatile uint8_t flushToFlash,beaconID,wrongCoordPkt = 0,CoordPktReady =0,offsetPktReady =0 ;
volatile float distCM,beaconX,beaconY,beaconZ,thisOffset;
volatile uint16_t firstRisingEdge;
static char txBuf[UART_BUF_SIZE];
/*public valurable-------------------------------------------------------------*/
float beaconCoordinFlash[MAX_NUM_BEACON*4];
uint8_t numBeacon,maxBeaconId;
float beaconCoord[MAX_NUM_BEACON*3];
float offset[MAX_NUM_BEACON];//{16,36,45,45,45,45,0,0,0,0,0,0};
sRecvStage curStage,uart1Stage;
/* Private variables ---------------------------------------------------------*/
static __IO uint32_t uwTimingDelay;
RCC_ClocksTypeDef    RCC_Clocks;

/* Private function prototypes -----------------------------------------------*/
static void Delay(__IO uint32_t nTime);
uint8_t lastSeq;
float distV[MAX_NUM_BEACON];
uint16_t edgeV[MAX_NUM_BEACON];

float lastPos[2][3] ={{0,0,0},{0,0,0}};

/* Private functions ---------------------------------------------------------*/
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

uint32_t flashStartSectorAddr = 0;

int main(void){


    curStage = WAITING;
    uart1Stage = WAITING;
    maxBeaconId = 0;
    STM_EVAL_LEDInit(LED1);
    STM_EVAL_LEDInit(LED2);
    BSP_ADC_Init();

    uint32_t zz;
    uint32_t tmpdelay;
    while(1)
    {
	for(zz=0;zz<ADC_BuffLength;zz++)
	{
		if(uhADCxConvertedValue[zz]>3000)
		{


                  STM_EVAL_LEDOff(LED1);
                  tmpdelay=10000;
                  while(tmpdelay> 0)tmpdelay--;
	}
		STM_EVAL_LEDOn(LED1);
	}

     }
    //load beaconCoord from the flash
    /* Enable the flash control register access */
    uint32_t ReadAddr = ADDR_FLASH_SECTOR_3;
    numBeacon =  (uint8_t)(*(volatile uint32_t*)ReadAddr);
    ReadAddr += 4;
    if(numBeacon>MAX_NUM_BEACON)
        numBeacon = MAX_NUM_BEACON;
    for(int i=0;i<numBeacon*3;i++){
        beaconCoord[i] = *(volatile uint32_t*)ReadAddr;
        ReadAddr += 4;
    }
    for(int i=0;i<numBeacon;i++){
        offset[i]  = *(volatile uint32_t*)ReadAddr;
        ReadAddr += 4;
    }
    //memcpy((void*)beaconCoord,(void*)(ADDR_FLASH_SECTOR_3+sizeof(uint32_t)),16*3);
#ifdef __IAR_SYSTEMS_ICC__
#ifdef __USING_I2C__
    SPI_Config();
    SPI_Cmd(EVAL_SPI,ENABLE);
    SPI_I2S_ITConfig(EVAL_SPI,SPI_I2S_IT_RXNE,ENABLE);
    //SPI_I2S_ITConfig(EVAL_SPI,SPI_I2S_IT_TXE,DISABLE);
#else
    USART_Config(COM2);
    USART_NVIC_Config(COM2);
    USART_ITConfig(EVAL_COM2,USART_IT_RXNE,ENABLE);
#endif
    USART_Config(COM1);
    USART_NVIC_Config(COM1);
    USART_ITConfig(EVAL_COM1,USART_IT_RXNE,ENABLE);
#ifdef DEBUG_FLASH
    sprintf(txBuf,"numBeaconInFlash = %u\n\r",numBeacon);
    print(txBuf);
    for(int k=0;k<numBeacon;k++){
         sprintf(txBuf,"%f %f %f\n\r",beaconCoord[k*3],beaconCoord[k*3+1],beaconCoord[k*3+2]);
         print(txBuf);
    }
#endif
#else
    if(RS232_OpenComport(US_cport_nr,bdrate,mode)){
        printf("Can not open comport %s with bps %d\n",comports[US_cport_nr],bdrate);
        return (0);
    }
#endif

    volatile unsigned char szBuffer[SZ_BUF_LEN];
    volatile uint8_t sMsgBuffer[SMSG_BUF_LEN];
    volatile uint16_t sBufferSize = 0;
    volatile uint8_t hasNew = 0;

    volatile uint8_t coordBuffer[SZ_BUF_LEN];
    volatile uint16_t coordBufLen = 0;

#ifdef DEBUG
    sprintf(txBuf," LBS ver 1\n\r");
    print(txBuf);
#endif

    extern int NBeacon;
    extern int confidence;
    extern int N;

    double targetPos[3] ={100,100,100};
    extern int NBeacon;
    extern int confidence;
    extern dMatrix z;
    extern dMatrix X;
    extern dMatrix xl;    //3*1
    extern dMatrix R;
    extern dMatrix Q;
    extern dMatrix P;

    uint8_t roundCount =0;
    uint16_t dwBytesRead = 0;
    pktReady = 1;

    initBeacons(NBeacon,beaconCoord,3);

    lastSeq = 0;
    uint16_t cumulatedPos =0,filted_cnt =0;
    for(int k = 0;k<MAX_NUM_BEACON;k++){
        edgeV[k] = 0;
        distV[k] = -1.0f;
    }
    STM_EVAL_LEDOn(LED1);
    STM_EVAL_LEDOn(LED2);
    /* Infinite loop */
    while (1){
        //handle the rangebuffer
        if(pktReady){
       // if(0){
            pktReady = 0;
            if(wrongPkt){
                wrongPkt = 0;
                sprintf(txBuf,"wrong pkt:%u \n\r",wrongPkt);
                print(txBuf);
                continue;
            }
            STM_EVAL_LEDToggle(LED1);
            distCM = (firstRisingEdge*34.6/250)-offset[ID-1];
#ifdef DEBUG_LEVLE_2
            sprintf(txBuf,"%u, %f",ID,distCM);
            print(txBuf);
            //sprintf(txBuf,"id =%u Seq = %u num_r=%u num_f=%u first_r=%u\n\r",rangeMsg.msg.ID, rangeMsg.msg.Seq ,rangeMsg.msg.numRising,rangeMsg.msg.numFalling,rangeMsg.msg.risingEdge[0]);
            //print(txBuf);
#endif
#ifdef DEBUG_LSQ_DIS
            if(Seq!=lastSeq){
                sprintf(txBuf,"%03u ",lastSeq);
                //print(txBuf);
                for(int k =1;k<MAX_NUM_BEACON;k++){
                    if(distV[k]>0.0f){
                        sprintf(txBuf,"%02u:%04.2f ",k,distV[k]);
                        printf(txBuf,"%02u:%06u ",k,edgeV[k]);
                        print(txBuf);
                    }else{
                        print("          ");
                    }
                }
                print("\n\r");
            }
            lastSeq = Seq;
            edgeV[ID] = firstRisingEdge;
            distV[ID] = distCM;
#endif
            //int ri = rand()%15;
            if(distCM >10 && distCM <500 && ID>0 && ID<7){

//                sprintf(txBuf,"%u %.2f\n\r", ID, distCM);
//                print(txBuf);
                if(filted_cnt >20){
                    filted_cnt = 0;
                    cumulatedPos = 0;
                }
                if(cumulatedPos<10){
                    cumulatedPos ++;
                }
                if(cumulatedPos>=10){
                    float estPos[3];
                    uint8_t k;
                    float temp =0;
                    for(k=0;k<3;k++){
                        estPos[k] = lastPos[1][k]+lastPos[1][k]-lastPos[0][k];
                        temp+=(estPos[k]-beaconCoord[(ID-1)*3+k])*(estPos[k]-beaconCoord[(ID-1)*3+k]);
                    }
                    float estDist = sqrt(temp);
                    if(abs(estDist -distCM)>RANGE_FILTER_THRESHOLD){
#ifdef DEBUG_LSQ_DIS
                        sprintf(txBuf,"%u %f filted",ID,distCM);
                        print(txBuf);
#endif
                        filted_cnt++;
                        STM_EVAL_LEDToggle(LED2);
                        continue;
                    }
                    filted_cnt = 0;
                }
                dMatrix z(OB_VEC_LEN,OB_VEC_LEN);
                z(0,0)=distCM;
                updateWindows(ID-1,double(distCM),step);
                CheckSingularGenerateVirtualDis();
                if(roundCount<= WINDOW_SIZE){
                    roundCount++;
                }

                if(roundCount == WINDOW_SIZE ){
                    xl= NonLinearLSQ();
                    updatePos(xl);
                    //initEKF(xl);  //initialize EKF
                }
                else if(roundCount > WINDOW_SIZE) {
                    //updateEKF(z, ID-1, step);
                    xl= NonLinearLSQ();
                    updatePos(xl);
                    double ekf_x = X(0,0);
                    double ekf_y = X(1,0);
                    double ekf_z = X(2,0);
                    double lsq_x = xl(0,0);
                    double lsq_y = xl(1,0);
                    double lsq_z = xl(2,0);
                    lastPos[0][0]= lastPos[1][0];
                    lastPos[0][1]= lastPos[1][1];
                    lastPos[0][2]= lastPos[1][2];
                    lastPos[1][0]= (float)lsq_x;
                    lastPos[1][1]= (float)lsq_y;
                    lastPos[1][2]= (float)lsq_z;
#ifdef DEBUG
                    //sprintf(txBuf,"ekf:x= %lf y= %lf z= %lf\n\r", ekf_x,ekf_y,ekf_z);
                    //print(txBuf);

#endif
#ifdef LSQ_RAW_RESULT
                    uint8_t confidence = 128;
                    rawPrint(0,(float)lsq_x,(float)lsq_y,(float)lsq_z,confidence);
#endif
#ifdef DEBUG_LSQ_RESULT
                    sprintf(txBuf,"lsq:x= %.2lf y= %.2lf z= %.2lf\n\r", lsq_x,lsq_y,lsq_z);
                    print(txBuf);
#endif
                }
            }
        }
        if(CoordPktReady){
        //if(0){
            STM_EVAL_LEDToggle(LED2);
            CoordPktReady = 0;
            uint8_t beaconIdx =(beaconID -1)*3;
            beaconCoord[beaconIdx++] = beaconX;
            beaconCoord[beaconIdx++] = beaconY;
            beaconCoord[beaconIdx++] = beaconZ;
            if(beaconID>maxBeaconId)
                maxBeaconId = beaconID ;
            sprintf(txBuf,"%u %.2lf  %.2lf  %.2lf\n\r", beaconID,beaconX,beaconY,beaconZ);
            print(txBuf);
        }
        if(offsetPktReady){
          if(beaconID == 1)
            offsetPktReady = 0;
            offset[beaconID-1] = thisOffset;
        }
        if(flushToFlash){
             flushToFlash = 0;
             numBeacon = maxBeaconId;
             if(numBeacon > MAX_NUM_BEACON)
                 numBeacon = MAX_NUM_BEACON;
             FLASH_Unlock();
             FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR |
                     FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR|FLASH_FLAG_PGSERR);
             if(FLASH_EraseSector(FLASH_Sector_3,VoltageRange_3)!=FLASH_COMPLETE){
                 while(1){
                 }
             }
             uint32_t *ptr;
             uint32_t writeAddr = ADDR_FLASH_SECTOR_3;
             if(FLASH_ProgramWord(writeAddr,(uint32_t)numBeacon)!=FLASH_COMPLETE){
                 while(1){}
             }
             writeAddr+=4;
             for(int i=0;i<numBeacon*3;i++){
                 if(FLASH_ProgramWord(writeAddr,beaconCoord[i])!=FLASH_COMPLETE){
                     while(1){}
                 }
                 writeAddr+= 4;
             }
             for(int i=0;i<numBeacon;i++){
                 if(FLASH_ProgramWord(writeAddr,offset[i])!=FLASH_COMPLETE){
                     while(1){}
                 }
                 writeAddr+= 4;
             }
             maxBeaconId = 0;
             FLASH_Lock();
        }
    }//finish range handler
}

/**
 * @brief  Inserts a delay time.
 * @param  nTime: specifies the delay time length, in milliseconds.
 * @retval None
 */
void Delay(__IO uint32_t nTime) {
    uwTimingDelay = nTime;

    while(uwTimingDelay != 0);
}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{
    if (uwTimingDelay != 0x00){
        uwTimingDelay--;
    }
}



static void SPI_NVIC_Config(void){

}

static void rawPrint(uint8_t id, float x,float y,float z,uint8_t sConfidence){
    union{
#ifdef __IAR_SYSTEMS_ICC__
#pragma pack(1)
#endif
        struct {
            uint8_t id;
            uint8_t sConfidence;
            float x;
            float y;
            float z;

        } a;
#ifdef __IAR_SYSTEMS_ICC__
#pragma pack()
#endif
        struct {
            uint8_t data[14];
        } b;
    } shared;
    shared.a.id = id;
    shared.a.x = x;
    shared.a.y = y;
    shared.a.z = z;
    shared.a.sConfidence = sConfidence;
    uint8_t outBuf[40];
    uint8_t inIdx = 0,outIdx = 0;
    outBuf[outIdx++] = 0x7E;
    for(inIdx=0;inIdx<14;inIdx++){
        if(shared.b.data[inIdx]==0x7E){
            outBuf[outIdx++] = 0x7D;
            outBuf[outIdx++] = 0x5E;

        }else if(shared.b.data[inIdx]==0x7D){
            outBuf[outIdx++] = 0x7D;
            outBuf[outIdx++] = 0x5D;

        }else{
            outBuf[outIdx++] = shared.b.data[inIdx];

        }
    }
    outBuf[outIdx++] = 0x7E;
#ifdef __IAR_SYSTEMS_ICC__
    //printf("%s",outBuf);
    USART1_Putsn((char*)outBuf,outIdx);
#else
    write(stdout,outBuf,outIdx);
#endif
}


static void SPI_Config(void){
    GPIO_InitTypeDef GPIO_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    /* Peripheral Clock Enable -------------------------------------------------*/
    /* Enable the SPI clock */
    EVAL_SPI_CLK_INIT(EVAL_SPI_CLK, ENABLE);

    /* Enable GPIO clocks */
    RCC_AHB1PeriphClockCmd(EVAL_SPI_SCK_GPIO_CLK | EVAL_SPI_MISO_GPIO_CLK | EVAL_SPI_MOSI_GPIO_CLK, ENABLE);

    /* SPI GPIO Configuration --------------------------------------------------*/
    /* GPIO Deinitialisation */
    /*
       GPIO_DeInit(EVAL_SPI_SCK_GPIO_PORT);
       GPIO_DeInit(EVAL_SPI_MISO_GPIO_PORT);
       GPIO_DeInit(EVAL_SPI_MOSI_GPIO_PORT);
       */

    /* Connect SPI pins to AF5 */
    GPIO_PinAFConfig(EVAL_SPI_SCK_GPIO_PORT, EVAL_SPI_SCK_SOURCE, EVAL_SPI_SCK_AF);
    GPIO_PinAFConfig(EVAL_SPI_MISO_GPIO_PORT, EVAL_SPI_MISO_SOURCE, EVAL_SPI_MISO_AF);
    GPIO_PinAFConfig(EVAL_SPI_NSS_GPIO_PORT, EVAL_SPI_MOSI_SOURCE, EVAL_SPI_MOSI_AF);
    // GPIO_PinAFConfig(EVAL_SPI_NSS, EVAL_SPI_MOSI_SOURCE, EVAL_SPI_MOSI_AF);

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;

    /* SPI SCK pin configuration */
    GPIO_InitStructure.GPIO_Pin = EVAL_SPI_SCK_PIN;
    GPIO_Init(EVAL_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

    /* SPI  MISO pin configuration */
    GPIO_InitStructure.GPIO_Pin =  EVAL_SPI_MISO_PIN;
    GPIO_Init(EVAL_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

    /* SPI  MOSI pin configuration */
    GPIO_InitStructure.GPIO_Pin =  EVAL_SPI_MOSI_PIN;
    GPIO_Init(EVAL_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

    /* SPI configuration -------------------------------------------------------*/
    //SPI_I2S_DeInit(EVAL_SPI);
    SPI_InitTypeDef  SPI_InitStructure;
    SPI_InitStructure.SPI_Direction = SPI_Direction_1Line_Rx;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Hard;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial = 7;
    SPI_InitStructure.SPI_Mode = SPI_Mode_Slave;

    SPI_Init(EVAL_SPI,&SPI_InitStructure);

    /* Configure the Priority Group to 1 bit */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

    /* Configure the SPI interrupt priority */
    NVIC_InitStructure.NVIC_IRQChannel = EVAL_SPI_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/*
void decodeMsg(uint8_t curByte){
    if(curStage == IN_PREAMBLE){
        if(curByte == 0x7E && lastByte == 0x7E)
            curStage = IN_PAYLOAD;
        if(curByte != 0x7E){
            rangeBuffer[rangePktCnt+] = curByte;
            curStage = IN_PAYLOAD;
        }
    }else if(curStage == IN_PAYLOAD){
        if(curByte == 0x7E){
            curStage = IN_PREAMBLE;
            if(rangePktCnt == sizeof(Msg_t)){
                Msg_t* msgPtr = (Msg_t *)(&rangeBuffer);
                ID = msgPtr->ID;
                Seq = msgPtr->Seq;
                firstRisingEdge = msgPtr->risingEdge[0];
                distCM = firstRisingEdge*34.6/250-offset[ID];
                wrongPkt = 0;
                pktReady = 1;
            }else{
                ID = 0;
                distCM = 0;
                wrongPkt = rangePktCnt;
                pktReady = 1;
            }
            rangePktCnt = 0;
        }
        else if(lastByte == 0x7D && curByte == 0x5D){
            rangeBuffer[rangePktCnt-1] = 0x7D;
        }
        else if(lastByte == 0x7D && curByte == 0x5E){
             rangeBuffer[rangePktCnt-1] = 0x7E;
        }else{
             rangeBuffer[rangePktCnt++] = curByte;
        }
    }else if(curStage == WAITING){
        //do nothing
        if(curByte == 0x7E){
            curStage = IN_PREAMBLE;
            rangePktCnt = 0;
            pktReady = 0;
        }
    }
    lastByte = curByte;
}
*/
/** @addtogroup Template_Project
  * @{
  */

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
