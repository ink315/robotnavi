#include "stm324xg_eval.h"
#include "stdint.h"
#include "yaEkf6.hh"
#include <stdio.h>
#include "arm_math.h"
#include <stdint.h>

#define Nf  6  //Number of States
#define Mf  6  //Number of Observations

float32_t rf = 50.0f;
float32_t qf = 1.0f; 
float32_t dt=0.02;


arm_matrix_instance_f32 Rf;
float32_t Rfm[Mf][Mf];

arm_matrix_instance_f32 Qf;
float32_t Qfm[Nf][Nf];


arm_matrix_instance_f32 Af;
float32_t Afm[Nf][Nf];


arm_matrix_instance_f32 Bf;
float32_t Bfm[Nf][Mf];

arm_matrix_instance_f32 Hf;
float32_t Hfm[Mf][Nf];

arm_matrix_instance_f32 If;
float32_t Ifm[Nf][Nf];

arm_matrix_instance_f32 PP;
float32_t PPm[Nf][Nf];

arm_matrix_instance_f32 XX;
float32_t XXm[Nf];


uint8_t initFusion(arm_matrix_instance_f32 *initPos, float att[]){

    setMatToUnit(PPm,Nf,1.0);
    arm_mat_init_f32(&PP,Nf,Nf,&(PPm[0][0]));

    setMatToUnit(Rfm,Mf,200);
    arm_mat_init_f32(&Rf,Mf,Mf,&(Rfm[0][0]));


    setMatToUnit(Qfm,Nf,qf*qf);
    arm_mat_init_f32(&Qf,Nf,Nf,&(Qfm[0][0]));


    setMatToUnit(Afm,Nf,1);
    arm_mat_init_f32(&Af,Nf,Nf,&(Afm[0][0]));
    Af.pData[0*Nf+3] = dt;
    Af.pData[1*Nf+4] = dt;
    Af.pData[2*Nf+5] = dt;
    

    arm_mat_init_f32(&Bf,Nf,Nf-3,&(Bfm[0][0]));
    Bf.pData[0*Nf+0] = dt*dt;
    Bf.pData[1*Nf+1] = dt*dt;
    Bf.pData[2*Nf+2] = dt*dt;
    Bf.pData[3*Nf+0] = dt;
    Bf.pData[4*Nf+1] = dt;
    Bf.pData[5*Nf+2] = dt;


    setMatToUnit(Hfm,Nf,1);
    arm_mat_init_f32(&Hf,Mf,Nf,&(Hfm[0][0]));  
    
    setMatToUnit(Ifm,Nf,1);
    arm_mat_init_f32(&If,Nf,Nf,&(Ifm[0][0]));  

    arm_mat_init_f32(&XX,Nf,1,&(XXm[0]));  
    for(int i = 0; i<3; i++)
        XX.pData[i] = initPos->pData[i];
    for(int i = 3; i<6; i++)
        XX.pData[i] = 0;
    return 0;
}
//  X = AX + BU;
//  P = APA' + Q;

uint8_t updateByImu(float32_t Imu[]){
    arm_matrix_instance_f32 Uf;
    arm_matrix_instance_f32 BUf;
    arm_matrix_instance_f32 AXf;
    arm_matrix_instance_f32 ATf;
    arm_matrix_instance_f32 APf;
    arm_matrix_instance_f32 APATf;

    float32_t ATfm[Nf][Nf];   
    float32_t APfm[Nf][Nf];
    float32_t APATfm[Nf][Nf];

    
    float32_t BUfm[Nf]={{0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}};
    float32_t AXfm[Nf]={{0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}};
    
    arm_mat_init_f32(&Uf,Nf,1,&(Imu[0]));
    arm_mat_init_f32(&BUf,Nf,1,&(BUfm[0]));
    arm_mat_init_f32(&ATf, Nf, Nf, &(ATfm[0][0]));
    arm_mat_init_f32(&APf, Nf, Nf, &(APfm[0][0]));
    arm_mat_init_f32(&APATf, Nf, Nf, &(APATfm[0][0]));
    arm_mat_init_f32(&AXf,Nf,1, &(AXfm[0]));  

    arm_mat_mult_f32(&Bf, &Uf, &BUf);

    arm_mat_mult_f32(&Af, &XX, &AXf);
    arm_mat_add_f32(&AXf, &BUf, &XX);

    arm_mat_mult_f32(&Af, &PP, &APf);
    arm_mat_trans_f32(&Af, &ATf);
    arm_mat_mult_f32(&APf, &ATf, &APATf);
    arm_mat_add_f32(&APATf, &Qf, &PP);
//    for(int i = 6; i<9; i++)
//        XX.pData[i] = att[i-6];
    return 0;
    
}

uint8_t constructAccGyro(float32_t AccGyro[], float acc[], float pal[])
{
	AccGyro[0] = acc[0];
	AccGyro[1] = acc[1];
	AccGyro[2] = acc[2];
	AccGyro[3] = pal[0];
	AccGyro[4] = pal[1];
	AccGyro[5] = pal[2];
	return 0;
}

uint8_t constructLoc(float32_t Loc[],  arm_matrix_instance_f32 x, float att[])
{
	Loc[0] = x.pData[0];
	Loc[1] = x.pData[1];
	Loc[2] = x.pData[2];
	Loc[3] = x.pData[3];
	Loc[4] = x.pData[4];
	Loc[5] = x.pData[5];
	return 0;
}

uint8_t updateByUsLoc(float32_t Loc[]){
    arm_matrix_instance_f32 HTf;
    arm_matrix_instance_f32 PHTf;
    arm_matrix_instance_f32 HPHTf;
    arm_matrix_instance_f32 Df;    
    arm_matrix_instance_f32 IDf;
    arm_matrix_instance_f32 Kf;
    arm_matrix_instance_f32 HXf; 
    arm_matrix_instance_f32 Z_HXf;
    arm_matrix_instance_f32 KZ_HXf;    
    arm_matrix_instance_f32 KHf;
    arm_matrix_instance_f32 I_KHf;
    arm_matrix_instance_f32 LOC;

    float32_t HTfm[Nf][Mf];
    float32_t PHTfm[Nf][Nf];
    float32_t HPHTfm[Mf][Mf];
    float32_t Dfm[Mf][Mf];
    float32_t IDfm[Mf][Mf];
    float32_t Kfm[Nf][Mf];
    float32_t HXfm[Mf][1];
    float32_t Z_HXfm[Mf][1];
    float32_t KZ_HXfm[Nf][1];
    float32_t KHfm[Nf][Nf];
    float32_t I_KHfm[Nf][Nf];


    arm_mat_init_f32(&HTf,Nf,Mf, &HTfm[0][0]);
    arm_mat_init_f32(&PHTf,Nf,Nf, &PHTfm[0][0]);
    arm_mat_init_f32(&HPHTf,Mf,Mf,&HPHTfm[0][0]);
    arm_mat_init_f32(&Df,Mf,Mf,&Dfm[0][0]);
    arm_mat_init_f32(&IDf,Mf,Mf, &IDfm[0][0]);
    arm_mat_init_f32(&Kf, Nf, Mf, &Kfm[0][0]);
    arm_mat_init_f32(&HXf, Mf, 1, &HXfm[0][0]);
    arm_mat_init_f32(&Z_HXf, Mf, 1, &Z_HXfm[0][0]);
    arm_mat_init_f32(&KZ_HXf, Nf, 1, &KZ_HXfm[0][0]);
    arm_mat_init_f32(&KHf, Nf, Nf, &KHfm[0][0]);
    arm_mat_init_f32(&I_KHf, Nf, Nf, &I_KHfm[0][0]);
    arm_mat_init_f32(&LOC, Mf, 1, (float32_t *)Loc);

/*
    D = H*cur_P*H' + R;
    K = cur_P * H' * inv(D);
    I = diag(ones(n,1),0);
    cur_x = cur_x + K*(c(j,1:9)'-H*cur_x)
    cur_P = (I - K*H)*cur_P;
*/
    arm_mat_trans_f32(&Hf, &HTf);
    arm_mat_mult_f32(&PP, &HTf, &PHTf);
    arm_mat_mult_f32(&Hf, &PHTf, &HPHTf);
    arm_mat_add_f32(&HPHTf, &Rf, &Df);
    arm_mat_inverse_f32(&Df, &IDf);
    arm_mat_mult_f32(&PHTf, &IDf, &Kf);
    arm_mat_mult_f32(&Hf, &XX, &HXf);
    arm_mat_sub_f32(&LOC, &HXf, &Z_HXf);
    arm_mat_mult_f32(&Kf, &Z_HXf, &KZ_HXf);
    arm_mat_add_f32(&XX, &KZ_HXf, &XX);
    arm_mat_mult_f32(&Kf, &Hf, &KHf);
    arm_mat_sub_f32(&If, &KHf, &I_KHf);
    arm_mat_mult_f32(&I_KHf, &PP, &PP);
	return 0;

}




    
    
    
    
  