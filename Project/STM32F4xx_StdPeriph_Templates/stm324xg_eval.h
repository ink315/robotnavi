/**
  ******************************************************************************
  * @file    stm324xg_eval.h
  * @author  MCD Application Team
  * @version V1.1.1
  * @date    11-January-2013
  * @brief   This file contains definitions for STM324xG_EVAL's Leds, push-buttons
  *          and COM ports hardware resources.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STM324xG_EVAL_H
#define __STM324xG_EVAL_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "stm32_eval_legacy.h"
#include "Msg.hh"

#ifndef CRITICAL_SECTION_START
#define CRITICAL_SECTION_START __set_PRIMASK(1)
#define CRITICAL_SECTION_END   __set_PRIMASK(0)
#endif //CRITICAL_SECTION_START


/** @addtogroup Utilities
  * @{
  */

/** @addtogroup STM32_EVAL
  * @{
  */

/** @addtogroup STM324xG_EVAL
  * @{
  */

/** @addtogroup STM324xG_EVAL_LOW_LEVEL
  * @{
  */

/** @defgroup STM324xG_EVAL_LOW_LEVEL_Exported_Types
  * @{
  */
typedef enum
{
  LED1 = 0,
  LED2 = 1,
} Led_TypeDef;

typedef enum
{
  BUTTON_WAKEUP = 0,
  BUTTON_TAMPER = 1,
  BUTTON_KEY = 2,
  BUTTON_RIGHT = 3,
  BUTTON_LEFT = 4,
  BUTTON_UP = 5,
  BUTTON_DOWN = 6,
  BUTTON_SEL = 7
} Button_TypeDef;

typedef enum
{
  BUTTON_MODE_GPIO = 0,
  BUTTON_MODE_EXTI = 1
} ButtonMode_TypeDef;

typedef enum
{
  JOY_NONE = 0,
  JOY_SEL = 1,
  JOY_DOWN = 2,
  JOY_LEFT = 3,
  JOY_RIGHT = 4,
  JOY_UP = 5
} JOYState_TypeDef
;

typedef enum
{
  COM1 = 0,
  COM2 = 1
} COM_TypeDef;


typedef union uFloatInt{
    uint32_t ui;
    float fl;
} floatInt;

typedef union uMsgBuf{
    Msg_t msg;
    uint8_t buf[48];
} MsgBuf;

typedef union uPointMsgBuf{
    Point_msg_t msg;
    uint8_t buf[17];
} PointMsgBuf;

/**
  * @}
  */

/** @defgroup STM324xG_EVAL_LOW_LEVEL_Exported_Constants
  * @{
  */

/**
  * @brief  Define for STM324xG_EVAL board
  */
#if !defined (USE_STM324xG_EVAL)
 #define USE_STM324xG_EVAL
#endif

/** @addtogroup STM324xG_EVAL_LOW_LEVEL_LED
  * @{
  */
#define LEDn                             2

#define LED1_PIN                         GPIO_Pin_1
#define LED1_GPIO_PORT                   GPIOC
#define LED1_GPIO_CLK                    RCC_AHB1Periph_GPIOC

#define LED2_PIN                         GPIO_Pin_0
#define LED2_GPIO_PORT                   GPIOC
#define LED2_GPIO_CLK                    RCC_AHB1Periph_GPIOC

/*
 * Define the ADC port
 */
#define US_ADC_DMA_CLK                   RCC_AHB1Periph_DMA2
#define US_ADC_DMA_Channel               DMA_Channel_2
#define US_ADC_GPIO_CLK                  RCC_AHB1Periph_GPIOA
#define US_ADC                           ADC3
#define US_ADC_Channel                   ADC_Channel_1
#define US_ADC_CLK                       RCC_APB2Periph_ADC3
#define US_ADC_GPIO_Pin                  GPIO_Pin_1
#define US_ADC_GPIO_Port                 GPIOA

#define ADC3_DR_ADDRESS                  ((uint32_t)0x4001224C)
#define ADC_VALUE_LEN 1000
/**
  * @}
  */

/** @addtogroup STM324xG_EVAL_LOW_LEVEL_BUTTON
  * @{
  */
#define BUTTONn                          3 /*!< Joystick pins are connected to
                                                an IO Expander (accessible through
                                                I2C1 interface) */

/**
 * @brief Wakeup push-button
 */
#define WAKEUP_BUTTON_PIN                GPIO_Pin_0
#define WAKEUP_BUTTON_GPIO_PORT          GPIOA
#define WAKEUP_BUTTON_GPIO_CLK           RCC_AHB1Periph_GPIOA
#define WAKEUP_BUTTON_EXTI_LINE          EXTI_Line0
#define WAKEUP_BUTTON_EXTI_PORT_SOURCE   EXTI_PortSourceGPIOA
#define WAKEUP_BUTTON_EXTI_PIN_SOURCE    EXTI_PinSource0
#define WAKEUP_BUTTON_EXTI_IRQn          EXTI0_IRQn

/**
 * @brief Tamper push-button
 */
#define TAMPER_BUTTON_PIN                GPIO_Pin_13
#define TAMPER_BUTTON_GPIO_PORT          GPIOC
#define TAMPER_BUTTON_GPIO_CLK           RCC_AHB1Periph_GPIOC
#define TAMPER_BUTTON_EXTI_LINE          EXTI_Line13
#define TAMPER_BUTTON_EXTI_PORT_SOURCE   EXTI_PortSourceGPIOC
#define TAMPER_BUTTON_EXTI_PIN_SOURCE    EXTI_PinSource13
#define TAMPER_BUTTON_EXTI_IRQn          EXTI15_10_IRQn

/**
 * @brief Key push-button
 */
#define KEY_BUTTON_PIN                   GPIO_Pin_15
#define KEY_BUTTON_GPIO_PORT             GPIOG
#define KEY_BUTTON_GPIO_CLK              RCC_AHB1Periph_GPIOG
#define KEY_BUTTON_EXTI_LINE             EXTI_Line15
#define KEY_BUTTON_EXTI_PORT_SOURCE      EXTI_PortSourceGPIOG
#define KEY_BUTTON_EXTI_PIN_SOURCE       EXTI_PinSource15
#define KEY_BUTTON_EXTI_IRQn             EXTI15_10_IRQn
/**
  * @}
  */

/** @addtogroup STM324xG_EVAL_LOW_LEVEL_COM
  * @{
  */
#define COMn                             2

/**
 * @brief Definition for COM port1, connected to USART3
 */
#define EVAL_COM1                        USART1
#define EVAL_COM1_CLK                    RCC_APB2Periph_USART1
#define EVAL_COM1_TX_PIN                 GPIO_Pin_9
#define EVAL_COM1_TX_GPIO_PORT           GPIOA
#define EVAL_COM1_TX_GPIO_CLK            RCC_AHB1Periph_GPIOA
#define EVAL_COM1_TX_SOURCE              GPIO_PinSource9
#define EVAL_COM1_TX_AF                  GPIO_AF_USART1
#define EVAL_COM1_RX_PIN                 GPIO_Pin_10
#define EVAL_COM1_RX_GPIO_PORT           GPIOA
#define EVAL_COM1_RX_GPIO_CLK            RCC_AHB1Periph_GPIOA
#define EVAL_COM1_RX_SOURCE              GPIO_PinSource10
#define EVAL_COM1_RX_AF                  GPIO_AF_USART1
#define EVAL_COM1_IRQn                   USART1_IRQn

#define EVAL_COM2                        USART2
#define EVAL_COM2_CLK                    RCC_APB1Periph_USART2
#define EVAL_COM2_TX_PIN                 GPIO_Pin_2
#define EVAL_COM2_TX_GPIO_PORT           GPIOA
#define EVAL_COM2_TX_GPIO_CLK            RCC_AHB1Periph_GPIOA
#define EVAL_COM2_TX_SOURCE              GPIO_PinSource2
#define EVAL_COM2_TX_AF                  GPIO_AF_USART2
#define EVAL_COM2_RX_PIN                 GPIO_Pin_3
#define EVAL_COM2_RX_GPIO_PORT           GPIOA
#define EVAL_COM2_RX_GPIO_CLK            RCC_AHB1Periph_GPIOA
#define EVAL_COM2_RX_SOURCE              GPIO_PinSource3
#define EVAL_COM2_RX_AF                  GPIO_AF_USART2
#define EVAL_COM2_IRQn                   USART2_IRQn

#define EVAL_SPI                         SPI1
#define EVAL_SPI_CLK                     RCC_APB2Periph_SPI1
#define EVAL_SPI_CLK_INIT                RCC_APB2PeriphClockCmd
#define EVAL_SPI_IRQn                   SPI1_IRQn

#define EVAL_SPI_SCK_PIN                 GPIO_Pin_5
#define EVAL_SPI_SCK_GPIO_PORT           GPIOA
#define EVAL_SPI_SCK_GPIO_CLK            RCC_AHB1Periph_GPIOA
#define EVAL_SPI_SCK_SOURCE              GPIO_PinSource5
#define EVAL_SPI_SCK_AF                  GPIO_AF_SPI1

#define EVAL_SPI_MISO_PIN                GPIO_Pin_6
#define EVAL_SPI_MISO_GPIO_PORT          GPIOA
#define EVAL_SPI_MISO_GPIO_CLK           RCC_AHB1Periph_GPIOA
#define EVAL_SPI_MISO_SOURCE             GPIO_PinSource6
#define EVAL_SPI_MISO_AF                 GPIO_AF_SPI1

#define EVAL_SPI_MOSI_PIN                GPIO_Pin_7
#define EVAL_SPI_MOSI_GPIO_PORT          GPIOA
#define EVAL_SPI_MOSI_GPIO_CLK           RCC_AHB1Periph_GPIOA
#define EVAL_SPI_MOSI_SOURCE             GPIO_PinSource7
#define EVAL_SPI_MOSI_AF                 GPIO_AF_SPI1

#define EVAL_SPI_NSS_PIN                      GPIO_Pin_4
#define EVAL_SPI_NSS_GPIO_PORT                GPIOA
#define EVAL_SPI_NSS_GPIO_CLK                 RCC_AHB1Periph_GPIOA
/**
  * @}
  */

/** @addtogroup STM324xG_EVAL_LOW_LEVEL_SD_FLASH
  * @{
  */
/**
  * @brief  SD FLASH SDIO Interface
  */
#define SD_DETECT_PIN                    GPIO_Pin_13                 /* PH.13 */
#define SD_DETECT_GPIO_PORT              GPIOH                       /* GPIOH */
#define SD_DETECT_GPIO_CLK               RCC_AHB1Periph_GPIOH

#define SDIO_FIFO_ADDRESS                ((uint32_t)0x40012C80)
/**
  * @brief  SDIO Intialization Frequency (400KHz max)
  */
#define SDIO_INIT_CLK_DIV                ((uint8_t)0x76)
/**
  * @brief  SDIO Data Transfer Frequency (25MHz max)
  */
#define SDIO_TRANSFER_CLK_DIV            ((uint8_t)0x0)

#define SD_SDIO_DMA                   DMA2
#define SD_SDIO_DMA_CLK               RCC_AHB1Periph_DMA2

#define SD_SDIO_DMA_STREAM3	          3
//#define SD_SDIO_DMA_STREAM6           6

#ifdef SD_SDIO_DMA_STREAM3
 #define SD_SDIO_DMA_STREAM            DMA2_Stream3
 #define SD_SDIO_DMA_CHANNEL           DMA_Channel_4
 #define SD_SDIO_DMA_FLAG_FEIF         DMA_FLAG_FEIF3
 #define SD_SDIO_DMA_FLAG_DMEIF        DMA_FLAG_DMEIF3
 #define SD_SDIO_DMA_FLAG_TEIF         DMA_FLAG_TEIF3
 #define SD_SDIO_DMA_FLAG_HTIF         DMA_FLAG_HTIF3
 #define SD_SDIO_DMA_FLAG_TCIF         DMA_FLAG_TCIF3
 #define SD_SDIO_DMA_IRQn              DMA2_Stream3_IRQn
 #define SD_SDIO_DMA_IRQHANDLER        DMA2_Stream3_IRQHandler
#elif defined SD_SDIO_DMA_STREAM6
 #define SD_SDIO_DMA_STREAM            DMA2_Stream6
 #define SD_SDIO_DMA_CHANNEL           DMA_Channel_4
 #define SD_SDIO_DMA_FLAG_FEIF         DMA_FLAG_FEIF6
 #define SD_SDIO_DMA_FLAG_DMEIF        DMA_FLAG_DMEIF6
 #define SD_SDIO_DMA_FLAG_TEIF         DMA_FLAG_TEIF6
 #define SD_SDIO_DMA_FLAG_HTIF         DMA_FLAG_HTIF6
 #define SD_SDIO_DMA_FLAG_TCIF         DMA_FLAG_TCIF6
 #define SD_SDIO_DMA_IRQn              DMA2_Stream6_IRQn
 #define SD_SDIO_DMA_IRQHANDLER        DMA2_Stream6_IRQHandler
#endif /* SD_SDIO_DMA_STREAM3 */

/**
  * @}
  */

/** @addtogroup STM324xG_EVAL_LOW_LEVEL_I2C_EE
  * @{
  */
/**
  * @brief  I2C EEPROM Interface pins
  */
#define sEE_I2C                          I2C1
#define sEE_I2C_CLK                      RCC_APB1Periph_I2C1
#define sEE_I2C_SCL_PIN                  GPIO_Pin_6                  /* PB.06 */
#define sEE_I2C_SCL_GPIO_PORT            GPIOB                       /* GPIOB */
#define sEE_I2C_SCL_GPIO_CLK             RCC_AHB1Periph_GPIOB
#define sEE_I2C_SCL_SOURCE               GPIO_PinSource6
#define sEE_I2C_SCL_AF                   GPIO_AF_I2C1
#define sEE_I2C_SDA_PIN                  GPIO_Pin_9                  /* PB.09 */
#define sEE_I2C_SDA_GPIO_PORT            GPIOB                       /* GPIOB */
#define sEE_I2C_SDA_GPIO_CLK             RCC_AHB1Periph_GPIOB
#define sEE_I2C_SDA_SOURCE               GPIO_PinSource9
#define sEE_I2C_SDA_AF                   GPIO_AF_I2C1
#define sEE_M24C64_32

#define sEE_I2C_DMA                      DMA1
#define sEE_I2C_DMA_CHANNEL              DMA_Channel_1
#define sEE_I2C_DMA_STREAM_TX            DMA1_Stream6
#define sEE_I2C_DMA_STREAM_RX            DMA1_Stream0
#define sEE_I2C_DMA_CLK                  RCC_AHB1Periph_DMA1
#define sEE_I2C_DR_Address               ((uint32_t)0x40005410)
#define sEE_USE_DMA

#define sEE_I2C_DMA_TX_IRQn              DMA1_Stream6_IRQn
#define sEE_I2C_DMA_RX_IRQn              DMA1_Stream0_IRQn
#define sEE_I2C_DMA_TX_IRQHandler        DMA1_Stream6_IRQHandler
#define sEE_I2C_DMA_RX_IRQHandler        DMA1_Stream0_IRQHandler
#define sEE_I2C_DMA_PREPRIO              0
#define sEE_I2C_DMA_SUBPRIO              0

#define sEE_TX_DMA_FLAG_FEIF             DMA_FLAG_FEIF6
#define sEE_TX_DMA_FLAG_DMEIF            DMA_FLAG_DMEIF6
#define sEE_TX_DMA_FLAG_TEIF             DMA_FLAG_TEIF6
#define sEE_TX_DMA_FLAG_HTIF             DMA_FLAG_HTIF6
#define sEE_TX_DMA_FLAG_TCIF             DMA_FLAG_TCIF6
#define sEE_RX_DMA_FLAG_FEIF             DMA_FLAG_FEIF0
#define sEE_RX_DMA_FLAG_DMEIF            DMA_FLAG_DMEIF0
#define sEE_RX_DMA_FLAG_TEIF             DMA_FLAG_TEIF0
#define sEE_RX_DMA_FLAG_HTIF             DMA_FLAG_HTIF0
#define sEE_RX_DMA_FLAG_TCIF             DMA_FLAG_TCIF0

#define sEE_DIRECTION_TX                 0
#define sEE_DIRECTION_RX                 1

#define SPI_OUT                           SPI2
#define SPI_OUT_CLK                       RCC_APB1Periph_SPI2
#define SPI_OUT_CLK_INIT                  RCC_APB1PeriphClockCmd
#define SPI_OUT_IRQn                      SPI2_IRQn
#define SPI_OUT_IRQHANDLER                SPI2_IRQHandler

#define SPI_OUT_SCK_PIN                   GPIO_Pin_13
#define SPI_OUT_SCK_GPIO_PORT             GPIOB
#define SPI_OUT_SCK_GPIO_CLK              RCC_AHB1Periph_GPIOB
#define SPI_OUT_SCK_SOURCE                GPIO_PinSource13
#define SPI_OUT_SCK_AF                    GPIO_AF_SPI2

#define SPI_OUT_MISO_PIN                  GPIO_Pin_14
#define SPI_OUT_MISO_GPIO_PORT            GPIOB
#define SPI_OUT_MISO_GPIO_CLK             RCC_AHB1Periph_GPIOB
#define SPI_OUT_MISO_SOURCE               GPIO_PinSource14
#define SPI_OUT_MISO_AF                   GPIO_AF_SPI2

#define SPI_OUT_MOSI_PIN                  GPIO_Pin_15
#define SPI_OUT_MOSI_GPIO_PORT            GPIOB
#define SPI_OUT_MOSI_GPIO_CLK             RCC_AHB1Periph_GPIOB
#define SPI_OUT_MOSI_SOURCE               GPIO_PinSource15
#define SPI_OUT_MOSI_AF                   GPIO_AF_SPI2

#define SPI_OUT_DMA                       DMA1
#define SPI_OUT_DMA_CLK                   RCC_AHB1Periph_DMA1
#define SPI_OUT_TX_DMA_CHANNEL            DMA_Channel_0
#define SPI_OUT_TX_DMA_STREAM             DMA1_Stream4
#define SPI_OUT_TX_DMA_FLAG_TCIF          DMA_FLAG_TCIF4
#define SPI_OUT_RX_DMA_CHANNEL            DMA_Channel_0
#define SPI_OUT_RX_DMA_STREAM             DMA1_Stream3
#define SPI_OUT_RX_DMA_FLAG_TCIF          DMA_FLAG_TCIF3



/* Time constant for the delay caclulation allowing to have a millisecond
   incrementing counter. This value should be equal to (System Clock / 1000).
   ie. if system clock = 168MHz then sEE_TIME_CONST should be 168. */
#define sEE_TIME_CONST                   168
#define MAX_USART_BUF_LEN		40

#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base @ of Sector 0, 16 Kbytes */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base @ of Sector 1, 16 Kbytes */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base @ of Sector 2, 16 Kbytes */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base @ of Sector 3, 16 Kbytes */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base @ of Sector 4, 64 Kbytes */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base @ of Sector 5, 128 Kbytes */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base @ of Sector 6, 128 Kbytes */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base @ of Sector 7, 128 Kbytes */
#define ADDR_FLASH_SECTOR_8     ((uint32_t)0x08080000) /* Base @ of Sector 8, 128 Kbytes */
#define ADDR_FLASH_SECTOR_9     ((uint32_t)0x080A0000) /* Base @ of Sector 9, 128 Kbytes */
#define ADDR_FLASH_SECTOR_10    ((uint32_t)0x080C0000) /* Base @ of Sector 10, 128 Kbytes */
#define ADDR_FLASH_SECTOR_11    ((uint32_t)0x080E0000) /* Base @ of Sector 11, 128 Kbytes */

/**
  * @}
  */
/**
  * @}
  */

/** @defgroup STM324xG_EVAL_LOW_LEVEL_Exported_Macros
  * @{
  */
/**
  * @}
  */


/** @defgroup STM324xG_EVAL_LOW_LEVEL_Exported_Functions
  * @{
  */
void STM_EVAL_LEDInit(Led_TypeDef Led);
void STM_EVAL_LEDOn(Led_TypeDef Led);
void STM_EVAL_LEDOff(Led_TypeDef Led);
void STM_EVAL_LEDToggle(Led_TypeDef Led);
void STM_EVAL_PBInit(Button_TypeDef Button, ButtonMode_TypeDef Button_Mode);
uint32_t STM_EVAL_PBGetState(Button_TypeDef Button);
void STM_EVAL_COMInit(COM_TypeDef COM, USART_InitTypeDef* USART_InitStruct);
void USART_NVIC_Config(COM_TypeDef COMx);
void USART_Config(COM_TypeDef COMx);
void SD_LowLevel_DeInit(void);
void SD_LowLevel_Init(void);
void SD_LowLevel_DMA_TxConfig(uint32_t *BufferSRC, uint32_t BufferSize);
void SD_LowLevel_DMA_RxConfig(uint32_t *BufferDST, uint32_t BufferSize);
void sEE_LowLevel_DeInit(void);
void sEE_LowLevel_Init(void);
void sEE_LowLevel_DMAConfig(uint32_t pBuffer, uint32_t BufferSize, uint32_t Direction);
void USART1_Puts(const char * str);
void USART2_Puts(char * str);
void USART1_Putsn(const char * str,uint8_t n);
void print(const char* output);
static void rawPrint(uint8_t id, float x,float y,float z,float vx,float vy,float vz,float euler_x,float euler_y,float euler_z);
void usADCInit(void);
void initTimer(void);
void SPI_Config(void);
void SPI_In_Config(void);
void TIM2_Configuration(void);
void SPI2_Init(void);
void SPI2_Putsn(const char * str,uint8_t n);
void rawPrint(uint8_t id, float x,float y,float z,float vx,float vy,float vz,float euler_x,float euler_y,float euler_z);
void InitUSART1DMA(void);
void Delay(__IO uint32_t nTime);
void TimingDelay_Decrement(void);
void usDelay(void);
int FlashReadConfig(uint8_t* numBeacon,uint8_t *beaconID,float *beaconCoord,float *offset);
void MPU6050_DMA_Config(void);
void I2C_DMA_Read(void);
void SPI_Out_Config(void);

/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __STM324xG_EVAL_H */
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
