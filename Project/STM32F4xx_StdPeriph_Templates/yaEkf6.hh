#ifndef __yaEkf_hh__
#define __yaEkf_hh__

#define setMatToUnit(M,col_size,val) {\
    uint8_t tts;\
    memset(&(M[0][0]),0,col_size*col_size);\
    for(tts = 0;tts<col_size;tts++)\
        M[tts][tts] = val;\
}

#define MAX_NUM_BEACON 20

#define STATE_VEC_LEN 9

#define OB_VEC_LEN 1

#define WINDOW_SIZE 7
#define bool uint8_t


//#include <cmatrix>
#include <arm_math.h>
#include <stdint.h>


extern arm_matrix_instance_f32 XX;

//uint8_t initEKF(dMatrix initPos);
uint8_t initEKF(float initpos[]);

//bool updateEKF(dMatrix z,int i, double step);
bool updateEKF(arm_matrix_instance_f32 z[], int i, float step);

float calDisFromBeacon(int i,  float x[]);
void NonLinearLSQ(float myx[], float32_t res[]);
bool CheckSingularGenerateVirtualDis();

int updateWindows(int b, float d, float t);
int updatePos(arm_matrix_instance_f32 *x);

void buildA(float32_t dt,arm_matrix_instance_f32* cA);
int isMember(int a, int A[]);
int updateTargetPos(float targetPos[], uint8_t t);

uint8_t cholesky(arm_matrix_instance_f32 *, arm_matrix_instance_f32 *);

void dfStateVec(arm_matrix_instance_f32 x, float step,arm_matrix_instance_f32* x1);

void buildH(arm_matrix_instance_f32 *cH,arm_matrix_instance_f32 *x, int i);

void dhmeasVec(arm_matrix_instance_f32 *dz,arm_matrix_instance_f32 x, int i);

int removeFromWindows();

void LSQwithNLOS(float myx[], float32_t r[], float rT, uint32_t *v, uint8_t *p);

bool distBufferFilter(float *dis, float ttime, float32_t Trh, uint8_t Msize, int id);

void updateByDis(float distCM, uint8_t ID);

void updatePosByIMU(uint8_t IMUReady, uint8_t MegReady);


bool CheckSingularGenerateVirtualDis(void);
int removeFromWindows(int k);

uint8_t resetEKF(arm_matrix_instance_f32 *resetPos);

uint8_t setBeaconCoordinates(int num, float x[][3]);

uint8_t setRangeOffset(int num,float o[]);

void setNumberOfBeacons(int n);

void setFilterParameters(float minDis, float maxDis, float jumpThreshold, float residueThreshold, float observeNoise, float stateNoise, float moveSize, float h)  ;

void setImuParameters(int imuon, int compassinterval, int imuinterval);

float32_t getLastPosition(float xyz[]);

float32_t calResidue(float pos[], float32_t res[]);

#endif
