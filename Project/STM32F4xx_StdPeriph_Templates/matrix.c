#include <math.h> 

public sealed class Matrix
 {
    public:
        uint8_t row, column;            //矩阵的行列数
        float data[][];            //矩阵的数据

        //构造函数
        Matrix(uint8_t rowNum, uint8_t columnNum)
        {
            row = rowNum;
            column = columnNum;
            data = new float[row][column];
        }
        Matrix(float[][] members, rowNum,columnNum)
        {
            row = rowNum;
            column = columnNum;
            data = members;
        }

        static Matrix transpose(Matrix input)
        {
            float transMatrix[,] = new float[input.column, input.row];
            for (uint8_t r = 0; r < input.row; r++)
                for (uint8_t c = 0; c < input.column; c++)
                    transMatrix[c, r] = input[r, c];
            return new Matrix(inverseMatrix);
        }

        static Matrix operator +(Matrix a, Matrix b)
        {
            if (a.row != b.row || a.column != b.column)
                throw new Exception("矩阵维数不匹配。");
            Matrix result = new Matrix(a.row, a.column);
            for (uint8_t i = 0; i < a.row; i++)
                for (uint8_t j = 0; j < a.column; j++)
                    result[i, j] = a[i, j] + b[i, j];
            return result;
        }

        static Matrix operator -(Matrix a, Matrix b)
        {
            return a + b * (-1);
        }

        static Matrix operator *(Matrix matrix, float factor)
        {
            Matrix result = new Matrix(matrix.row, matrix.column);
            for (uint8_t i = 0; i < matrix.row; i++)
                for (uint8_t j = 0; j < matrix.column; j++)
                    matrix[i, j] = matrix[i, j] * factor;
            return matrix;
        }
        static Matrix operator *(float factor, Matrix matrix)
        {
            return matrix * factor;
        }
        public static Matrix operator *(Matrix a, Matrix b)
        {
            if (a.column != b.row)
                return null;
            Matrix result = new Matrix(a.row, b.column);
            for (uint8_t i = 0; i < a.row; i++)
                for (uint8_t j = 0; j < b.column; j++)
                    for (uint8_t k = 0; k < a.column; k++)
                        result[i, j] += a[i, k] * b[k, j];
            return result;
        }
        static bool operator ==(Matrix a, Matrix b)
        {
            if (object.Equals(a, b)) return true;
            if (object.Equals(null, b))
                return a.Equals(b);
            return b.Equals(a);
        }
        static bool operator !=(Matrix a, Matrix b)
        {
            return !(a == b);
        }
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!(obj is Matrix)) return false;
            Matrix t = obj as Matrix;
            if (row != t.row || column != t.column) return false;
            return this.Equals(t, 10);
        }

        public bool Equals(Matrix matrix)
        {
            for (uint8_t r = 0; r < this.row; r++)
                for (uint8_t c = 0; c < this.column; c++)
                    if (Math.Abs(this[r, c] - matrix[r, c]) >= 0.000001)
                        return false;

            return true;
        }

        static Matrix Inverse(Matrix source)//对当前矩阵求逆,对增广矩阵进行初等变换即可
        {
            if (source.rowNum != source.columnNum)
                return null;
            uint8_t i, j, k;
            float temp1, temp2;
            Matrix inverse_data = new Matrix(source.row,source.row);
            for (i = 0; i < 4; i++)
                for (j = 0; j < 4; j++)
                    inverse_data.data[i, j] = 0;
            for (i = 0; i < 4; i++)
                inverse_data.data[i, i] = 1;
            for (i = 0; i < 4; i++)
            {
                if (source.data[i, i] == 0)//处理到第i行时发现第i个数为0，就寻找不为0的行与之交换
                {
                    for (k = i + 1; source.data[k, i] == 0; k++) ;
                    for (j = 0; j < 4; j++)
                    {
                        temp1 = source.data[i, j];
                        temp2 = inverse_data.data[i, j];
                        source[i, j] = source.data[k, j];
                        source[k, j] = temp1;
                        inverse_data[i, j] = inverse_data[k, j];
                        inverse_data[k, j] = temp2;
                    }
                }
                temp1 = source[i, i];
                for (j = 0; j < 4; j++)//第i行标准化
                {
                    source[i, j] = source[i, j] / temp1;
                    inverse_data[i, j] = inverse_data[i, j] / temp1;
                }
                for (k = 0; k < 4; k++)  //余下行的第i列通过初等变换清0
                {
                    if (k == i || source[k, i] == 0) continue;
                    temp1 = source[k, i];
                    for (j = 0; j < 4; j++)
                    {
                        source[k, j] = source[k, j] - source[i, j] * temp1;
                        inverse_data[k, j] = inverse_data[k, j] - inverse_data[i, j] * temp1;
                    }
                }
            }
            return inverse_data;
        }
    }
