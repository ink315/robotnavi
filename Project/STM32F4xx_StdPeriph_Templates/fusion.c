#include "arm_math.h"
#include "stdint.h"
#include <stdio.h>

#define Nf = 9;  //Number of States
#define Mf = 9;  //Number of Observations

float32_t rf = 1.0f;
float32_t qf = 1.0f; 
float32_t rr = rf*rf;
float32_t qq = qf*qf;
float32_t dt=0.01;
float32_t dd=0.0001;

//dMatrix X;
arm_matrix_instance_f32 Xf;
float32_t Xfm[Nf]={
  0,0,0,0,0,0,0,0,0};

arm_matrix_instance_f32 Rf;
float32_t Rfm[Mf][Mf]={
  {rr, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, rr, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, rr, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, rr, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, rr, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, rr, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, rr, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, rr, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, rr, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, rr},
  };

arm_matrix_instance_f32 Qf;
float32_t Qfm[Nf][Nf]={
  {qq, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, qq, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, qq, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, qq, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, qq, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, qq, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, qq, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, qq, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, qq, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, qq},
  };

arm_matrix_instance_f32 Pf;
float32_t Pfm[Nf][Nf]={
  {1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
  };

arm_matrix_instance_f32 Af;
float32_t Afm[Nf][Nf] = {
  {1, 0, 0, dt, 0, 0, 0, 0, 0, 0},
  {0, 1, 0, 0, dt, 0, 0, 0, 0, 0},
  {0, 0, 1, 0, 0, dt, 0, 0, 0, 0},
  {0, 0, 0, 1, 0, 0,  0, 0, 0, 0},
  {0, 0, 0, 0, 1, 0, 0,  0, 0, 0},
  {0, 0, 0, 0, 0, 1, 0, 0,  0, 0},
  {0, 0, 0, 0, 0, 0, 1, 0, 0,  0},
  {0, 0, 0, 0, 0, 0, 0, 1, 0,  0},
  {0, 0, 0, 0, 0, 0, 0, 0, 1,  0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0,  1},
  };


arm_matrix_instance_f32 Bf;
float32_t Bfm[Nf][Nf-3]={
  {dd, 0, 0, 0, 0, 0}, 
  {0, dd, 0, 0, 0, 0}, 
  {0, 0, dd, 0, 0, 0}, 
  {dt, 0, 0, 0, 0, 0}, 
  {0, dt, 0, 0, 0, 0}, 
  {0, 0, dt, 0, 0, 0}, 
  {0, 0, 0, dt, 0, 0}, 
  {0, 0, 0, 0, dt, 0}, 
  {0, 0, 0, 0, 0, dt}, 
  };

arm_matrix_instance_f32 Hf;
float32_t Hfm[Mf][Nf]={
  {1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
  };

arm_matrix_instance_f32 If;
float32_t Ifm[Nf][Nf]={
  {1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
  {0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
  };




uint8_t initFusion(arm_matrix_instance_f32 *initPos){
    arm_mat_init_f32(&Rf,Mf,Mf,&(Rfm[0][0]));
    arm_mat_init_f32(&Qf,Nf,Nf,&(Qfm[0][0]));
    arm_mat_init_f32(&Pf,Nf,Nf,&(Pfm[0][0]));
    arm_mat_init_f32(&Af,Nf,Nf,&(Afm[0][0]));
    arm_mat_init_f32(&Bf,Nf,Nf-3,&(Bfm[0][0]));
    arm_mat_init_f32(&Hf,Mf,Nf,&(Hfm[0][0]));  
    arm_mat_init_f32(&If,Nf,Nf,&(Ifm[0][0]));  
    arm_mat_init_f32(&Xf,Nf,1, &(Xfm[0]));
    for(int i = 0; i<Nf; i++)
        Xf.pData[i] = initPos->pData[i];

    return 0;
}
//  X = AX + BU;
//  P = APA' + Q;

uint8_t updateByImu(float32_t Imu[]){
    arm_matrix_instance_f32 Uf;
    arm_matrix_instance_f32 BUf;
    arm_matrix_instance_f32 AXf;
    arm_matrix_instance_f32 ATf;
    arm_matrix_instance_f32 APf;
    arm_matrix_instance_f32 APATf;

    float32_t ATf[Nf][Nf];   
    float32_t APf[Nf][Nf];
    float32_t APATf[Nf][Nf];

    
    float32_t BUfm[Nf]={{0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}};
    float32_t AXfm[Nf]={{0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}};
    
    arm_mat_init_f32(&Uf,Nf-3,1 (float32_t *)Imu);
    arm_mat_init_f32(&BUf,Nf,1 &(BUfm[0]));
    arm_mat_init_f32(&ATf, Nf, Nf, &(ATf[0][0]));
    arm_mat_init_f32(&APf, Nf, Nf, &(APf[0][0]));
    arm_mat_init_f32(&APATf, Nf, Nf, &(APATf[0][0]));
    
    arm_mat_mult_f32(&Bf, &Uf, &BUf);
    arm_mat_init_f32(&AXf,Nf,1 &(AXfm[0]));

    arm_mat_mult_f32(&Af, &Xf, &AXf);
    arm_mat_add_f32(&AXf, &BUf, &Xf);

    arm_mat_mult_f32(&Af, &Pf, &APf);
    arm_mat_tran_f32(&Af, &ATf);
    arm_mat_mult_f32(&APf, &ATf, &APATf);
    arm_mat_add_f32(&APATf, &Qf, &Pf);
    return 0;
    
}

uint8_t constructAccGyro(float32_t AccGyro[], float acc[], float pal[])
{
	AccGyro[0] = acc[0];
	AccGyro[1] = acc[1];
	AccGyro[2] = acc[2];
	AccGyro[3] = pal[0];
	AccGyro[4] = pal[1];
	AccGyro[5] = pal[2];
	return 0;
}

uint8_t constructLoc(float32_t Loc[], float att[], float X[])
{
	Loc[0] = X[0];
	Loc[1] = X[1];
	Loc[2] = X[2];
	Loc[3] = X[3];
	Loc[4] = X[4];
	Loc[5] = X[5];
	Loc[6] = att[0];
	Loc[7] = att[1];
	Loc[8] = att[2];
	return 0;
}

uint8_t updateByUsLoc(float32_t Loc[]){
    arm_matrix_instance_f32 HTf;
    arm_matrix_instance_f32 PHTf;
    arm_matrix_instance_f32 HPHTf;
    arm_matrix_instance_f32 Df;    
    arm_matrix_instance_f32 IDf;
    arm_matrix_instance_f32 Kf;
    arm_matrix_instance_f32 HXf; 
    arm_matrix_instance_f32 Z_HXf;
    arm_matrix_instance_f32 KZ_HXf;    
    arm_matrix_instance_f32 KHf;
    arm_matrix_instance_f32 I_KHf;
    arm_matrix_instance_f32 LOC;

    float32_t HTfm[Nf][Mf];
    float32_t PHTm[Nf][Nf];
    float32_t HPHTfm[Mf][Mf];
    float32_t Dfm[Mf][Mf];
    float32_t IDfm[Mf][Mf];
    float32_t Kfm[Nf][Mf];
    float32_t HXfm[Mf][1];
    float32_t Z_HXfm[Mf][1];
    float32_t KZ_HXfm[Nf][1];
    float32_t KHfm[Nf][Nf];
    float32_t I_KHfm[Nf][Nf];


    arm_mat_init_f32(&HTf,Nf,Mf, &HTfm[0][0]);
    arm_mat_init_f32(&PHTf,Nf,Nf, &PHTfm[0][0]);
    arm_mat_init_f32(&HPHTf,Mf,Mf,&HPHTfm[0][0]);
    arm_mat_init_f32(&Df,Mf,Mf,&Dfm[0][0]);
    arm_mat_init_f32(&IDf,Mf,Mf, &IDfm[0][0]);
    arm_mat_init_f32(&Kf, Nf, Mf, &Kfm[0][0]);
    arm_mat_init_f32(&HXf, Mf, 1, &HXfm[0][0]);
    arm_mat_init_f32(&Z_HXf, Mf, 1, &Z_HXfm[0][0]);
    arm_mat_init_f32(&KZ_HXf, Nf, 1, &KZ_HXfm[0][0]);
    arm_mat_init_f32(&KHf, Nf, Nf, &KHfm[0][0]);
    arm_mat_init_f32(&I_KHf, Nf, Nf, &I_KHfm[0][0]);
    arm_mat_init_f32(&LOC, Nf, 1, (float32_t *)Loc);
/*
    D = H*cur_P*H' + R;
    K = cur_P * H' * inv(D);
    I = diag(ones(n,1),0);
    cur_x = cur_x + K*(c(j,1:9)'-H*cur_x)
    cur_P = (I - K*H)*cur_P;
*/
    arm_mat_tran_f32(&Hf, &HTf);
    arm_mat_mult_f32(&Pf, &HTf, &PHTf);
    arm_mat_mult_f32(&Hf, &PHTf, &HPHTf);
    arm_mat_add_f32(&HPHTf, &Rf, &Df);
    arm_mat_inverse_f32(&Df, &IDf);
    arm_mat_mult_f32(&PHTf, &IDf, &Kf);
    arm_mat_mult_f32(&Hf, &Xf, &HXf);
    arm_mat_sub_f32(&LOC, &HXf, &Z_HXf);
    arm_mat_mult_f32(&Kf, &Z_HXf, &KZ_HXf);
    arm_mat_add_f32(&Xf, &KZ_HXf, &Xf);
    arm_mat_mult_f32(&Kf, &Hf, &KHf);
    arm_mat_sub_f32(&If, &KHf, &I_KHf);
    arm_mat_mult_f32(&I_KHf， &Pf, &Pf);

	return 0;

}




    
    
    
    
  