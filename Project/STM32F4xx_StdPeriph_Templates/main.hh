/*
 * =====================================================================================
 *
 *       Filename:  main.hh
 *
 *    Description:  header file for main.cc
 *
 *        Version:  1.0
 *        Created:  9/24/2015 9:53:13 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lei Song
 *   Organization:  Beijing GWZZ
 *
 * =====================================================================================
 */
#ifndef __MAIN_HH__
#define __MAIN_HH__
#include <stdint.h>
#include "stm324xg_eval.h"
#include <arm_math.h>


#ifdef __cplusplus
extern "C" {
#endif
#define BUFFER_SIZE 128
    /* Base address of the Flash sectors */

    typedef enum{
        WAITING,
        IN_PREAMBLE,
        IN_PAYLOAD
    } sRecvStage;

    static uint8_t pop8();
    static int32_t pop32();
    static double popDouble();
    void ClearRxUart1();
    void ClearRxUart2();
    void initUSART();
    void USART1_NVIC_Configuration();
    void USART2_NVIC_Configuration();
    void decodeMsg(uint8_t curByte);
    void testDSP();
#ifdef __cplusplus
} /* extern "C" */
#endif

#endif //__MAIN_HH__
