#include "arm_math.h"
#include "stdint.h"
#include "attitudeSolution.h"

//Attitude solution value
//#define PI 3.14159
//#define RATE_R2D 0.0174533 
//#define RATE_D2R 57.2957

void attitudeInit(void);

void attitudeSolution(int16_t rawData[],uint8_t mode);

//float32_t timespan=0.00521;//interval of two attitude solution
float32_t timespan=0.011;//interval of two attitude solution
float32_t q_g[4]={0,0,0,0},
      q_he[4]={0,0,0,0},
      q_z[4]={0,0,0,0},
      h_qg[3]={0,0,0},
      angle_delta[3]={0,0,0};
float32_t euler[3];

float32_t Q_k[4]=
        {
            1.0, 		0.0, 		0.0, 		0.0
        },
        lQ_k[4]=
        {
            1.0, 		0.0, 		0.0, 		0.0
        },
     Q_k1_k[4]=
        {

            1.0, 		0.0, 		0.0, 		0.0
        },
    Q_k1[4]=
        {
            1.0, 		0.0, 		0.0, 		0.0
        },
    Z[4]={1.0,0.0,0.0,0.0},
    P_k1[16]=
        {
            1.0, 		0.0, 		0.0, 		0.0,  
            0.0, 		1.0, 		0.0, 		0.0, 
            0.0, 		0.0, 		1.0, 		0.0, 
            0.0, 		0.0, 		0.0, 		1.0, 
        },
    P_k_k1[16]=
        {
            1.0, 		0.0, 		0.0, 		0.0,  
            0.0, 		1.0, 		0.0, 		0.0, 
            0.0, 		0.0, 		1.0, 		0.0, 
            0.0, 		0.0, 		0.0, 		1.0, 
        },
    P_k[16]=
        {
            1.0, 		0.0, 		0.0, 		0.0,  
            0.0, 		1.0, 		0.0, 		0.0, 
            0.0, 		0.0, 		1.0, 		0.0, 
            0.0, 		0.0, 		0.0, 		1.0, 
        },
    H[16]=
        {
            1.0, 		0.0, 		0.0, 		0.0,  
            0.0, 		1.0, 		0.0, 		0.0, 
            0.0, 		0.0, 		1.0, 		0.0, 
            0.0, 		0.0, 		0.0, 		1.0, 
        },
    T[16]=
        {
            1.0, 		0.0, 		0.0, 		0.0,  
            0.0, 		1.0, 		0.0, 		0.0, 
            0.0, 		0.0, 		1.0, 		0.0, 
            0.0, 		0.0, 		0.0, 		1.0, 
        },
    Q_attitude[16]=
        {
            1.0, 		0.0, 		0.0, 		0.0,  
            0.0, 		1.0, 		0.0, 		0.0, 
            0.0, 		0.0, 		1.0, 		0.0, 
            0.0, 		0.0, 		0.0, 		1.0, 
        },
     R_attitude[16]=
        {
            10.0, 		0.0, 		0.0, 		0.0,  
            0.0, 		10.0, 		0.0, 		0.0, 
            0.0, 		0.0, 		10.0, 		0.0, 
            0.0, 		0.0, 		0.0, 		10.0, 
        },
     Theta[16]=
        {
            1.0, 		0.0, 		0.0, 		0.0,  
            0.0, 		1.0, 		0.0, 		0.0, 
            0.0, 		0.0, 		1.0, 		0.0, 
            0.0, 		0.0, 		0.0, 		1.0, 
        },
     Theta_trans[16]=//transpose of Theta
        {
            1.0, 		0.0, 		0.0, 		0.0,  
            0.0, 		1.0, 		0.0, 		0.0, 
            0.0, 		0.0, 		1.0, 		0.0, 
            0.0, 		0.0, 		0.0, 		1.0, 
        },
	 K_k[16],
     I[16]=
        {
            1.0, 		0.0, 		0.0, 		0.0,  
            0.0, 		1.0, 		0.0, 		0.0, 
            0.0, 		0.0, 		1.0, 		0.0, 
            0.0, 		0.0, 		0.0, 		1.0, 
        },
     temp[5][16],//store intermediate values
     tempv[3][4];
arm_matrix_instance_f32 Q_k_m,Q_k1_k_m,Q_k1_m,//4*1
                        Z_m,//4*1
                        P_k1_m,P_k_k1_m,P_k_m,//4*4
						K_k_m,//4*4
                        H_m,T_m,Q_m,R_m,//4*4
						Theta_m,Theta_trans_m,//4*4
                        I_m,//4*4
                        temp_m[5],//temp[i]:4*4
                        tempv_m[3];//tempv_m[i]:4*1
arm_status KMF_status;
int16_t 
 aoffset[3]={35,190,2400},
 woffset[3]={12,-9,-26},
 hoffset[3]={-25,-16,40};

float32_t a[3],w[3],h[3];

float32_t lasta[3],lastw[3],lasth[3],lasteuler[3];//value of smoother

float32_t cy,tempp;
uint8_t asdfg=0;
extern float linearyaw;
extern int yawmeasured;


void setImuBiasOffset(int16_t abias[], int16_t wbias[], int16_t hbias[])
{
  for (int i=0; i<3; i++)
  {
    aoffset[i] = abias[i];
    woffset[i] = wbias[i];
    hoffset[i] = hbias[i];
  }
  
}
void attitudeInit()
{
    //Init the matrix used in KMF
    arm_mat_init_f32(&Q_k_m, 4, 1, (float32_t *)Q_k);
    arm_mat_init_f32(&Q_k1_k_m, 4, 1, (float32_t *)Q_k1_k);
    arm_mat_init_f32(&Q_k1_m, 4, 1, (float32_t *)Q_k1); 
    arm_mat_init_f32(&Z_m, 4, 1, (float32_t *)Z);
    arm_mat_init_f32(&P_k1_m, 4, 4, (float32_t *)P_k1);
    arm_mat_init_f32(&P_k_k1_m, 4, 4, (float32_t *)P_k_k1);
    arm_mat_init_f32(&P_k_m, 4, 4, (float32_t *)P_k);
    arm_mat_init_f32(&K_k_m, 4, 4, (float32_t *)K_k);
    arm_mat_init_f32(&H_m, 4, 4, (float32_t *)H);
    arm_mat_init_f32(&T_m, 4, 4, (float32_t *)T);
    arm_mat_init_f32(&Q_m, 4, 4, (float32_t *)Q_attitude);
    arm_mat_init_f32(&R_m, 4, 4, (float32_t *)R_attitude);
    arm_mat_init_f32(&Theta_m, 4, 4, (float32_t *)Theta);
    arm_mat_init_f32(&Theta_trans_m, 4, 4, (float32_t *)Theta_trans);
    arm_mat_init_f32(&I_m, 4, 4, (float32_t *)I);
    for(uint8_t i=0;i<5;i++)
    {
        arm_mat_init_f32(&temp_m[i], 4, 4, (float32_t *)temp[i]);
    }     
    for(uint8_t i=0;i<3;i++)
    {
        arm_mat_init_f32(&tempv_m[i], 4, 1, (float32_t *)tempv[i]);
    }     
}

void attitudeSolution(int16_t rawData[],uint8_t mode)
{
    //rawData[0-2] correspond to ax-az, the frame is east-north-ground.
    //Let az=-az to get the measure under east-north-sky frame.
    //rawData[3-5] correspond to wx-wz, their forward direction fellow left-handed frame.
    //To decode data, refer MPU6050_Initialize() in MPU6050.c to get the measurement scale.
    //rawData is 16-bit resulution.
    //For scale_acc=2g, ax=(float)(rawData[0]/2^15*2)g
    //For scale_w=250,wx=(float)(rawData[3]/2^15*250) degree/s

    //mode==1,use megnet
    //mode==0,don't use megnet
    for(uint8_t i=0;i<3;i++)
    {
      a[i]=(float32_t)(rawData[i]+aoffset[i])/16384.0f;
      w[i]=-1*(float32_t)(rawData[i+3]+woffset[i])*0.00762939453f; 
    }
    a[2]=-a[2];
    w[2]=-w[2];
    if(mode)
    {
      h[0]=(float32_t)(-(rawData[1+6]+hoffset[1]));
      h[1]=(float32_t)(-(rawData[0+6]+hoffset[0]));
      h[2]=(float32_t)(-(rawData[2+6]+hoffset[2]));
    }

    for(uint8_t i=0;i<3;i++)
    {
      //a[i]=0.01*a[i]+0.99*lasta[i];
      lasta[i]=a[i];
      //w[i]=0.01*w[i]+0.99*lastw[i];
      lastw[i]=w[i];
      if(mode)
        lasth[i]=h[i];
    }
    if(!(rawData[6]|rawData[7]|rawData[8])&&mode)
    {
        h[0]=lasth[0];
        h[1]=lasth[1];
        h[2]=lasth[2];
        
    }
    //caculte Z
    //q_g  q_xy
    float32_t theta4 = (float32_t)acosf(-a[2] / sqrtl(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]));
    q_g[0] = cosf(theta4 / 2);
    q_g[1] = sinf(theta4 / 2) * (-a[1]) / sqrtl(a[0] * a[0] + a[1] * a[1]);
    q_g[2] = sinf(theta4 / 2) * a[0] / sqrtl(a[0] * a[0] + a[1] * a[1]);
    q_g[3] = 0;
    if(mode)
    {
      
      //将探测到的地磁场量还原回绕X和Y轴旋转之前
      h_qg[0] = (q_g[0] * q_g[0] + q_g[1] * q_g[1] - q_g[2] * q_g[2]) * h[0] + 2 * (q_g[1] * q_g[2] ) * h[1] + 2 * (q_g[0] * q_g[2]) * h[2];
      h_qg[1] = (q_g[0] * q_g[0] - q_g[1] * q_g[1] + q_g[2] * q_g[2]) * h[1] + 2 * (q_g[1] * q_g[2] ) * h[0] + 2 * (- q_g[0] * q_g[1]) * h[2];
      
       
      //q_z
      //判断其与Y轴(本地正北)的夹角和对应转轴
      float32_t u_he = h_qg[0]>=0?1:(-1);
      float theta3 = acosf((h_qg[1]) /  sqrtf(h_qg[0] * h_qg[0] + h_qg[1] * h_qg[1]));
      q_he[0] = arm_cos_f32(theta3 / 2);
      q_he[3] = u_he * arm_sin_f32(theta3 / 2);

      Z[0] = q_g[0] * q_he[0];
      Z[1] = q_g[1] * q_he[0] - q_g[2] * q_he[3];
      Z[2] = q_g[2] * q_he[0] + q_g[1] * q_he[3];
      Z[3] = q_g[0] * q_he[3];

      if (Z[0]*Z[0] < 0.01)//q0较小时，说明转动角接近180度，这时候转轴可能正可能负，尽量使其与卡尔曼滤波结果接近，避免突变
      {
          for(uint8_t i=1;i<4;i++)
          if (Z[i]*Z[i] > 0.01f)
          {
              if (Q_k[i] / Z[i]<0)
              {
                  for (uint8_t j = 1; j < 4; j++)
                  {
                      Z[j] = -Z[j];
                  }
                  break;
              }

          }
      }
    }
    else
    {
        
       for(uint8_t i=0;i<4;i++)
      {
        Z[i]=q_g[i];     
      }
    
    }
      

    //caculte Q_k1_k
    for(uint8_t i=0;i<3;i++)
    {
        angle_delta[i]=w[i] * timespan *RATE_D2R/2;
    }
    float32_t delta_s = (float32_t)sqrtf(angle_delta[0] * angle_delta[0] + angle_delta[1] * angle_delta[1] + angle_delta[2] * angle_delta[2])/8;
    //second-order approximation
    Q_k1_k[0] = (1 - delta_s) * Q_k[0] - angle_delta[0] * Q_k[1] - angle_delta[1] * Q_k[2] - angle_delta[2] * Q_k[3];
    Q_k1_k[1] = angle_delta[0] * Q_k[0] + (1 - delta_s) * Q_k[1] + angle_delta[2] * Q_k[2] - angle_delta[1] * Q_k[3];
    Q_k1_k[2] = angle_delta[1] * Q_k[0] - angle_delta[2] * Q_k[1] + (1 - delta_s) * Q_k[2] + angle_delta[0] * Q_k[3];
    Q_k1_k[3] = angle_delta[2] * Q_k[0] + angle_delta[1] * Q_k[1] - angle_delta[0] * Q_k[2] + (1 - delta_s) * Q_k[3];
    

	/*
    if (fabsf((a[0] * a[0] + a[1] * a[1] + a[2] * a[2]) - 1)< 0.05 && fabsf(w[0] * w[0] + w[1] * w[1] + w[2] * w[2]) < 20&&mode)
    {

        //KMF filter
        Theta[0*4+ 0] = 1;
        Theta[0*4+ 1] = -angle_delta[0];
        Theta[0*4+ 2] = -angle_delta[1];
        Theta[0*4+ 3] = -angle_delta[2];
        Theta[1*4+ 0] = angle_delta[0];
        Theta[1*4+ 1] = 1; 
        Theta[1*4+ 2] = angle_delta[2];
        Theta[1*4+ 3] = -angle_delta[1];
        Theta[2*4+ 0] = angle_delta[1];
        Theta[2*4+ 1] = -angle_delta[2];
        Theta[2*4+ 2] = 1;
        Theta[2*4+ 3] = angle_delta[0];
        Theta[3*4+ 0] = angle_delta[2];
        Theta[3*4+ 1] = angle_delta[1];
        Theta[3*4+ 2] = -angle_delta[0];
        Theta[3*4+ 3] = 1;
            
        //P_k_k1 = Theta * P_k1 * Theta' + T * Q * T';
        //T=I, then Q=T*Q*T'
        arm_mat_trans_f32(&Theta_m, &Theta_trans_m); 
        arm_mat_mult_f32(&Theta_m, &P_k1_m, &temp_m[0]);
        arm_mat_mult_f32(&temp_m[0], &Theta_trans_m, &temp_m[1]);
        arm_mat_add_f32(&temp_m[1], &Q_m, &P_k_k1_m);
        
        //K_k = P_k_k1 * H' * (H * P_k_k1 * H' + R)^(-1);
        //H=I, K_k = P_k_k1 * (P_k_k1 + R)^(-1);
        arm_mat_add_f32(&P_k_k1_m, &R_m, &temp_m[0]);
        arm_mat_inverse_f32(&temp_m[0], &temp_m[1]);
        arm_mat_mult_f32(&P_k_k1_m, &temp_m[1], &K_k_m);
        
        //P_k = (I - K_k * H) * P_k_k1;
        //H=I, P_k=(I-K_k)*P_k_k1
        arm_mat_scale_f32(&K_k_m,(float32_t)(-1),&temp_m[0]);
        arm_mat_add_f32(&I_m, &temp_m[0], &temp_m[1]);
        arm_mat_mult_f32(&temp_m[1], &P_k_k1_m, &P_k1_m);

        //Q_k1 = Q_k1_k + K_k * (Z - H * Q_k1_k);
        //Q_k1 = Q_k1_k + K_k * (Z-Q_k1_k);
        //Q_k=Q_k1
        arm_mat_scale_f32(&Q_k1_k_m,(float32_t)(-1.0),&tempv_m[0]);
        arm_mat_add_f32(&Z_m, &tempv_m[0], &tempv_m[1]);
         arm_mat_mult_f32(&K_k_m, &tempv_m[1], &tempv_m[2]);
        arm_mat_add_f32(&Q_k1_k_m, &tempv_m[2], &Q_k_m);
    }    
    else
    {
        for(uint8_t i=0;i<4;i++)
        {
            Q_k[i]=Q_k1_k[i];
        }
    } 
	*/
	if(yawmeasured)
	{
		yawmeasured=0;
		Z[0]=cosf(linearyaw/2);
		Z[1]=0;
		Z[2]=0;
		Z[3]=sinf(linearyaw/2);
		//KMF filter
		Theta[0*4+ 0] = 1;
		Theta[0*4+ 1] = -angle_delta[0];
		Theta[0*4+ 2] = -angle_delta[1];
		Theta[0*4+ 3] = -angle_delta[2];
		Theta[1*4+ 0] = angle_delta[0];
		Theta[1*4+ 1] = 1; 
		Theta[1*4+ 2] = angle_delta[2];
		Theta[1*4+ 3] = -angle_delta[1];
		Theta[2*4+ 0] = angle_delta[1];
		Theta[2*4+ 1] = -angle_delta[2];
		Theta[2*4+ 2] = 1;
		Theta[2*4+ 3] = angle_delta[0];
		Theta[3*4+ 0] = angle_delta[2];
		Theta[3*4+ 1] = -angle_delta[1];
		Theta[3*4+ 2] = -angle_delta[0];
		Theta[3*4+ 3] = 1;


		//P_k_k1 = Theta * P_k1 * Theta' + T * Q * T';
		//T=I, then Q=T*Q*T'
		arm_mat_trans_f32(&Theta_m, &Theta_trans_m); 
		arm_mat_mult_f32(&Theta_m, &P_k1_m, &temp_m[0]);
		arm_mat_mult_f32(&temp_m[0], &Theta_trans_m, &temp_m[1]);
		arm_mat_add_f32(&temp_m[1], &Q_m, &P_k_k1_m);
		
		//K_k = P_k_k1 * H' * (H * P_k_k1 * H' + R)^(-1);
		//H=I, K_k = P_k_k1 * (P_k_k1 + R)^(-1);
		arm_mat_add_f32(&P_k_k1_m, &R_m, &temp_m[0]);
		arm_mat_inverse_f32(&temp_m[0], &temp_m[1]);
		arm_mat_mult_f32(&P_k_k1_m, &temp_m[1], &K_k_m);
		
		//P_k = (I - K_k * H) * P_k_k1;
		//H=I, P_k=(I-K_k)*P_k_k1
		arm_mat_scale_f32(&K_k_m,(float32_t)(-1),&temp_m[0]);
		arm_mat_add_f32(&I_m, &temp_m[0], &temp_m[1]);
		arm_mat_mult_f32(&temp_m[1], &P_k_k1_m, &P_k1_m);

        //Q_k1 = Q_k1_k + K_k * (Z - H * Q_k1_k);
	    //Q_k1 = Q_k1_k + K_k * (Z-Q_k1_k);
	    //Q_k=Q_k1
	    arm_mat_scale_f32(&Q_k1_k_m,(float32_t)(-1.0),&tempv_m[0]);
        arm_mat_add_f32(&Z_m, &tempv_m[0], &tempv_m[1]);
        arm_mat_mult_f32(&K_k_m, &tempv_m[1], &tempv_m[2]);
        arm_mat_add_f32(&Q_k1_k_m, &tempv_m[2], &Q_k_m);
		
	}
	else
	{
		for(uint8_t i=0;i<4;i++)
                {
                    Q_k[i]=Q_k1_k[i];
                }		
	}


	
//    for(uint8_t i=0;i<4;i++)
//    {
//        Q_k[i]=Q_k1_k[i];
//    }
    //normalize
    float norm = sqrtf(Q_k[0] * Q_k[0] + Q_k[1] * Q_k[1] + Q_k[2] * Q_k[2] + Q_k[3] * Q_k[3]);
    if (fabsf(norm-1) > 0.05)
    {
        for(uint8_t i=0;i<4;i++)
        {
            Q_k[i] /= norm;
        }
    }
    euler[1] = (float32_t)asinf(-2 * (Q_k[1] * Q_k[3] - Q_k[0] * Q_k[2]));
    cy = arm_cos_f32(euler[1]);
    tempp=1 - 2 * (Q_k[1] * Q_k[1] + Q_k[2] * Q_k[2]);
    if ( tempp> 0)
        euler[0]=atanf(2 * (Q_k[0] * Q_k[1] + Q_k[2] * Q_k[3]) / tempp);
    else
    {
        euler[0] =PI * ((Q_k[0] * Q_k[1] + Q_k[2] * Q_k[3])>0?1:(-1)) + atanf(2 * (Q_k[0] * Q_k[1] + Q_k[2] * Q_k[3]) / tempp);
    }
    tempp=1 - 2 * (Q_k[2] * Q_k[2] + Q_k[3] * Q_k[3]);
    if (tempp>0)
    {
        euler[2] =atanf(2 * (Q_k[1] * Q_k[2] + Q_k[0] * Q_k[3]) /tempp);
    }
    else
    {
        euler[2] =PI * ((Q_k[1] * Q_k[2] + Q_k[0] * Q_k[3])>0?1:(-1)) + atanf(2 * (Q_k[1] * Q_k[2] + Q_k[0] * Q_k[3]) / tempp);
    }


    for(uint8_t i=0;i<3;i++)
    {
        att_euler[i]=euler[i]*RATE_R2D;
//        euler[i]=euler[i]*0.3+0.7*lasteuler[i];
        //lasteuler[i]=euler[i];
    }
    acc[0] = -((Q_k[0] * Q_k[0] + Q_k[1] * Q_k[1] - Q_k[2] * Q_k[2] - Q_k[3] * Q_k[3]) * a[0] + 2 * (Q_k[1] * Q_k[2] - Q_k[0] * Q_k[3]) * a[1] + 2 * (Q_k[0] * Q_k[2] + Q_k[1] * Q_k[3]) * a[2]);
    acc[1] = -((Q_k[0] * Q_k[0] - Q_k[1] * Q_k[1] + Q_k[2] * Q_k[2] - Q_k[3] * Q_k[3]) * a[1] + 2 * (Q_k[1] * Q_k[2] + Q_k[0] * Q_k[3]) * a[0] + 2 * (Q_k[2] * Q_k[3] - Q_k[0] * Q_k[1]) * a[2]);
    acc[2] = -1-((Q_k[0] * Q_k[0] - Q_k[1] * Q_k[1] - Q_k[2] * Q_k[2] + Q_k[3] * Q_k[3]) * a[2] + 2 * (Q_k[1] * Q_k[3] - Q_k[0] * Q_k[2]) * a[0] + 2 * (Q_k[0] * Q_k[1] + Q_k[2] * Q_k[3]) * a[1]);
    pal[0] = -((Q_k[0] * Q_k[0] + Q_k[1] * Q_k[1] - Q_k[2] * Q_k[2] - Q_k[3] * Q_k[3]) * w[0] + 2 * (Q_k[1] * Q_k[2] - Q_k[0] * Q_k[3]) * w[1] + 2 * (Q_k[0] * Q_k[2] + Q_k[1] * Q_k[3]) * w[2]);
    pal[1] = -((Q_k[0] * Q_k[0] - Q_k[1] * Q_k[1] + Q_k[2] * Q_k[2] - Q_k[3] * Q_k[3]) * w[1] + 2 * (Q_k[1] * Q_k[2] + Q_k[0] * Q_k[3]) * w[0] + 2 * (Q_k[2] * Q_k[3] - Q_k[0] * Q_k[1]) * w[2]);
    pal[2] = -((Q_k[0] * Q_k[0] - Q_k[1] * Q_k[1] - Q_k[2] * Q_k[2] + Q_k[3] * Q_k[3]) * w[2] + 2 * (Q_k[1] * Q_k[3] - Q_k[0] * Q_k[2]) * w[0] + 2 * (Q_k[0] * Q_k[1] + Q_k[2] * Q_k[3]) * w[1]);

     
//    if(fabsf(euler[0])<5&&fabsf(euler[1])<5&&fabsf(euler[2])<5)
//        {
//            
//
//            asdfg++;
//        }
//    asdfg++;
//    for(uint8_t i=0;i<4;i++)
//                
//        {
//            lQ_k[i]=Q_k[i];
//        }
//         asdfg++;  
//    for(uint8_t i=0;i<3;i++)
//    {
//       // euler[i]*=RATE_R2D;
////        euler[i]=euler[i]*0.3+0.7*lasteuler[i];
//        //lasteuler[i]=euler[i];
//       // euler[i]=w[i];
//    }
//   
}

