#include <stdint.h>
#include <Msg.hh>
#include <string.h>
#include <stdio.h>
#include "CircularBuffer.hh"
#include "stm324xg_eval.h"
#define TMP_BUF_LEN 100
namespace decodeRange{
#define RANGE_BUFFER_SIZE 512
    char sBuf[100];
    uint8_t buffer_data[RANGE_BUFFER_SIZE];
    CircularBuffer rangeBuffer(RANGE_BUFFER_SIZE,buffer_data);
    uint16_t getLength(){
        return rangeBuffer.getLength();
    }
    void reset(){
        rangeBuffer.reset();
    }
    void push(uint8_t data){
        rangeBuffer.push(data);
    }
    uint16_t capacity(){
        return rangeBuffer.getRemainingCapacity();
    }
    //ret:
    //      -1 wrong pkt
    //      positive value pkt length
    //      0 incomplete pkt
    int8_t decode(Msg_t* msg){
#ifdef DEBUG_LEVLE_2
        sprintf(sBuf,"data len = %u\n\r",rangeBuffer.getLength());
        print(sBuf);
        int k;
        for(k=0;k<rangeBuffer.getLength();k++){
             sprintf(sBuf,"%x ",rangeBuffer[k]);
             print(sBuf);
        }
        print("\n\r");
#endif
        //handle the dummy
        while(rangeBuffer.getLength()>0 && rangeBuffer[0] !=0x7E)
            rangeBuffer.pop();
        if(rangeBuffer.getLength() < 2)
            return 0;
        if(rangeBuffer[1]==0x7E)
            rangeBuffer.pop();
        // now pkt is start from 7E,we will find net 7e
        uint8_t j = 1;
        while(j<rangeBuffer.getLength() && rangeBuffer[j] != 0x7e )
            j++;
        if(j>=rangeBuffer.getLength())
            return 0;
        //now we have a complete pkt in rangebuffer;
        uint8_t temp[TMP_BUF_LEN];

        j=0;
        rangeBuffer.pop();
        //while(rangeBuffer[0]!=0x7E && rangeBuffer.getLength() >1){
        while(rangeBuffer[0]!=0x7E){
            if(rangeBuffer[0] ==0x7D && rangeBuffer[1] == 0x5E){
                temp[j++] = 0x7E;
                rangeBuffer.pop();
                rangeBuffer.pop();
            }else if(rangeBuffer[0] ==0x7D &&rangeBuffer[1] == 0x5D ){
                temp[j++] = 0x7D;
                rangeBuffer.pop();
                rangeBuffer.pop();
            }else{
                 temp[j++] = rangeBuffer.pop();
            }
        }
        rangeBuffer.pop();
#ifdef DEBUG_LEVEL_2
        //sprintf(sBuf,"j = %d\n\r",j);
        //print(sBuf);
#endif
        if(j == sizeof(Msg_t)){
            memcpy((void*)msg,(void*)temp,j);
            return j;
        }else{
            return -1;
        }
    }
}
