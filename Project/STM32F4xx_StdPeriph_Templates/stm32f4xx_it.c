/** ******************************************************************************
  * @file    Project/STM32F4xx_StdPeriph_Template/stm32f4xx_it.c
  * @author  MCD Application Team
  * @version V1.1.0
  * @date    18-January-2013
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_it.h"
#include "stm32f4xx_conf.h"
#include "stm324xg_eval.h"
#include "yaEkf6.hh"
//#include "decodeRange.hh"
#include "main.hh"
#include "MPU6050.h"
#include <stdio.h>
#include <Msg.hh>
extern volatile uint8_t RxCounter1;
extern volatile uint8_t IMUReady;
extern volatile uint8_t MegReady;
extern volatile uint8_t RxBufferOfUart1[BUFFER_SIZE ];
extern __IO uint8_t spiBuf[];
extern __IO uint16_t spiBufLen;


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/
static uint8_t cnt =0;
void EXTI15_10_IRQHandler(void)
{
    if(EXTI_GetITStatus(EXTI_Line11)){
	EXTI_ClearITPendingBit(EXTI_Line11);
	cnt++;
	if(cnt==255){
	    cnt=0;
#ifdef USE_MPU6050_I2C_DMA
	    printf("imu->dma\n\r");
	    MPU6050_DMA_Read();
#else
	    //updatePosByIMU(1, 4, 2);  //PARA: 1.IMU Option; 2 Compass option; 3: freqency
	    //printf("called IMU\n\r",cnt);
#endif
	}
    }
}


/**
  * @brief   This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
volatile uint32_t timeStamp;
void SysTick_Handler(void)
{
    timeStamp++;
}

extern uint8_t ADCReady;
void DMA2_Stream0_IRQHandler(void){
    if(DMA_GetITStatus(DMA2_Stream0,DMA_IT_TCIF0)!=RESET){
        DMA_ClearITPendingBit(DMA2_Stream0,DMA_IT_HTIF0);
        ADCReady = 1;
    }
}

extern uint8_t* USART1InBuf;
extern uint16_t USART1InBufLen;
extern __IO uint8_t USARTpktReady;
void DMA2_Stream2_IRQHandler(void){
    if(DMA_GetITStatus(DMA2_Stream2,DMA_IT_TCIF4)!=RESET){
        DMA_ClearITPendingBit(DMA2_Stream2,DMA_IT_TCIF4);
    }
    if(DMA_GetITStatus(DMA2_Stream2,DMA_IT_HTIF4)!=RESET){
        DMA_ClearITPendingBit(DMA2_Stream2,DMA_IT_HTIF4);
    }
    USARTpktReady = 1;
}
// communication with PC or mobile
extern sRecvStage uart1Stage;

void TIM3_IRQHandler(void){
    if(TIM_GetITStatus(TIM3,TIM_IT_CC1)!= RESET){
        TIM_ClearITPendingBit(TIM3,TIM_IT_CC1);
       // IMUReady = 1;
    }
}

void TIM2_IRQHandler(void)
{
  if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
  {
    IMUReady = 1;
    MegReady++;
    TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
  }
}

extern __IO uint8_t *USARTInBuf;
extern __IO uint8_t *USARTOutBuf;
extern __IO uint8_t USARTInBufLen,USARTOutBufLen;
void USART1_IRQHandler(void){
    if(USART_GetITStatus(EVAL_COM1,USART_IT_IDLE)!=RESET){
        USART_ClearITPendingBit(EVAL_COM1,USART_IT_IDLE);
        USARTInBufLen = DMA_GetCurrDataCounter(DMA2_Stream2);
        USARTpktReady = 1;
    }
    /*
    if(USART_GetITStatus(EVAL_COM1,USART_IT_RXNE) == SET){
        uint8_t curByte = USART_ReceiveData(EVAL_COM1);
        USARTInBuf[USARTInBufLen++] = curByte;
        if(curByte==0x7E){
            if(USARTInBufLen == 1){
                USARTInBufLen = 0;
            }else{
                USARTOutBufLen =  USARTInBufLen;
                USARTInBufLen = 0;
                __IO uint8_t *bufPtr;
                bufPtr = USARTInBuf;
                USARTInBuf = USARTOutBuf;
                USARTOutBuf = bufPtr;
                USARTpktReady = 1;
            }
        }
        USART_ClearFlag(EVAL_COM1,USART_FLAG_RXNE );
    }*/
}


void USART2_IRQHandler(void){
    if(USART_GetITStatus(EVAL_COM2,USART_IT_RXNE)){
        USART_ClearFlag(EVAL_COM2,USART_FLAG_RXNE );
    }
}

extern sRecvStage curStage;
static uint8_t curByte;
extern volatile uint8_t ID,Seq,wrongPkt,pktReady;
extern volatile uint16_t firstRisingEdge;

// communicaiton with cc2530
uint8_t curByte;
extern __IO uint8_t *spiInBuf;
extern __IO uint8_t *spiOutBuf;
extern __IO uint16_t spiInBufLen,spiOutBufLen;
extern __IO uint8_t badSPI;
void SPI1_IRQHandler(void){
    if (SPI_I2S_GetITStatus(SPI1, SPI_I2S_IT_RXNE) == SET){
        curByte = SPI_ReceiveData(SPI1);
        if(spiInBufLen < 60){
            spiInBuf[spiInBufLen++] = curByte;
        }else{
            spiInBufLen = 0;
            badSPI = 1;
        }
        if(curByte == 0x7e ){
            if(spiInBufLen == 1){
                spiInBufLen = 0;
            }else{
                spiOutBufLen =  spiInBufLen;
                spiInBufLen = 0;
                __IO uint8_t *spiPtr;
                spiPtr = spiInBuf;
                spiInBuf = spiOutBuf;
                spiOutBuf = spiPtr;
                pktReady = 1;
            }
        }
        SPI_I2S_ClearFlag(SPI1,SPI_I2S_FLAG_RXNE);
    }
}


void DMA1_Stream4_IRQHandler(void){
    if(DMA_GetFlagStatus(DMA1_Stream4,DMA_FLAG_TCIF4)){
	DMA_ClearFlag(DMA1_Stream4,DMA_FLAG_TCIF4);
        SPI_I2S_DMACmd(SPI_OUT, SPI_I2S_DMAReq_Tx, DISABLE); //add by jian
 //       printf("dma output\n");
    }
}

int16_t AccelGyro[6];
extern uint8_t I2C_Rx_Buffer[14];
void DMA1_Stream0_IRQHandler(void){
    if (DMA_GetFlagStatus(DMA1_Stream0, DMA_FLAG_TCIF0))
    {
	/* Clear transmission complete flag */
	DMA_ClearFlag(DMA1_Stream0,DMA_FLAG_TCIF0);

	I2C_DMACmd(MPU6050_I2C, DISABLE);
	/* Send I2C1 STOP Condition */
	I2C_GenerateSTOP(MPU6050_I2C, ENABLE);
	/* Disable DMA channel*/
	DMA_Cmd(DMA1_Stream0, DISABLE);

	//Read Accel data from byte 0 to byte 2
	int i;
	for(i=0; i<3; i++) 
	    AccelGyro[i]=((int16_t)((uint16_t)I2C_Rx_Buffer[2*i] << 8) + I2C_Rx_Buffer[2*i+1]);
	//Skip byte 3 of temperature data
	//Read Gyro data from byte 4 to byte 6
	for(i=4; i<7; i++)
	    AccelGyro[i-1]=((int16_t)((uint16_t)I2C_Rx_Buffer[2*i] << 8) + I2C_Rx_Buffer[2*i+1]);
	printf("acc: %d %d %d \n\r",AccelGyro[0],AccelGyro[1],AccelGyro[2]);

    }
}

/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f40xx.s/startup_stm32f427x.s).                         */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
