
/* ------------------------------------------------------------------------------------------------
 *                                     Copyright (2014-2024)
 *  All source is private properties of Beijing Ganweizhizhu Co. Lt.
 * @file    yaEkf.cpp
 * @author  Lei Song
 * @version V4.5.0
 * @date    07-March-2011
 * @brief   This is another implementation of the EKF
 * ------------------------------------------------------------------------------------------------
 */


/* ------------------------------------------------------------------------------------------------ *                                     Includes
 * ------------------------------------------------------------------------------------------------
 */
#include "stm324xg_eval.h"
#include "stdint.h"
#include "yaEkf6.hh"
#include "fusion.hh"
#include <stdlib.h>
#include <stdio.h>
#include "arm_math.h"
#include "attitudeSolution.h"
#include <stdint.h>
#include "mpu6050.h"
/* ------------------------------------------------------------------------------------------------
 *                                     Defs
 * ------------------------------------------------------------------------------------------------
 */

/* ------------------------------------------------------------------------------------------------
 *                                     External functions
 * ------------------------------------------------------------------------------------------------
 */

/* ------------------------------------------------------------------------------------------------
 *                                     External variables
 * ------------------------------------------------------------------------------------------------
 */

/* ------------------------------------------------------------------------------------------------
 *                                     Local functions
 * ------------------------------------------------------------------------------------------------
 */


extern __IO uint8_t numBeacon;

arm_matrix_instance_f32 z;
float32_t zm[OB_VEC_LEN];
float distCM;
uint8_t ID;

//dMatrix X;
arm_matrix_instance_f32 X;
float32_t Xm[STATE_VEC_LEN];

arm_matrix_instance_f32 R;
float32_t Rm[OB_VEC_LEN][OB_VEC_LEN];

arm_matrix_instance_f32 Q;
float32_t Qm[STATE_VEC_LEN][STATE_VEC_LEN];

arm_matrix_instance_f32 P;
float32_t Pm[STATE_VEC_LEN][STATE_VEC_LEN];

static float B[MAX_NUM_BEACON][3];
static float offset[MAX_NUM_BEACON];
static float beaconNum;
static int beaconID[MAX_NUM_BEACON];
                    
int imuOn;
int compassInterval;
int imuInterval;
extern volatile uint32_t timeStamp;


static float distWindow[WINDOW_SIZE]={0.0,0.0,0.0,0.0,0.0,0.0,0.0};
static int beaconWindow[WINDOW_SIZE]={-1,-1,-1,-1,-1,-1,-1};
float estPosWindow[WINDOW_SIZE][3]={{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0}};
float vWindow[WINDOW_SIZE][3]={{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0}};
int windex = 0;

static float32_t distBufferWindow[WINDOW_SIZE][MAX_NUM_BEACON][3]; 
static int32_t DisBufferTop[MAX_NUM_BEACON];

static int confidence=0;
static float tWindow[WINDOW_SIZE] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0};

char outBuf[100];

float32_t RESIDUETRESHOLD; 
float32_t JUMPTHRESHOLD;
uint8_t MOVESIZE = 1; 
float MINDIS;
float MAXDIS;
uint32_t ttime = 0;

float r;  //观测方差
float q;  //预测方差
float height;

#define WINDOW_SIZE 7  // 1;

float xl[3]={10.0, 10.0, 10.0};

float32_t loc[9];
float sumDisResidue = 0;
int FusionInitialized=0;

float32_t residue[WINDOW_SIZE];
uint32_t v;
uint8_t p;
uint8_t EKFInit = 0;
uint16_t failCount = 0;

float step=0.053;
int imuCount=0;
float32_t accgyro[6];
float att_euler[3];
float acc[3];
float pal[3];
/* ------------------------------------------------------------------------------------------------
 *                                     Local variables
 * ------------------------------------------------------------------------------------------------
 */

/* ------------------------------------------------------------------------------------------------
 *                                     Global functions
 * ------------------------------------------------------------------------------------------------
 */

/* ------------------------------------------------------------------------------------------------
 *                                     Global variables
 * ------------------------------------------------------------------------------------------------
 */


uint8_t setBeaconCoordinates(int num, float x[][3])
{
      for (int i=0; i< num; i++)
      {
        for (int j=0; j<3; j++)
        {
          B[i][j]=x[i][j];
          beaconID[i] = i+1;
        }
      }
}
uint8_t setRangeOffset(int num, float o[])
{
  for(int i=0; i<num; i++)
    offset[i] = o[i];
}

void setNumberOfBeacons(int n)
{
  numBeacon = n;
}


void setFilterParameters(float minDis, float maxDis, float jumpThreshold, float residueThreshold, float observeNoise, float stateNoise, float moveSize, float h) 
{
  r = observeNoise;
  q = stateNoise;
  RESIDUETRESHOLD = residueThreshold;
  JUMPTHRESHOLD = jumpThreshold;
  MAXDIS = maxDis;
  MINDIS = minDis;
  MOVESIZE = moveSize; 
  arm_mat_init_f32(&z,OB_VEC_LEN,1,&(zm[0]));
  for (int i=0; i< MAX_NUM_BEACON; i++)
    DisBufferTop[i] = -1;
  height = h;
  
}

void setImuParameters(int imuon, int compassinterval, int imuinterval)
{
  imuOn = imuon;
  compassInterval = compassinterval;
  imuInterval = imuinterval;
}


#define initSpeed 0.1f
uint8_t initEKF(float initpos[]){
    arm_mat_init_f32(&R,OB_VEC_LEN,OB_VEC_LEN,&(Rm[0][0]));
    R.pData[0] = r*r;
    setMatToUnit(Qm,STATE_VEC_LEN,q*q);
    arm_mat_init_f32(&Q,STATE_VEC_LEN,STATE_VEC_LEN,&(Qm[0][0]));
    setMatToUnit(Pm,STATE_VEC_LEN,1);
    arm_mat_init_f32(&P,STATE_VEC_LEN,STATE_VEC_LEN,&(Pm[0][0]));
    arm_mat_init_f32(&X,STATE_VEC_LEN,1,&(Xm[0]));
    //arm_mat_init_f32(&z,OB_VEC_LEN,1,&(zm[0]));

    uint8_t i,k;
    for(i = 0;i<3;i++)
        X.pData[i] = initpos[i];
    for(i=3;i<6;i++)
        X.pData[i] = initSpeed;
    for(i=6;i<9;i++)
        X.pData[i] = 0;

    for(k= 0; k< WINDOW_SIZE; k++){
        estPosWindow[k][0] = initpos[0];
        estPosWindow[k][1] = initpos[1];
        estPosWindow[k][2] = initpos[2];
    }
    return 0;
}

uint8_t resetEKF(arm_matrix_instance_f32 *resetPos){
    uint8_t i,k;
    for(i = 0;i<3;i++)
        X.pData[i] = resetPos->pData[i];
    for(i=3;i<6;i++)
        X.pData[i] = 0;
    for(i=6;i<9;i++)
        X.pData[i] = 0;

    setMatToUnit(Pm,STATE_VEC_LEN,1);
    arm_mat_init_f32(&P,STATE_VEC_LEN,STATE_VEC_LEN,&(Pm[0][0]));
    
    for(k= 0; k< WINDOW_SIZE; k++){
        estPosWindow[k][0] = resetPos->pData[0];
        estPosWindow[k][1] = resetPos->pData[1];
        estPosWindow[k][2] = resetPos->pData[2];
    }
    return 0;
}
  

bool updateEKF(arm_matrix_instance_f32 *z, int i, float step){
  
    arm_matrix_instance_f32 x1,A,At,H,I,z1,P12,tH,tMat,tMat2,tMat3;   //声明 arm_matrix_instance_f32
    arm_matrix_instance_f32 tR,itR,tMat5,tMat6,tMat8, U, tMat4, tMat7, tMat9, temp;

    // the memory storage space for the matrix
    float32_t x1m[STATE_VEC_LEN],Am[STATE_VEC_LEN][STATE_VEC_LEN], Im[STATE_VEC_LEN][STATE_VEC_LEN], Atm[STATE_VEC_LEN][STATE_VEC_LEN];
    float32_t tMatm[STATE_VEC_LEN][STATE_VEC_LEN], tMat2m[STATE_VEC_LEN][STATE_VEC_LEN];
    float32_t Hm[OB_VEC_LEN][STATE_VEC_LEN], z1m[OB_VEC_LEN], P12m[STATE_VEC_LEN][OB_VEC_LEN], tHm[STATE_VEC_LEN][OB_VEC_LEN],tMat3m[OB_VEC_LEN][OB_VEC_LEN];
    float32_t tRm[OB_VEC_LEN][OB_VEC_LEN];
    float32_t itRm[OB_VEC_LEN][OB_VEC_LEN];
    float32_t tMat5m[OB_VEC_LEN];               //memory space
    float32_t tMat6m[STATE_VEC_LEN][OB_VEC_LEN];
    float32_t tMat8m[STATE_VEC_LEN][STATE_VEC_LEN];
    float32_t Um[STATE_VEC_LEN][OB_VEC_LEN], tMat4m[OB_VEC_LEN][OB_VEC_LEN], tMat7m[OB_VEC_LEN][STATE_VEC_LEN], tMat9m[OB_VEC_LEN][OB_VEC_LEN], tempm[OB_VEC_LEN][OB_VEC_LEN];

    // init a all zero matrix size:
    arm_mat_init_f32(&x1,STATE_VEC_LEN,1,&(x1m[0]));              //将memory space赋给声明的矩阵变量 arm_mat_init_f32
    arm_mat_init_f32(&A,STATE_VEC_LEN,STATE_VEC_LEN,&(Am[0][0]));
    arm_mat_init_f32(&I,STATE_VEC_LEN,STATE_VEC_LEN,&(Im[0][0]));
    arm_mat_init_f32(&At,STATE_VEC_LEN,STATE_VEC_LEN,&(Atm[0][0]));
    arm_mat_init_f32(&tMat,STATE_VEC_LEN,STATE_VEC_LEN,&(tMatm[0][0]));
    arm_mat_init_f32(&tMat2,STATE_VEC_LEN,STATE_VEC_LEN,&(tMat2m[0][0]));
    arm_mat_init_f32(&H,OB_VEC_LEN,STATE_VEC_LEN,&(Hm[0][0]));
    arm_mat_init_f32(&z1,OB_VEC_LEN,1,&(z1m[0]));
    arm_mat_init_f32(&P12,STATE_VEC_LEN,OB_VEC_LEN,&(P12m[0][0]));
    arm_mat_init_f32(&tH,STATE_VEC_LEN,OB_VEC_LEN,&(tHm[0][0]));
    arm_mat_init_f32(&tMat3,OB_VEC_LEN,OB_VEC_LEN,&(tMat3m[0][0]));
    arm_mat_init_f32(&tR,OB_VEC_LEN,OB_VEC_LEN,&(tRm[0][0]));
    arm_mat_init_f32(&itR,OB_VEC_LEN,OB_VEC_LEN,&(itRm[0][0]));
    arm_mat_init_f32(&tMat5,OB_VEC_LEN,1,&(tMat5m[0]));
    arm_mat_init_f32(&tMat6,STATE_VEC_LEN,OB_VEC_LEN,&(tMat6m[0][0]));
    arm_mat_init_f32(&tMat8,STATE_VEC_LEN,STATE_VEC_LEN,&(tMat8m[0][0]));
    arm_mat_init_f32(&U, STATE_VEC_LEN, OB_VEC_LEN, &(Um[0][0]));
    arm_mat_init_f32(&tMat4,OB_VEC_LEN,OB_VEC_LEN,&(tMat4m[0][0]));
    arm_mat_init_f32(&tMat7,OB_VEC_LEN,STATE_VEC_LEN,&(tMat7m[0][0]));
    arm_mat_init_f32(&tMat9,OB_VEC_LEN,OB_VEC_LEN,&(tMat9m[0][0]));
    arm_mat_init_f32(&temp,OB_VEC_LEN,OB_VEC_LEN,&(tempm[0][0]));


    buildA(1,&A);  //6*6
    arm_mat_mult_f32(&A,&X,&x1);  //6*1    x1=AX
    arm_mat_trans_f32(&A,&At);  //6*6
    arm_mat_mult_f32(&A,&P,&tMat);  //6*6    
    arm_mat_mult_f32(&tMat,&At,&tMat2);           //6*6  tMat2=A*P*~A
    arm_mat_add_f32(&tMat2,&Q,&P);                //6*6  P = A*P*~A+Q;



    buildH(&H,&x1,i);                    //1*6  
    dhmeasVec(&z1,x1,i);                //1*1

    arm_mat_trans_f32(&H,&tH);         //6*1
    arm_mat_mult_f32(&P,&tH,&P12);     //6*1           P12 = P*~H;
    arm_mat_mult_f32(&H,&P12,&tMat3);  //1*1           tMat3 = H*P*~H;
    arm_mat_add_f32(&tMat3,&R,&tR);    //1*1           tR = H*P*~H+R;

/****** cholesky的实现方式 */
    arm_matrix_instance_f32 tRs;
    float32_t tRsm[OB_VEC_LEN][OB_VEC_LEN];
    arm_mat_init_f32(&tRs,OB_VEC_LEN,OB_VEC_LEN,&(tRsm[0][0]));

    cholesky(&tR,&tRs);                 //  1*1     RR=chold(H*P12+R);
    arm_mat_trans_f32(&tRs,&temp);      //1*1   temp = RR' 
    arm_mat_inverse_f32(&tRs,&itR);     // 1*1  itR = 1/RR

    arm_mat_mult_f32(&P12,&itR,&U);     //6*1 U=P12/RR
    arm_mat_inverse_f32(&temp,&tMat4);   //1*1  tMat4=inv(~RR);
 
    arm_mat_sub_f32(z,&z1,&tMat5);       //1*1   tMat5=z-z1;
    arm_mat_mult_f32(&tMat4,&tMat5,&tMat9);  //1*1  tMat9= inv(~RR)*(z-z1)
    
    arm_mat_mult_f32(&U,&tMat9,&tMat6);  //6*1  tMat6=U*inv(~tRs);  
    arm_mat_add_f32(&x1,&tMat6,&X);      //6*1  X=x1+tMat9

    arm_mat_trans_f32(&U,&tMat7);       // 1*6, tMat7=~U;
    arm_mat_mult_f32(&U,&tMat7,&tMat8); //6*6   tMat8=U*~U;
    arm_mat_sub_f32(&P,&tMat8,&P);      //6*6 P=P-U*~U;
    
    return 1;
     
}

bool distBufferFilter(float *dis, float ttime, float32_t Trh, uint8_t Msize, int id)
{
    if(DisBufferTop[id] <= -1){
        DisBufferTop[id] =  DisBufferTop[id] + 1;
        distBufferWindow[DisBufferTop[id]][id][0] = *dis;
        distBufferWindow[DisBufferTop[id]][id][1] = ttime;
        distBufferWindow[DisBufferTop[id]][id][2] = distBufferWindow[DisBufferTop[id]][id][2] + 1; 

    }
    else if(DisBufferTop[id] >= 0)
    {
        //*dis = (*dis + distBufferWindow[DisBufferTop[id]][id][0]) /2;
        float meanvalue = 0.0;
        int temp = (int)distBufferWindow[DisBufferTop[id]][id][2];
        int mintemp = (Msize < temp) ? Msize: temp;
        int j;
        for (int i=0; i< mintemp; i++)
        {
            j= (DisBufferTop[id]-i+WINDOW_SIZE) % WINDOW_SIZE;
            meanvalue = meanvalue + distBufferWindow[j][id][0];
        }
        meanvalue = meanvalue/mintemp;
        
        if(((int)*dis - (int)meanvalue) > (int)Trh)
            *dis = meanvalue + Trh;
        else if(((int)meanvalue - (int)*dis)>(int)Trh)
            *dis = meanvalue - Trh;

        DisBufferTop[id] = (DisBufferTop[id] +1) % WINDOW_SIZE;
        distBufferWindow[DisBufferTop[id]][id][0] = *dis;
        distBufferWindow[DisBufferTop[id]][id][1] = ttime;
        float temp1 = distBufferWindow[DisBufferTop[id]][id][2] + 1; 
        distBufferWindow[DisBufferTop[id]][id][2] = (temp1 < WINDOW_SIZE) ? temp1: WINDOW_SIZE; 
    }

    return 1;

}




bool CheckSingularGenerateVirtualDis()
{

    while(confidence < 4)
    {
        int idx = 0;
        int newdisInx = 0;
        while(newdisInx < 1){
            idx = rand() % numBeacon;
            if(isMember(idx, beaconWindow) == 0)
                newdisInx = newdisInx +1;
        }
        float predictPos[3] ={0.0f,0.0f,0.0f};
        for(int k=0; k<3; k++)
        {
            predictPos[k] = estPosWindow[0][k] + 0.5*(estPosWindow[0][k] - estPosWindow[0][k]);
        }
        float32_t sdis = calDisFromBeacon(idx, predictPos);

        updateWindows(idx, sdis, 3.0f);

    }
    return 0;

}

int isMember(int a, int A[])
{
    for(int i=0; i<WINDOW_SIZE; i++)
        if(a == A[i])
            return 1;

    return 0;
}

int removeFromWindows(int k){
    for(int j=k; j<WINDOW_SIZE-1; j++)
    {
        beaconWindow[j] = beaconWindow[j+1];
        distWindow[j] = distWindow[j+1];
        tWindow[j] = tWindow[j+1];
    }

    windex = windex -1; 
    return 0;
}

int updateWindows(int b, float d, float t){

    uint8_t j;
    if(windex < WINDOW_SIZE)
        windex = windex +1;
    else
        windex = WINDOW_SIZE;


    for(j=WINDOW_SIZE-1; j>0; j--)
    {
        beaconWindow[j] = beaconWindow[j-1];
        distWindow[j] = distWindow[j-1];
        tWindow[j] = tWindow[j-1];
    }
    beaconWindow[0] = b;
    distWindow[0] = d;
    tWindow[0] = 1.0;


    // update confidence
/*
    k = WINDOW_SIZE;
    for(i=0;i<WINDOW_SIZE-1;i++)
        for(j=i+1;j<WINDOW_SIZE;j++)
        {
            if(beaconWindow[i]==beaconWindow[j])
            {
                k--;
                break;
            }
        }
    confidence = k;

    for(i=0; i<WINDOW_SIZE; i++)
        if(beaconWindow[i] == -1)
        {
            confidence = confidence-1;
            break;
        }
*/
    return 1;
}

int updatePos(arm_matrix_instance_f32 *x)
{
    for(int j=WINDOW_SIZE-1; j>0; j--)
    {
        for(int k=0; k<3; k++)
        {
            estPosWindow[j][k] =estPosWindow[j-1][k];
            vWindow[j][k] = vWindow[j-1][k];
        }
    }
    estPosWindow[0][0] = x->pData[0];
    estPosWindow[0][1] = x->pData[1];
    estPosWindow[0][2] = x->pData[2];

    for (int k=0; k<3; k++) {
        vWindow[0][k] = 0.1*(estPosWindow[0][k]-estPosWindow[1][k]);
    }
    return 0;
}


float calDisFromBeacon(int i, float y[])
{
    float dis = 0.0;
#ifdef DEBUG_LSQ
    printf("i= %d, targetpos=%f %f %f\n\r", i, y[0], y[1], y[2]);
#endif

    dis = sqrt((B[i][0]-y[0])*(B[i][0]-y[0]) + (B[i][1]-y[1])*(B[i][1]-y[1]) + (B[i][2]-y[2])*(B[i][2]-y[2]));
#ifdef DEBUG_LSQ
    printf("pos:(%f,%f,%f), dis= %f\n\r", B[i][0],B[i][1],B[i][2],dis);
#endif
    return dis;
}


int updateTargetPos(float *targetPos, uint8_t t){


    *targetPos =(targetPos[0]+10)/10.0;
    *(targetPos+1) =(targetPos[1]+10)/10.0;
    *(targetPos+2) = 100.0f;

    return 0;
}


//dMatrix NonLinearLSQ(int beaconWindow[], int eventTimeWindow[], float** vWindow, float distWindow[])
void NonLinearLSQ(float myx[], float32_t res[]){

    //printf("b= %d t= %f v= %f d= %f\n\r",beaconWindow[0], tWindow[0], vWindow[0][0], distWindow[0]);
    //printf("%f %f %f \n\r",distWindow[0],distWindow[1],distWindow[2]);
    //printf("%f %f %f \n \r", B[beaconWindow[0]-1][0], B[beaconWindow[0]-1][1], B[beaconWindow[0]-1][2]);
    

    //printf("%d\t%d\t%d\t%d\t%d\t%d\t%d\t\n\r", beaconWindow[0], beaconWindow[1], beaconWindow[2],beaconWindow[3],beaconWindow[4],beaconWindow[5],beaconWindow[6]);
    //printf("%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t\n\r", distWindow[0],distWindow[1],distWindow[2],distWindow[3],distWindow[4],distWindow[5],distWindow[6]);
    //printf("%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t\n\r", B[beaconWindow[0]-1][0], B[beaconWindow[1]-1][0], B[beaconWindow[2]-1][0],B[beaconWindow[3]-1][0],B[beaconWindow[4]-1][0],B[beaconWindow[5]-1][0], B[beaconWindow[6]-1][0]);
    //printf("%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t\n\r", B[beaconWindow[0]-1][1], B[beaconWindow[1]-1][1], B[beaconWindow[2]-1][1],B[beaconWindow[3]-1][1],B[beaconWindow[4]-1][1],B[beaconWindow[5]-1][1], B[beaconWindow[6]-1][1]);
    //printf("%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t\n\r", B[beaconWindow[0]-1][2], B[beaconWindow[1]-1][2], B[beaconWindow[2]-1][2],B[beaconWindow[3]-1][2],B[beaconWindow[4]-1][2],B[beaconWindow[5]-1][2], B[beaconWindow[6]-1][2]);

    int n = WINDOW_SIZE;
    int m = 2;

    arm_matrix_instance_f32 b, s;
    float32_t bm[WINDOW_SIZE][2];
    float32_t sm[WINDOW_SIZE][2];

    arm_mat_init_f32(&b, WINDOW_SIZE,2, &(bm[0][0]));
    arm_mat_init_f32(&s, WINDOW_SIZE,2, &(sm[0][0]));

    uint8_t i,j,t;

    for(i=0; i<n; i++){
        t = beaconWindow[i];
        for(j=0; j<m; j++){
            bm[i][j]=B[t][j];
        }

//        arm_matrix_instance_f32 temp;
//       float32_t tempm[2]={0,0};
//       arm_mat_init_f32(&temp, 2,1, tempm);
        float temp[2] = {0.0,0.0};
        for(int j=1; j<i; j++)
        {
            for(int k=0; k<m; k++)
                temp[k] = temp[k] + tWindow[j-1]*vWindow[j-1][k];
        }
        for (int k=0; k<m; k++)
            sm[i][k] = bm[i][k] + temp[k];
    }

    int sizeA = 21;
    arm_matrix_instance_f32 A, C;
    float32_t Am[21][2];
    float32_t Cm[21];
    arm_mat_init_f32(&A, sizeA, m, &(Am[0][0]));
    arm_mat_init_f32(&C, sizeA, 1, &(Cm[0]));


    int index = 0;
    uint8_t k;
    for( i=0; i<n; i++){
        for( j=i+1; j<n; j++)
        {
            for( k=0; k<m; k++){
                Am[index][k] = sm[j][k] - sm[i][k] + sm[j][k] - sm[i][k];
            }
            Cm[index] = distWindow[i]*distWindow[i] -distWindow[j]*distWindow[j] -sm[i][0]*sm[i][0] -sm[i][1]*sm[i][1] + sm[j][0]*sm[j][0] + sm[j][1]*sm[j][1];
            index = index + 1;
        }
    }

    arm_matrix_instance_f32 x1, At, temp, tempi, temp2, tz;
    float32_t x1m[2][1];
    float32_t Atm[2][21];
    float32_t tempm[2][2];
    float32_t tempim[2][2];
    float32_t temp2m[2][21];
    float32_t tzm[WINDOW_SIZE][1];


    arm_mat_init_f32(&x1, m, 1, &(x1m[0][0]));
    arm_mat_init_f32(&At, m, sizeA, &(Atm[0][0]));
    arm_mat_init_f32(&temp, m, m, &(tempm[0][0]));
    arm_mat_init_f32(&tempi, m, m, &(tempim[0][0]));
    arm_mat_init_f32(&temp2, m, sizeA, &(temp2m[0][0]));
    arm_mat_init_f32(&tz, n, 1, &(tzm[0][0]));

    //dMatrix x1(m, 1);
    arm_mat_trans_f32(&A, &At);
    arm_mat_mult_f32(&At, &A, &temp);
    //dMatrix temp = ~A * A;

    arm_mat_inverse_f32(&temp, &tempi);
    //temp.inv();
    arm_mat_mult_f32(&tempi, &At, &temp2);
    //dMatrix temp2 = tempi * ~A;
    arm_mat_mult_f32(&temp2, &C, &x1);
    //x1 = temp2 * C;

    //dMatrix tz(n,1);

    for(int i=0; i<n; i++)
    {
        float32_t temp33;
        float32_t temp3 = distWindow[i]*distWindow[i]-(x1m[0][0]-sm[i][0])*(x1m[0][0]-sm[i][0])-(x1m[1][0]-sm[i][1])*(x1m[1][0]-sm[i][1]);
        int t=beaconWindow[i];

        if(temp3 < 0)
            tzm[i][0] = B[t][2];
        else
        {
            arm_sqrt_f32(temp3, &temp33);
            tzm[i][0] = B[t][2]-temp33;
        }


    }

    ///// Calculate Z coordinate 
    float temp4=0;
    for(int i=0; i<n; i++)
    {
        temp4= temp4 + tzm[i][0];
    }
    temp4 = temp4/n;

    myx[0]  = x1m[0][0];
    myx[1]  = x1m[1][0];
    myx[2]  = temp4;

    //// Calculate Residue
    
    calResidue(myx, res);
//    float32_t temp5 = 0.0;
//    float32_t temp6 = 0.0;
//    for (int i=0; i<n ;i++)
//    {
//        int t=beaconWindow[i];
//        temp5=(B[t][0]-x1m[0][0])*(B[t][0]-x1m[0][0]) + (B[t][1]-x1m[1][0])*(B[t][1]-x1m[1][0]) +(B[t][2]-temp4)*(B[t][2]-temp4); 
//        arm_sqrt_f32(temp5, &temp6);
//        if(distWindow[i]-temp6 > 0)
//          res[i] = distWindow[i]-temp6;
//        else
//          res[i] = temp6 - distWindow[i];
////        printf("temp5 =%f, temp6=%f, distWindow[i]=%f, r[i]=%f\n\r", temp5, temp6, distWindow[i], r[i]);
//    }


}

float32_t calResidue(float pos[], float32_t res[])
{
  float32_t dissquare; float32_t sumResidue = 0;
    for (int i=0; i<1 ;i++)
    {
        int t=beaconWindow[i];
        dissquare=(B[t][0]-pos[0])*(B[t][0]-pos[0]) + (B[t][1]-pos[1])*(B[t][1]-pos[1]) +(B[t][2]-pos[2])*(B[t][2]-pos[2]); 
        float32_t rDis = sqrt(dissquare);
        if(distWindow[i]-rDis > 0)
          res[i] = distWindow[i]-rDis;
        else
          res[i] = rDis - distWindow[i];
        
        sumResidue = sumResidue + res[i];
    }
    return sumResidue;
}

void LSQwithNLOS(float myx[], float32_t r[], float rT, uint32_t *v, uint8_t *p){
  

    uint32_t n = WINDOW_SIZE;
    float sumr =0;
    float32_t rmean;
    uint32_t maxPos;
    float32_t max;
    *v = -1;

    NonLinearLSQ(myx, r);
    for (int i=0; i< n; i++)
    {
         sumr = sumr + r[i];
    } 
    if (sumr <= rT){
      *p = 1;
    }
    else{

        float32_t absr[WINDOW_SIZE];
        float32_t temp[WINDOW_SIZE];

        arm_mean_f32(r, n, &rmean);
        for (int i=0; i<n; i++)
            temp[i] = r[i]-rmean;
        arm_abs_f32(temp, absr, n);
        arm_max_f32(absr, n, &max, &maxPos);
        *v = maxPos;
        *p = 0;
    }
}



uint8_t cholesky(arm_matrix_instance_f32 *A, arm_matrix_instance_f32 *L) {
    if (A == NULL||A->numRows != A->numCols||A->numRows != L->numRows){
        //wrong input matrix
        return 0;
    }
    uint16_t n = A->numRows;
    for (uint16_t i = 0; i < n; i++)
        for (uint16_t j = 0; j < (i+1); j++) {
            float s = 0;
            for (uint16_t k = 0; k < j; k++)
                s += L->pData[i * n + k] * L->pData[j * n + k];
            L->pData[i * n + j] = (i == j) ?
                sqrt(A->pData[i * n + i] - s) :
                (1.0 / L->pData[j * n + j] * (A->pData[i * n + j] - s));
        }
    return 1;
}

void buildA(float32_t dt,arm_matrix_instance_f32* cA)
{

    //dMatrix cA(STATE_VEC_LEN,STATE_VEC_LEN);
    uint8_t i,j;
    for(i=0; i<STATE_VEC_LEN; i++)
    {
        for(j=0; j<STATE_VEC_LEN; j++)
        {
            cA->pData[i*cA->numCols+j] = 0;
        }
        cA->pData[i*cA->numCols+i] = 1;
    }
    cA->pData[0*cA->numCols+3] = dt;
    cA->pData[1*cA->numCols+4] = dt;
    cA->pData[2*cA->numCols+5] = dt;
}

void dfStateVec(arm_matrix_instance_f32 x, float step,arm_matrix_instance_f32* x1){
    x1->pData[0]=x.pData[0]+step*x.pData[3];
    x1->pData[1]=x.pData[1]+step*x.pData[4];
    x1->pData[2]=x.pData[2]+step*x.pData[5];
    x1->pData[3]=x.pData[3];
    x1->pData[4]=x.pData[4];
    x1->pData[5]=x.pData[5];
}

void buildH(arm_matrix_instance_f32 *cH,arm_matrix_instance_f32 *x, int i)
{
    float dis = sqrt((x->pData[0]-B[i][0])*(x->pData[0]-B[i][0])+(x->pData[1]-B[i][1])*(x->pData[1]-B[i][1])+(x->pData[2]-B[i][2])*(x->pData[2]-B[i][2]));
    cH->pData[0] = (x->pData[0]-B[i][0])/dis;
    cH->pData[1] = (x->pData[1]-B[i][1])/dis;
    cH->pData[2] = (x->pData[2]-B[i][2])/dis;
    cH->pData[3] = 0.0;
    cH->pData[4] = 0.0;
    cH->pData[5] = 0.0;
}

void dhmeasVec(arm_matrix_instance_f32 *dz,arm_matrix_instance_f32 x, int i){
    float dis = 0;
    for (int k=0; k<3; k++)
    {
        dis += (x.pData[k]-B[i][k])*(x.pData[k]-B[i][k]);
    }
    dis = sqrt(dis);
    dz->pData[0]= dis;
}

void updateByDis(float dist, uint8_t id){
                float loc3[3];
                float calDis;
                float disDiff;
                ID = id;
                distCM = dist + offset[ID-1];
                if(distCM >MINDIS && distCM <MAXDIS)
                {

		   // distBufferFilter(&distCM, ttime, JUMPTHRESHOLD, MOVESIZE, ID-1);
                    z.pData[0]=distCM;
                    updateWindows(ID-1,distCM, step);
                    if(windex == WINDOW_SIZE && EKFInit ==0){       
                      
                        NonLinearLSQ(xl, residue);
                      
                        initEKF(xl);  //initialize EKF
                        initFusion(xl, att_euler);
                        EKFInit = 1;
                        FusionInitialized = 1;
                    }
                    else if(windex == WINDOW_SIZE)
                    {
                      
                      
                        loc3[0] = X.pData[0];
                        loc3[1] = X.pData[1];
                        loc3[2] = X.pData[2];
                        calDis = calDisFromBeacon (ID-1, loc3);
                        disDiff = calDis - distCM;
                     
                        if((int)disDiff > 50 && sumDisResidue < 20)
                        {
                          distCM = calDis -10;
                          printf("too small \n");
                          return;
                        }
                          else if((int)disDiff < -50 && sumDisResidue < 20)
                         {
                          distCM = calDis +10;
                          printf("too large \n");
                          return;
                         }

 /*                     
                        LSQwithNLOS(xl, residue, RESIDUETRESHOLD, &v, &p); 
                        if(p == 0)
                        {
                            removeFromWindows(v);
                            failCount = failCount + 1;
                            if(failCount == 5)
                            {
                                NonLinearLSQ(xl, residue);
                                //resetEKF(xl);  //reset EKF
                                failCount = 0;
                            }
    
                        }
                        else 
                        {
  */           
                          updateEKF(&z, ID-1, step);
                            //printf("1 %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2d\n",X.pData[0],X.pData[1],X.pData[2],X.pData[3],X.pData[4],X.pData[5],att_euler[0],att_euler[1],att_euler[2],timeStamp);
                          constructLoc(loc3, loc, X, att_euler);
                          updateByUsLoc(loc);
                          sumDisResidue = calResidue(loc3, residue);
                          printf("%8.2f \n", sumDisResidue);
                  
                        }
                    
                
          }
}
void updatePosByIMU(uint8_t IMUReady, uint8_t MegReady){
        if(IMUReady==imuOn){
            IMUReady = 0;
            int16_t rawData[9];
            int16_t temp;
            MPU6050_GetRawAccelGyro(rawData,&temp);
           /* if(MegReady>=compassInterval)
            {
                MegReady=0;
                //STM_EVAL_LEDToggle(LED2);
                AK8975_GetRawCompass(rawData+6);
                //TODO
                attitudeSolution(rawData,1);
                //printf("%d,%d,%d,%d\n",rawData[6],rawData[7],rawData[8],saaaaaa);
            }
            else
         */    
            attitudeSolution(rawData,0);
             imuCount++;
            if(imuCount==imuInterval)
            {
               imuCount = 0;
               constructAccGyro(accgyro, acc, pal);
               if(FusionInitialized==1)
                updateByImu(accgyro);
                //printf("%f %f %f %f %f %f\n",accgyro[0], accgyro[1], accgyro[2], accgyro[3],accgyro[4],accgyro[5]);

            }
        }
}

float32_t  getLastPosition(float xyz[]){
   
  if(EKFInit == 0)
    return -1.0; 
  else{
     for(int i=0; i<6; i++)
        xyz[i] = XX.pData[i];
     for(int i=6; i<9; i++)
        xyz[i] = att_euler[i-6];
     
     return sumDisResidue;
  }
}
