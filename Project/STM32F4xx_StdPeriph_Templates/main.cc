/**
 ******************************************************************************
 * @file    OOPS/main.c
 * @author  Yongcai Wang, Lei Song
 * @version V1.2.0

 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm324xg_eval.h"
#include "stm32f4xx_conf.h"
#include <stdio.h>
#include "stm32f4xx_it.h"
#include "MPU6050.h"
#include "yaEkf6.hh"
#include <stdlib.h>
#include "main.hh"
#include "Msg.hh"
#include "arm_math.h"
#include "math.h"
#include "attitudeSolution.h"
#include "fusion.hh"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define __IO volatile
#define SZ_BUF_LEN 100
#define SMSG_BUF_LEN 50
#define UART_BUF_SIZE 100
#define MAX_MSG_CNT 10
#define RANGE_FILTER_THRESHOLD 40


#if defined (USE_STM324xG_EVAL)
#define MESSAGE1   "     STM32F40xx     "
#define MESSAGE2   " Device running on  "
#define MESSAGE3   "   STM324xG-EVAL    "

#else /* USE_STM324x7I_EVAL */
#define MESSAGE1   "     STM32F427x     "
#define MESSAGE2   " Device running on  "
#define MESSAGE3   "   STM324x7I-EVAL   "
#endif


/*public valurable-------------------------------------------------------------*/
float beaconCoordinFlash[MAX_NUM_BEACON*4];
uint8_t numBeacon;
uint8_t numBeaconToWrite;

float dist;
float offset[];
float beaconCoord[];
volatile uint8_t IMUReady,MegReady;
extern uint8_t SpiOutBuf[];
void SPI_Printf(uint8_t* array,int len);

extern volatile uint32_t timeStamp;
//#ifdef _SIMULATOR_

    int16_t abias[3] ={550, 173, -5320};
    int16_t wbias[3] ={244, 149, -254};
    int16_t hbias[3] ={40, -30, -15};

    float ab[9][3]=
                {
                    {0,172,245},
                    {0,0,245},
                    {78,138,245},
                    {77,60,245},
                    {154,172,245},
                    {144,0,245},
                    {220,183,245},
                    {196,27,245},
                    {280,105,245}
                };

    float o[9]={-0,-0,-0,-0,-0,-0,-0,-0,-0};
    //float o[9]={-5,-8,-10,-13,-16,-21,-24,-31,-40};
    float targetPos[3] = {30.0, 0.0, 0.0};

    int simID = 0;
//#endif

int s=0;

/* Private macro -------------------------------------------------------------*/
//CircularBuffer rangeBuffer(DATA_BUFFER_SIZE,range_data);

/* Private variables ---------------------------------------------------------*/
uint8_t FlagRxUart1;
uint8_t FlagRxUart2;
volatile uint8_t RxBufferOfUart1[BUFFER_SIZE];

volatile uint8_t id,Seq,pktReady = 0,wrongPkt = 0;
volatile uint8_t flushToFlash,wrongCoordPkt = 0,CoordPktReady =0,offsetPktReady =0 ,ADCReady =0;
int32_t lasttime=0;

static int16_t accGyroOffset[9];

sRecvStage curStage,uart1Stage;
/* Private variables ---------------------------------------------------------*/
RCC_ClocksTypeDef    RCC_Clocks;
TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
TIM_OCInitTypeDef  TIM_OCInitStructure;
//volatile uint16_t CCR1_Val = 40961;
/* Private function prototypes -----------------------------------------------*/

__IO uint16_t rawUSValue[ADC_VALUE_LEN];
#define MAX_SPI_BUF_LEN 100
__IO uint8_t spiBuf[2][MAX_SPI_BUF_LEN];
__IO uint8_t *spiInBuf;
__IO uint8_t *spiOutBuf;
__IO uint16_t spiInBufLen,spiOutBufLen;


__IO uint8_t USARTBuf[2][MAX_USART_BUF_LEN];
__IO uint8_t *USARTInBuf;
__IO uint8_t *USARTOutBuf;
__IO uint8_t USARTInBufLen,USARTOutBufLen;
__IO uint8_t USARTpktReady;
__IO uint8_t badSPI;
__IO floatInt converter;




/* Private functions ---------------------------------------------------------*/
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#endif

#ifdef __SIMULATOR__
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#else
//#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

/* Variables defined for IMU　and fusion ------------------------------------------*/
uint32_t flashStartSectorAddr = 0;
void AlgoInit(void);
void boardInit(void);


int main(void){
    float32_t cal_residue=0; 
    float xyz[9];
    uint32_t loctime;
    uint8_t beaconID[MAX_NUM_BEACON];
    AlgoInit();
    boardInit();
    //FlashReadConfig(&numBeacon,beaconID,beaconCoord,offset);
    
    setBeaconCoordinates(9, ab);
    setNumberOfBeacons(9);
    setRangeOffset(9, o);
    setImuParameters(1, 4, 1);    
    setFilterParameters(20, 500, 80, 40, 5, 1, 1, 100);
    setImuBiasOffset(abias, wbias, hbias);
    
    spiInBuf =&(spiBuf[0][0]);
    spiOutBuf =&(spiBuf[1][0]);
    USARTInBuf = &(USARTBuf[0][0]);
    USARTOutBuf = &(USARTBuf[1][0]);
#ifdef ENABLE_ADC
    usADCInit();
#endif

#ifdef DEBUG_FLASH
    printf("numBeaconInFlash = %u\n\r",numBeacon);
    for(int k=0;k<numBeacon;k++){
	printf("%u:%.1f %.1f %.1f %.1f\n\r",beaconID[k],beaconCoord[k*3],beaconCoord[k*3+1],beaconCoord[k*3+2],offset[k]);
    }
#endif
    volatile unsigned char szBuffer[SZ_BUF_LEN];
    volatile uint8_t sMsgBuffer[SMSG_BUF_LEN];
    volatile uint16_t sBufferSize = 0;
    volatile uint8_t hasNew = 0;
    volatile uint8_t coordBuffer[SZ_BUF_LEN];
    volatile uint16_t coordBufLen = 0;

    pktReady = 0;
    MsgBuf thisMsgBuf;
    PointMsgBuf thisPointMsgBuf;
    badSPI = 0;
    /* Infinite loop */
    attitudeInit();


    /* Infinite loop */
    while (1){
#ifdef ONLY_RAW_OUTPUT  
	int16_t rawData[9];
	int16_t temp;
	MPU6050_GetRawAccelGyro(rawData,&temp);
	int i;
	float outData[6];
	for(i = 0;i<9;i++){
	    rawData[i] += accGyroOffset[i];
	}
	for(i = 0;i<3;i++){
	    outData[i] = rawData[i]/32768.0f*2;
	}
	for(i = 3;i<6;i++){
	    outData[i] = rawData[i]/32768.0f*250;
	}
	printf("%03d 0 %8d %8d %8d %8d %8d %8d %8d %8d %8d\n",
		0xff&timeStamp,(rawData[0]),(rawData[1]),(rawData[2]),
		(rawData[3]),(rawData[4]),(rawData[5]),
		0,0,0);
#endif



	//update Position and Attitude by IMU readings
	//EXTI->SWIER |= EXTI_Line11;

	updatePosByIMU(IMUReady, MegReady);
        cal_residue = getLastPosition(xyz); 
#ifdef ASCII_RESULT
	printf("2 %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f  %d \n",
		XX.pData[0],XX.pData[1],XX.pData[2],
		XX.pData[3],XX.pData[4],XX.pData[5],
		XX.pData[6],XX.pData[7],XX.pData[8], 
                cal_residue, timeStamp);
#endif

	if(badSPI){
	    badSPI = 0;
	    SPI_Cmd(EVAL_SPI,DISABLE);
	    SPI_In_Config();
	    SPI_Cmd(EVAL_SPI,ENABLE);
	    SPI_I2S_ITConfig(EVAL_SPI,SPI_I2S_IT_RXNE,ENABLE);
	}

	if(ADCReady){
	    uint32_t USSum = 0;
	    uint16_t i;
	    for(i= 0;i<ADC_VALUE_LEN;i++){
		USSum += rawUSValue[i];
	    }
	    USSum/= i;
	    if(USSum >3000){
		//if capture us intensity then toggle the led;
		//TODO replace the led toggle with sync algorthm
		//TODO fifo quee in the usar
		//STM_EVAL_LEDToggle(LED2);
	    }
	}
#ifdef _SIMULATOR_
        if(1){
#else
	if(pktReady){
	    pktReady = 0;
	    uint16_t i,j;
	    j=0;
	    for(i=0;i<spiOutBufLen-1;i++){
		if(spiOutBuf[i]==0x7E)
		    continue;
		else if(spiOutBuf[i]==0x7D&&spiOutBuf[i+1]==0x5D){
		    thisMsgBuf.buf[j++] = 0x7D;
		    i++;
		}else if(spiOutBuf[i]==0x7D&&spiOutBuf[i+1]==0x5E){
		    thisMsgBuf.buf[j++] = 0x7E;
		    i++;
		}else{
		    thisMsgBuf.buf[j++] = spiOutBuf[i];
		}
	    }
	    if(j!=sizeof(Msg_t)){
		continue;
	    }
	    dist = (thisMsgBuf.msg.risingEdge[0]*34.6/250);
	    id = thisMsgBuf.msg.ID;
            
#endif
#ifdef _SIMULATOR_
            targetPos[0] = 200*sin((float)timeStamp/1000.0);
            targetPos[1] = 200*cos((float)timeStamp/1000.0);
            if(timeStamp - lasttime >50)
            {
              simID = (simID +1) %numBeacon;              // 1-9;
              if(simID == 0)
                simID = numBeacon;
              if(rand()%100 <10 )
              {
                //printf("%d\n", rand());
                dist = calDisFromBeacon(simID-1, targetPos) + (float)(rand()%200)-100.0 ;
              }
              else
                 dist = calDisFromBeacon(simID-1, targetPos) + (float)(rand()%20)-10.0 ;
              printf("Pos:x=%.2f\ty=%.2f\tz=%.2f\t\n\r", targetPos[0], targetPos[1], targetPos[2]);
              id = simID;                           //1-9
              lasttime = timeStamp;
           
           
#endif
           
            updateByDis(dist, id);
            cal_residue = getLastPosition(xyz);
		
#ifdef ASCII_RESULT       
		//printf("1 %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8d\n",XX.pData[0],XX.pData[1],XX.pData[2],XX.pData[3],XX.pData[4],XX.pData[5],att_euler[0],att_euler[1],att_euler[2], cal_residue, timeStamp);
		//sprintf(SpiOutBuf,"1 %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f\n",XX.pData[0],XX.pData[1],XX.pData[2],XX.pData[3],XX.pData[4],XX.pData[5],att_euler[0],att_euler[1],att_euler[2]);
             printf("1 %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %d\n",XX.pData[0],XX.pData[1],XX.pData[2],XX.pData[3],XX.pData[4],XX.pData[5],XX.pData[6],XX.pData[7],XX.pData[8], timeStamp);
	     sprintf(SpiOutBuf,"1 %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f\n",XX.pData[0],XX.pData[1],XX.pData[2],XX.pData[3],XX.pData[4],XX.pData[5],att_euler[0],att_euler[1],att_euler[2]);
            SPI_Printf(SpiOutBuf,83);
#endif
            
#ifdef _SIMULATOR_         
             }
#endif 

	    
	}
	if(USARTpktReady){
	    USARTpktReady = 0;
	    uint8_t i,j=0;
	    for(i=0;i<USARTInBufLen-1;i++){
		if(USARTInBuf[i]==0x7E)
		    continue;
		else if(USARTInBuf[i]==0x7D && USARTInBuf[i+1]==0x5D){
		    thisPointMsgBuf.buf[j++] = 0x7D;
		    i++;
		}else if(USARTInBuf[i]==0x7D && USARTInBuf[i+1]==0x5E){
		    thisPointMsgBuf.buf[j++] = 0x7E;
		    i++;
		}else{
		    thisPointMsgBuf.buf[j++] = USARTInBuf[i];
		}
	    }
	    if(j!=sizeof(Point_msg_t)){
#ifdef DEBUG_ERROR
		printf("wrong usart pkt %u %u\n\r",j,sizeof(Point_msg_t));
#endif
		continue;
	    }

	    if(thisPointMsgBuf.msg.ID==254){
		accGyroOffset[6] = thisPointMsgBuf.msg.x;
		accGyroOffset[7] = thisPointMsgBuf.msg.y;
		accGyroOffset[8] = thisPointMsgBuf.msg.z;
	    }else if (thisPointMsgBuf.msg.ID==253){
		accGyroOffset[3] = thisPointMsgBuf.msg.x;
		accGyroOffset[4] = thisPointMsgBuf.msg.y;
		accGyroOffset[5] = thisPointMsgBuf.msg.z;
	    }else if (thisPointMsgBuf.msg.ID==252){
		accGyroOffset[0] = thisPointMsgBuf.msg.x;
		accGyroOffset[1] = thisPointMsgBuf.msg.y;
		accGyroOffset[2] = thisPointMsgBuf.msg.z;
	    }else if(thisPointMsgBuf.msg.ID!=255){
		beaconID[numBeaconToWrite] = thisPointMsgBuf.msg.ID;
		offset[numBeaconToWrite] = thisPointMsgBuf.msg.offset;
		uint8_t ptr = numBeaconToWrite*3;
		beaconCoord[ptr++] = thisPointMsgBuf.msg.x;
		beaconCoord[ptr++] = thisPointMsgBuf.msg.y;
		beaconCoord[ptr++] = thisPointMsgBuf.msg.z;
		numBeaconToWrite++;
	    }
	    else if(thisPointMsgBuf.msg.ID==255 && numBeaconToWrite>0){
		uint8_t i,j;
		for(i=0;i<numBeaconToWrite;i++){
#ifdef POS_UPDATE
		    printf("%u:%.1f %.1f %.1f %.1f\n\r",beaconID[i],beaconCoord[i*3],beaconCoord[i*3+1],beaconCoord[i*3+2],offset[i]);
#endif
		}
		FLASH_Unlock();
		FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR|FLASH_FLAG_PGSERR);
		if(FLASH_EraseSector(FLASH_Sector_4,VoltageRange_3)!=FLASH_COMPLETE){
		    while(1){}
		}
		uint32_t writeAddr = ADDR_FLASH_SECTOR_4;
		//write numbeacon
		if(FLASH_ProgramWord(writeAddr,(uint32_t)numBeaconToWrite)!=FLASH_COMPLETE){
		    while(1){}
		}
		writeAddr+= 4;
		for(i=0;i<numBeaconToWrite;i++){
		    //write ID
		    if(FLASH_ProgramWord(writeAddr,(uint32_t)beaconID[i])!=FLASH_COMPLETE){
			while(1){}
		    }
		    writeAddr+= 4;
		    //write coord
		    for(j=0;j<3;j++){
			converter.fl=beaconCoord[i*3+j];
			if(FLASH_ProgramWord(writeAddr,converter.ui)!=FLASH_COMPLETE){
			    while(1){}
			}
			writeAddr+= 4;
		    }
		    converter.fl=offset[i];
		    if(FLASH_ProgramWord(writeAddr,converter.ui)!=FLASH_COMPLETE){
			while(1){}
		    }
		    writeAddr+= 4;
		}
		numBeacon = numBeaconToWrite;
		numBeaconToWrite = 0;
		FLASH_Lock();
	    }
	}
    }//finish infinit loop
}



/** @addtogroup Template_Project
 * @{
 */
void SPI_Printf(uint8_t* array,int len){
    int i;
    for(i=0;i<len;i++){
	while(SPI_I2S_GetFlagStatus(SPI_OUT,SPI_I2S_FLAG_TXE)==RESET);
	SPI_SendData(SPI2,array[i]);

    }
}

PUTCHAR_PROTOTYPE
{
    /* Place your implementation of fputc here */
    /* e.g. write a character to the USART */
    USART_SendData(EVAL_COM1, (uint8_t) ch);

    /* Loop until the end of transmission */
    while (USART_GetFlagStatus(EVAL_COM1, USART_FLAG_TC) == RESET)
    {}

    return ch;
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

void AlgoInit(){
    timeStamp = 0;
    spiInBufLen = 0;
    spiOutBufLen = 0;
    numBeaconToWrite = 0;
    numBeacon = 9;
    int i;
    for(i=0;i<9;i++){
	accGyroOffset[i] = 0;
    }
}

void boardInit(){
    STM_EVAL_LEDInit(LED1);
    STM_EVAL_LEDInit(LED2);
    SPI_In_Config(); // get ranging result from F1
    SPI_Cmd(EVAL_SPI,ENABLE);
    SPI_I2S_ITConfig(EVAL_SPI,SPI_I2S_IT_RXNE,ENABLE);
    SPI_Out_Config();

    /* Enable DMA SPI TX Stream */
    DMA_Cmd(SPI_OUT_TX_DMA_STREAM,ENABLE);

    USART_Config(COM1);
    USART_NVIC_Config(COM1);
    USART_ITConfig(EVAL_COM1,USART_IT_RXNE,ENABLE);
    SPI_Cmd(SPI_OUT,ENABLE);
    while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);// to avoid usart swallow first byte

    if(SysTick_Config(SystemCoreClock/1000)){
	while(1);
    }
#ifdef ENABLE_IMU
    while(timeStamp<5){
    }
    MPU6050_I2C_Init();
    for(int i=0;i<1000000;i++){
      asm("nop");
    }

    if(!MPU6050_TestConnection() ){
      while(1){
        printf("mpu6050 test connection failed!\n\r");
      };//connection failed
    }
    
    MPU6050_Initialize();
#ifdef DEBUG_IMU
    printf("IMU ready\n\r");
#endif
    MPU9150_SetBypass();
    if(!AK8975_TestConnection()){
      while(1){
        printf("mpu6050 test connection failed!\n\r");
      };//connection failed
    }
#ifdef DEBUG_IMU
    printf("AK ready\n\r");
#endif
    //MPU6050_I2C_Enable_exti_int();
#endif

    initTimer();
    STM_EVAL_LEDOn(LED1);
    STM_EVAL_LEDOn(LED2);
    TIM2_Configuration();
}
/**
 * @}
 */



/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
