#ifndef __fusion_hh__
#define __fusion_hh__

#include "arm_math.h"
#include "stdint.h"
#include <stdio.h>




uint8_t initFusion(float initPos[], float att[]);
uint8_t updateByImu(float32_t Imu[]);
uint8_t constructAccGyro(float32_t AccGyro[], float acc[], float pal[]);
uint8_t updateByUsLoc(float32_t Loc[]);
uint8_t constructLoc(float32_t Loc3[], float32_t Loc[],  arm_matrix_instance_f32 x, float att[]);

#endif


    
    
    
    
  