/**
 ******************************************************************************
 * @file    stm324xg_eval.c
 * @author  MCD Application Team
 * @version V1.1.1
 * @date    11-January-2013
 * @brief   This file provides
 *            - set of firmware functions to manage Leds, push-button and COM ports
 *            - low level initialization functions for SD card (on SDIO) and
 *              serial EEPROM (sEE)
 *          available on STM324xG-EVAL evaluation board(MB786) RevB from
 *          STMicroelectronics.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
 *
 * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        http://www.st.com/software_license_agreement_liberty_v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm324xg_eval.h"
#include "stm32f4xx_sdio.h"
#include "stm32f4xx_dma.h"
#include "stm32f4xx_i2c.h"
#include "MPU6050.h"
#include "stdio.h"

extern uint16_t rawUSValue[ADC_VALUE_LEN];
extern __IO uint8_t *USARTInBuf;
static __IO uint32_t uwTimingDelay;
/** @addtogroup Utilities
 * @{
 */

/** @addtogroup STM32_EVAL
 * @{
 */

/** @addtogroup STM324xG_EVAL
 * @{
 */

/** @defgroup STM324xG_EVAL_LOW_LEVEL
 * @brief This file provides firmware functions to manage Leds, push-buttons,
 *        COM ports, SD card on SDIO and serial EEPROM (sEE) available on
 *        STM324xG-EVAL evaluation board from STMicroelectronics.
 * @{
 */

/** @defgroup STM324xG_EVAL_LOW_LEVEL_Private_TypesDefinitions
 * @{
 */
/**
 * @}
 */


/** @defgroup STM324xG_EVAL_LOW_LEVEL_Private_Defines
 * @{
 */
/**
 * @}
 */


/** @defgroup STM324xG_EVAL_LOW_LEVEL_Private_Macros
 * @{
 */
/**
 * @}
 */


/** @defgroup STM324xG_EVAL_LOW_LEVEL_Private_Variables
 * @{
 */
GPIO_TypeDef* GPIO_PORT[LEDn] = {LED1_GPIO_PORT, LED2_GPIO_PORT};
const uint16_t GPIO_PIN[LEDn] = {LED1_PIN, LED2_PIN};
const uint32_t GPIO_CLK[LEDn] = {LED1_GPIO_CLK, LED2_GPIO_CLK};

GPIO_TypeDef* BUTTON_PORT[BUTTONn] = {WAKEUP_BUTTON_GPIO_PORT, TAMPER_BUTTON_GPIO_PORT,
    KEY_BUTTON_GPIO_PORT};

const uint16_t BUTTON_PIN[BUTTONn] = {WAKEUP_BUTTON_PIN, TAMPER_BUTTON_PIN,
    KEY_BUTTON_PIN};

const uint32_t BUTTON_CLK[BUTTONn] = {WAKEUP_BUTTON_GPIO_CLK, TAMPER_BUTTON_GPIO_CLK,
    KEY_BUTTON_GPIO_CLK};

const uint16_t BUTTON_EXTI_LINE[BUTTONn] = {WAKEUP_BUTTON_EXTI_LINE,
    TAMPER_BUTTON_EXTI_LINE,
    KEY_BUTTON_EXTI_LINE};

const uint16_t BUTTON_PORT_SOURCE[BUTTONn] = {WAKEUP_BUTTON_EXTI_PORT_SOURCE,
    TAMPER_BUTTON_EXTI_PORT_SOURCE,
    KEY_BUTTON_EXTI_PORT_SOURCE};

const uint16_t BUTTON_PIN_SOURCE[BUTTONn] = {WAKEUP_BUTTON_EXTI_PIN_SOURCE,
    TAMPER_BUTTON_EXTI_PIN_SOURCE,
    KEY_BUTTON_EXTI_PIN_SOURCE};
const uint16_t BUTTON_IRQn[BUTTONn] = {WAKEUP_BUTTON_EXTI_IRQn, TAMPER_BUTTON_EXTI_IRQn,
    KEY_BUTTON_EXTI_IRQn};

USART_TypeDef* COM_USART[COMn] = {EVAL_COM1,EVAL_COM2};

GPIO_TypeDef* COM_TX_PORT[COMn] = {EVAL_COM1_TX_GPIO_PORT,EVAL_COM2_TX_GPIO_PORT};

GPIO_TypeDef* COM_RX_PORT[COMn] = {EVAL_COM1_RX_GPIO_PORT,EVAL_COM2_RX_GPIO_PORT};

const uint32_t COM_USART_CLK[COMn] = {EVAL_COM1_CLK,EVAL_COM2_CLK};

const uint32_t COM_TX_PORT_CLK[COMn] = {EVAL_COM1_TX_GPIO_CLK,EVAL_COM2_TX_GPIO_CLK};

const uint32_t COM_RX_PORT_CLK[COMn] = {EVAL_COM1_RX_GPIO_CLK,EVAL_COM2_RX_GPIO_CLK};

const uint16_t COM_TX_PIN[COMn] = {EVAL_COM1_TX_PIN,EVAL_COM2_TX_PIN};

const uint16_t COM_RX_PIN[COMn] = {EVAL_COM1_RX_PIN,EVAL_COM2_RX_PIN};

const uint16_t COM_TX_PIN_SOURCE[COMn] = {EVAL_COM1_TX_SOURCE,EVAL_COM2_TX_SOURCE};

const uint16_t COM_RX_PIN_SOURCE[COMn] = {EVAL_COM1_RX_SOURCE,EVAL_COM2_RX_SOURCE};

const uint16_t COM_TX_AF[COMn] = {EVAL_COM1_TX_AF,EVAL_COM2_TX_AF};

const uint16_t COM_RX_AF[COMn] = {EVAL_COM1_RX_AF,EVAL_COM2_RX_AF};

const uint16_t COM_NVIC_Channel[COMn] = {EVAL_COM1_IRQn,EVAL_COM2_IRQn};

DMA_InitTypeDef    sEEDMA_InitStructure;
NVIC_InitTypeDef   NVIC_InitStructure;

/**
 * @}
 */


/** @defgroup STM324xG_EVAL_LOW_LEVEL_Private_FunctionPrototypes
 * @{
 */

/**
 * @}
 */

/** @defgroup STM324xG_EVAL_LOW_LEVEL_Private_Functions
 * @{
 */

/**
 * @brief  Configures LED GPIO.
 * @param  Led: Specifies the Led to be configured.
 *   This parameter can be one of following parameters:
 *     @arg LED1
 *     @arg LED2
 *     @arg LED3
 *     @arg LED4
 * @retval None
 */
void STM_EVAL_LEDInit(Led_TypeDef Led)
{
    GPIO_InitTypeDef  GPIO_InitStructure;

    /* Enable the GPIO_LED Clock */
    RCC_AHB1PeriphClockCmd(GPIO_CLK[Led], ENABLE);


    /* Configure the GPIO_LED pin */
    GPIO_InitStructure.GPIO_Pin = GPIO_PIN[Led];
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIO_PORT[Led], &GPIO_InitStructure);
}

/**
 * @brief  Turns selected LED On.
 * @param  Led: Specifies the Led to be set on.
 *   This parameter can be one of following parameters:
 *     @arg LED1
 *     @arg LED2
 *     @arg LED3
 *     @arg LED4
 * @retval None
 */
void STM_EVAL_LEDOn(Led_TypeDef Led)
{
    GPIO_PORT[Led]->BSRRL = GPIO_PIN[Led];
}

/**
 * @brief  Turns selected LED Off.
 * @param  Led: Specifies the Led to be set off.
 *   This parameter can be one of following parameters:
 *     @arg LED1
 *     @arg LED2
 *     @arg LED3
 *     @arg LED4
 * @retval None
 */
void STM_EVAL_LEDOff(Led_TypeDef Led)
{
    GPIO_PORT[Led]->BSRRH = GPIO_PIN[Led];
}

/**
 * @brief  Toggles the selected LED.
 * @param  Led: Specifies the Led to be toggled.
 *   This parameter can be one of following parameters:
 *     @arg LED1
 *     @arg LED2
 *     @arg LED3
 *     @arg LED4
 * @retval None
 */
void STM_EVAL_LEDToggle(Led_TypeDef Led)
{
    GPIO_PORT[Led]->ODR ^= GPIO_PIN[Led];
}

/**
 * @brief  Configures Button GPIO and EXTI Line.
 * @param  Button: Specifies the Button to be configured.
 *   This parameter can be one of following parameters:
 *     @arg BUTTON_WAKEUP: Wakeup Push Button
 *     @arg BUTTON_TAMPER: Tamper Push Button
 *     @arg BUTTON_KEY: Key Push Button
 *     @arg BUTTON_RIGHT: Joystick Right Push Button
 *     @arg BUTTON_LEFT: Joystick Left Push Button
 *     @arg BUTTON_UP: Joystick Up Push Button
 *     @arg BUTTON_DOWN: Joystick Down Push Button
 *     @arg BUTTON_SEL: Joystick Sel Push Button
 * @param  Button_Mode: Specifies Button mode.
 *   This parameter can be one of following parameters:
 *     @arg BUTTON_MODE_GPIO: Button will be used as simple IO
 *     @arg BUTTON_MODE_EXTI: Button will be connected to EXTI line with interrupt
 *                     generation capability
 * @retval None
 */
void STM_EVAL_PBInit(Button_TypeDef Button, ButtonMode_TypeDef Button_Mode)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    EXTI_InitTypeDef EXTI_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;


    /* Enable the BUTTON Clock */
    RCC_AHB1PeriphClockCmd(BUTTON_CLK[Button], ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    /* Configure Button pin as input */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Pin = BUTTON_PIN[Button];
    GPIO_Init(BUTTON_PORT[Button], &GPIO_InitStructure);


    if (Button_Mode == BUTTON_MODE_EXTI)
    {
	/* Connect Button EXTI Line to Button GPIO Pin */
	SYSCFG_EXTILineConfig(BUTTON_PORT_SOURCE[Button], BUTTON_PIN_SOURCE[Button]);

	/* Configure Button EXTI line */
	EXTI_InitStructure.EXTI_Line = BUTTON_EXTI_LINE[Button];
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;

	if(Button != BUTTON_WAKEUP)
	{
	    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	}
	else
	{
	    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	}
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Enable and set Button EXTI Interrupt to the lowest priority */
	NVIC_InitStructure.NVIC_IRQChannel = BUTTON_IRQn[Button];
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

	NVIC_Init(&NVIC_InitStructure);
    }
}

/**
 * @brief  Returns the selected Button state.
 * @param  Button: Specifies the Button to be checked.
 *   This parameter can be one of following parameters:
 *     @arg BUTTON_WAKEUP: Wakeup Push Button
 *     @arg BUTTON_TAMPER: Tamper Push Button
 *     @arg BUTTON_KEY: Key Push Button
 *     @arg BUTTON_RIGHT: Joystick Right Push Button
 *     @arg BUTTON_LEFT: Joystick Left Push Button
 *     @arg BUTTON_UP: Joystick Up Push Button
 *     @arg BUTTON_DOWN: Joystick Down Push Button
 *     @arg BUTTON_SEL: Joystick Sel Push Button
 * @retval The Button GPIO pin value.
 */
uint32_t STM_EVAL_PBGetState(Button_TypeDef Button)
{
    return GPIO_ReadInputDataBit(BUTTON_PORT[Button], BUTTON_PIN[Button]);
}


/**
 * @brief  Configures COM port.
 * @param  COM: Specifies the COM port to be configured.
 *   This parameter can be one of following parameters:
 *     @arg COM1
 *     @arg COM2
 * @param  USART_InitStruct: pointer to a USART_InitTypeDef structure that
 *   contains the configuration information for the specified USART peripheral.
 * @retval None
 */
void STM_EVAL_COMInit(COM_TypeDef COM, USART_InitTypeDef* USART_InitStruct)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /* Enable GPIO clock */
    RCC_AHB1PeriphClockCmd(COM_TX_PORT_CLK[COM] | COM_RX_PORT_CLK[COM], ENABLE);

    //RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);
    if (COM == COM1)
    {
	/* Enable UART clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);
	// RCC_APB2PeriphClockCmd(COM_USART_CLK[COM], ENABLE);
    }else{
	RCC_APB1PeriphClockCmd(COM_USART_CLK[COM], ENABLE);
    }

    /* Connect PXx to USARTx_Tx*/
    GPIO_PinAFConfig(COM_TX_PORT[COM], COM_TX_PIN_SOURCE[COM], COM_TX_AF[COM]);

    /* Connect PXx to USARTx_Rx*/
    GPIO_PinAFConfig(COM_RX_PORT[COM], COM_RX_PIN_SOURCE[COM], COM_RX_AF[COM]);

    /* Configure USART Tx as alternate function  */
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;

    GPIO_InitStructure.GPIO_Pin = COM_TX_PIN[COM];
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(COM_TX_PORT[COM], &GPIO_InitStructure);

    /* Configure USART Rx as alternate function  */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Pin = COM_RX_PIN[COM];
    GPIO_Init(COM_RX_PORT[COM], &GPIO_InitStructure);

    /* USART configuration */
    USART_Init(COM_USART[COM], USART_InitStruct);

    /* Enable USART */
    USART_Cmd(COM_USART[COM], ENABLE);
}


uint16_t PrescalerValue = 0;
uint16_t CCR1_Val = 333;
void initTimer(){

    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    TIM_OCInitTypeDef       TIM_OCInitStructure;
    TIM_TimeBaseStructure.TIM_Period = 65535;
    TIM_TimeBaseStructure.TIM_Prescaler = 0;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

    TIM_TimeBaseInit(TIM3,&TIM_TimeBaseStructure);
    TIM_PrescalerConfig(TIM3, PrescalerValue, TIM_PSCReloadMode_Immediate);

    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Timing;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = CCR1_Val;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

    TIM_OC1Init(TIM3, &TIM_OCInitStructure);

    TIM_OC1PreloadConfig(TIM3,TIM_OCPreload_Disable);
    TIM_ITConfig(TIM3,TIM_IT_CC1,ENABLE);
    TIM_Cmd(TIM3,ENABLE);

    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
}



/**
 * @brief  Configures the nested vectored interrupt controller.
 * @param  None
 * @retval None
 */
void USART_NVIC_Config(COM_TypeDef COMx){
    NVIC_InitTypeDef NVIC_InitStructure;

    /* Enable the USARTx Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = COM_NVIC_Channel[COMx];
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/**
 * @brief  Configures the USART Peripheral.
 * @param  None
 * @retval None
 */
void USART_Config(COM_TypeDef COMx){
    USART_InitTypeDef USART_InitStructure;
    USART_InitStructure.USART_BaudRate = 115200;
    //USART_InitStructure.USART_BaudRate = 230400;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    STM_EVAL_COMInit(COMx, &USART_InitStructure);
    USART_TypeDef * USARTx = (COMx == COM1)?USART1:USART2;
    USART_GetFlagStatus(USARTx, USART_FLAG_TC);
}

//function : send str
void USART1_Puts(const char * str){
    while(*str){
	USART_SendData(USART1, *str++);
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
    }
}

void USART1_Putsn(const char * str,uint8_t n){

    for(int i=0;i<n;i++){
	USART_SendData(USART1, str[i]);
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
    }
}
void USART2_Puts(char * str){
    while(*str){
	USART_SendData(USART2, *str++);
	while(USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
    }
}
void SPI2_Putsn(const char * str,uint8_t n){
    for(int i=0;i<n;i++){
	SPI_I2S_SendData(SPI2, str[i]);
	while(SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET);
    }
}

/**
 * @brief   Print program
 * @param  None
 * @retval None
 */
void print(const char* output){
#ifdef __IAR_SYSTEMS_ICC__
    USART1_Puts(output);
#else
    printf("%s",output);
#endif
}

void usADCInit(void)
{
    ADC_InitTypeDef       ADC_InitStructure;
    ADC_CommonInitTypeDef ADC_CommonInitStructure;
    DMA_InitTypeDef       DMA_InitStructure;
    GPIO_InitTypeDef      GPIO_InitStructure;

    /* Turn on clk of adc dma and gpio****************************************/
    RCC_AHB1PeriphClockCmd(US_ADC_DMA_CLK | US_ADC_GPIO_CLK, ENABLE);
    RCC_APB2PeriphClockCmd(US_ADC_CLK, ENABLE);

    /* DMA2 Stream0 channel2 **************************************/
    DMA_InitStructure.DMA_Channel = US_ADC_DMA_Channel;
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)ADC3_DR_ADDRESS;
    DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)rawUSValue;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
    DMA_InitStructure.DMA_BufferSize = 1000;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
    DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
    DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
    DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
    DMA_Init(DMA2_Stream0, &DMA_InitStructure);
    DMA_Cmd(DMA2_Stream0, ENABLE);

    DMA_ITConfig(DMA2_Stream0,DMA_IT_HT,ENABLE);
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    /* ÅäÖÃADC3Í¨µÀ7Òý½ÅÎªÄ£ÄâÊäÈëÄ£Ê½******************************/
    GPIO_InitStructure.GPIO_Pin = US_ADC_GPIO_Pin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
    GPIO_Init(US_ADC_GPIO_Port, &GPIO_InitStructure);

    /****************************************************************************
      PCLK2 = HCLK / 2
      ADCCLK = PCLK2 /2 = HCLK / 8 = 168 / 8 = 10.5M
      ADC Sampling Time + Conversion Time = 56 + 12 cycles = 68
      Conversion Time = 42MHz / 68cyc = 154.4khz.
     *****************************************************************************/
    ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
    ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div8;
    ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
    ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
    ADC_CommonInit(&ADC_CommonInitStructure);

    /* US_ADC ³õÊ¼»¯ ****************************************************************/
    ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
    ADC_InitStructure.ADC_ScanConvMode = DISABLE;
    ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
    ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_NbrOfConversion = 1;
    ADC_Init(US_ADC, &ADC_InitStructure);

    /* US_ADC ¹æÔò channel1 ÅäÖÃ *************************************/
    ADC_RegularChannelConfig(US_ADC, US_ADC_Channel, 1, ADC_SampleTime_28Cycles);

    /* Ê¹ÄÜDMAÇëÇó(µ¥ADCÄ£Ê½) */
    ADC_DMARequestAfterLastTransferCmd(US_ADC, ENABLE);

    /* Ê¹ÄÜ US_ADC DMA */
    ADC_DMACmd(US_ADC, ENABLE);

    /* Ê¹ÄÜ US_ADC */
    ADC_Cmd(US_ADC, ENABLE);

    /* Èí¼þÆô¶¯ADC×ª»» */
    ADC_SoftwareStartConv(US_ADC);
}

void SPI_In_Config(void){
    GPIO_InitTypeDef GPIO_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    /* Peripheral Clock Enable -------------------------------------------------*/
    /* Enable the SPI clock */
    EVAL_SPI_CLK_INIT(EVAL_SPI_CLK, ENABLE);

    /* Enable GPIO clocks */
    RCC_AHB1PeriphClockCmd(EVAL_SPI_SCK_GPIO_CLK | EVAL_SPI_MISO_GPIO_CLK | EVAL_SPI_MOSI_GPIO_CLK, ENABLE);

    /* SPI GPIO Configuration --------------------------------------------------*/
    /* GPIO Deinitialisation */
    /*
       GPIO_DeInit(EVAL_SPI_SCK_GPIO_PORT);
       GPIO_DeInit(EVAL_SPI_MISO_GPIO_PORT);
       GPIO_DeInit(EVAL_SPI_MOSI_GPIO_PORT);
       */

    /* Connect SPI pins to AF5 */
    GPIO_PinAFConfig(EVAL_SPI_SCK_GPIO_PORT, EVAL_SPI_SCK_SOURCE, EVAL_SPI_SCK_AF);
    GPIO_PinAFConfig(EVAL_SPI_MISO_GPIO_PORT, EVAL_SPI_MISO_SOURCE, EVAL_SPI_MISO_AF);
    GPIO_PinAFConfig(EVAL_SPI_NSS_GPIO_PORT, EVAL_SPI_MOSI_SOURCE, EVAL_SPI_MOSI_AF);
    // GPIO_PinAFConfig(EVAL_SPI_NSS, EVAL_SPI_MOSI_SOURCE, EVAL_SPI_MOSI_AF);

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;

    /* SPI SCK pin configuration */
    GPIO_InitStructure.GPIO_Pin = EVAL_SPI_SCK_PIN;
    GPIO_Init(EVAL_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

    /* SPI  MISO pin configuration */
    GPIO_InitStructure.GPIO_Pin =  EVAL_SPI_MISO_PIN;
    GPIO_Init(EVAL_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

    /* SPI  MOSI pin configuration */
    GPIO_InitStructure.GPIO_Pin =  EVAL_SPI_MOSI_PIN;
    GPIO_Init(EVAL_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

    /* SPI configuration -------------------------------------------------------*/
    //SPI_I2S_DeInit(EVAL_SPI);
    SPI_InitTypeDef  SPI_InitStructure;
    SPI_InitStructure.SPI_Direction = SPI_Direction_1Line_Rx;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial = 7;
    SPI_InitStructure.SPI_Mode = SPI_Mode_Slave;

    SPI_Init(EVAL_SPI,&SPI_InitStructure);

    /* Configure the Priority Group to 1 bit */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

    /* Configure the SPI interrupt priority */
    NVIC_InitStructure.NVIC_IRQChannel = EVAL_SPI_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

void SPI2_Init(void)
{  
    GPIO_InitTypeDef GPIO_InitStructure;
    SPI_InitTypeDef SPI_InitStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);
    //RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    /* Connect SPI pins to AF5 */
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_SPI2);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_SPI2);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_SPI2);
    /* Configure SPI2 pins: SCK-PB13 and MOSI-PB15 ---------------------------------*/
    /* Configure SCK and MOSI pins as Alternate Function Push Pull */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    //GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    /* SPI2 master configuration ------------------------------------------------*/
    SPI_InitStructure.SPI_Direction = SPI_Direction_1Line_Tx;
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial = 7;
    SPI_Init(SPI2, &SPI_InitStructure);

    /* Configure the Priority Group to 1 bit */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

    /* Configure the SPI interrupt priority */
    NVIC_InitStructure.NVIC_IRQChannel = SPI2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    SPI_Cmd(SPI2, ENABLE);
}


void TIM2_Configuration()
{
    /* Enable TIM2 clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

    NVIC_InitTypeDef NVIC_InitStructure;  
    /* Configure one bit for preemption priority */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
    /* Enable the EXTI9_5 Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 4;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    //���½�Timer����Ϊȱʡֵ
    TIM_DeInit(TIM2);
    //�����ڲ�ʱ�Ӹ�TIM2�ṩʱ��Դ
    TIM_InternalClockConfig(TIM2);
    //Ԥ��Ƶϵ��Ϊ36000-1������������ʱ��Ϊ168MHz/16800 = 10kHz
    TIM_TimeBaseStructure.TIM_Prescaler = 16800 - 1;
    //����ʱ�ӷָ�
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    //���ü�����ģʽΪ���ϼ���ģʽ
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    //���ü��������С��ÿ��25�����Ͳ���һ�������¼�
    TIM_TimeBaseStructure.TIM_Period = 25;
    //������Ӧ�õ�TIM2��
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
    //�������жϱ�־
    TIM_ClearFlag(TIM2, TIM_FLAG_Update);
    //��ֹARRԤװ�ػ�����
    TIM_ARRPreloadConfig(TIM2, DISABLE);  //Ԥװ�ؼĴ��������ݱ��������͵�Ӱ�ӼĴ��� 
    //����TIM2���ж�
    TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);

    TIM_Cmd(TIM2, ENABLE);	//������ʱ��2
}

void rawPrint(uint8_t id, float x,float y,float z,float vx,float vy,float vz,float euler_x,float euler_y,float euler_z){
    union{
#pragma pack(1)
	struct {
	    uint8_t id;
	    float x;
	    float y;
	    float z;
	    float vx;
	    float vy;
	    float vz;
	    float euler_x;
	    float euler_y;
	    float euler_z;
	} a;
#pragma pack()
	struct {
	    uint8_t data[37];
	} b;
    } shared;
    shared.a.id = id;
    shared.a.x = x;
    shared.a.y = y;
    shared.a.z = z;
    shared.a.vx = vx;
    shared.a.vy = vy;
    shared.a.vz = vz;
    shared.a.euler_x = euler_x;
    shared.a.euler_y = euler_y;
    shared.a.euler_z = euler_z;
    uint8_t outBuf[55];
    uint8_t inIdx = 0,outIdx = 0;
    outBuf[outIdx++] = 0x7E;
    for(inIdx=0;inIdx<37;inIdx++){
	if(shared.b.data[inIdx]==0x7E){
	    outBuf[outIdx++] = 0x7D;
	    outBuf[outIdx++] = 0x5E;

	}else if(shared.b.data[inIdx]==0x7D){
	    outBuf[outIdx++] = 0x7D;
	    outBuf[outIdx++] = 0x5D;

	}else{
	    outBuf[outIdx++] = shared.b.data[inIdx];

	}
    }
    outBuf[outIdx++] = 0x7E;
#ifdef __IAR_SYSTEMS_ICC__
    //printf("%s",outBuf);
    USART1_Putsn((char*)outBuf,outIdx);
    SPI2_Putsn((char*)outBuf,outIdx);
#else
    write(stdout,outBuf,outIdx);
#endif
}

void InitUSART1DMA(){
    DMA_InitTypeDef DMA_InitStructure;  
    DMA_InitStructure.DMA_Channel = DMA_Channel_4;
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&(USART1->DR));
    DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)USARTInBuf;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
    DMA_InitStructure.DMA_BufferSize = (uint32_t)MAX_USART_BUF_LEN;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
    DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
    DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
    DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
    DMA_Init(DMA2_Stream2, &DMA_InitStructure);
}

/**
 * @brief  Inserts a delay time.
 * @param  nTime: specifies the delay time length, in milliseconds.
 * @retval None
 */
void Delay(__IO uint32_t nTime) {
    uwTimingDelay = nTime;
    while(uwTimingDelay != 0);
}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void){
    if (uwTimingDelay != 0x00){
	uwTimingDelay--;
    }
}
void usDelay(){
    int loop;
    for(loop=0;loop<0xffff;loop++){
	uint16_t loop2;
	for(loop2=0;loop2<0xf;loop2++){
	    asm("nop");
	}
    }
}

int FlashReadConfig(uint8_t* numBeacon,uint8_t *beaconID,float *beaconCoord,float *offset){
    int cnt = 0;
    uint32_t ReadAddr = ADDR_FLASH_SECTOR_4;
    *numBeacon = (uint8_t)(*(volatile uint32_t*)ReadAddr);
    ReadAddr += 4;
    cnt += 4;
    floatInt converter;
    if(*numBeacon!=255){
	uint8_t i,j;
	for(i=0;i<*numBeacon;i++){
	    beaconID[i] = (uint8_t)(*(volatile uint32_t*)ReadAddr);
	    ReadAddr += 4;
	    cnt += 4;
	    for(j=0;j<3;j++){
		converter.ui=*(volatile uint32_t*)ReadAddr;
		beaconCoord[i*3+j] = converter.fl;
		ReadAddr += 4;
		cnt += 4;
	    }
	    converter.ui = *(volatile uint32_t*)ReadAddr;
	    offset[i]=converter.fl;
	    ReadAddr += 4;
	    cnt += 4;
	}
    }else{
	*numBeacon = 0;
    }
    return cnt;
}
uint8_t I2C_Rx_Buffer[14];

/*
typedef struct
{
  uint32_t DMA_Channel;           
  uint32_t DMA_PeripheralBaseAddr;
  uint32_t DMA_Memory0BaseAddr;   
  uint32_t DMA_DIR;               
  uint32_t DMA_BufferSize;        
  uint32_t DMA_PeripheralInc;     
  uint32_t DMA_MemoryInc;         
  uint32_t DMA_PeripheralDataSize;
  uint32_t DMA_MemoryDataSize;    
  uint32_t DMA_Mode;              
  uint32_t DMA_Priority;          
  uint32_t DMA_FIFOMode;          
  uint32_t DMA_FIFOThreshold;     
  uint32_t DMA_MemoryBurst;       
  uint32_t DMA_PeripheralBurst;   
}DMA_InitTypeDef;                 
*/




#define I2C_DR_Address 0x40005410
#define MPU6050_DMA_Channel DMA1_Stream0
void MPU6050_DMA_Config(){
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
    NVIC_InitTypeDef NVIC_InitStructure;
    DMA_InitTypeDef  DMA_InitStructure;
    //DMA_DeInit(MPU6050_DMA_Channel); //reset DMA1 channe1 to default values;
    DMA_InitStructure.DMA_Channel = DMA_Channel_1;
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)I2C_DR_Address; //=0x40005410 : address of data reading register of I2C1
    DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)I2C_Rx_Buffer; //variable to store data
    //DMA_InitStructure.DMA_M2M = DMA_M2M_Disable; //channel will be used for peripheral to memory transfer
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;	//setting normal mode (non circular)
    DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;	//medium priority
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;	//Location assigned to peripheral register will be source
    DMA_InitStructure.DMA_BufferSize = 14;	//number of data to be transfered
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; //automatic memory increment disable for peripheral
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;	//automatic memory increment enable for memory
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;	//source peripheral data size = 8bit
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;	//destination memory data size = 8bit
    DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
    DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
    DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
    DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
    DMA_Init(MPU6050_DMA_Channel, &DMA_InitStructure);
    DMA_ITConfig(MPU6050_DMA_Channel, DMA_IT_TC, ENABLE);

    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Stream0_IRQn; //I2C1 connect to channel 7 of DMA1
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x05;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x05;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}



#define  BUFFERSIZE 120 //120
uint8_t SpiOutBuf[BUFFERSIZE];
extern uint8_t *spiOutBuf;
void SPI_Out_Config(void){
  GPIO_InitTypeDef GPIO_InitStructure;
  SPI_InitTypeDef  SPI_InitStructure;
  DMA_InitTypeDef DMA_InitStructure;

  /* Peripheral Clock Enable -------------------------------------------------*/
  /* Enable the SPI clock */
  SPI_OUT_CLK_INIT(SPI_OUT_CLK, ENABLE);
  
  /* Enable GPIO clocks */
  RCC_AHB1PeriphClockCmd(SPI_OUT_SCK_GPIO_CLK | SPI_OUT_MISO_GPIO_CLK | SPI_OUT_MOSI_GPIO_CLK, ENABLE);
  
  /* Enable DMA clock */
  RCC_AHB1PeriphClockCmd(SPI_OUT_DMA_CLK, ENABLE);

  /* SPI GPIO Configuration --------------------------------------------------*/
  /* GPIO Deinitialisation */
  //GPIO_DeInit(SPI_OUT_SCK_GPIO_PORT);
  //GPIO_DeInit(SPI_OUT_MISO_GPIO_PORT);
  //GPIO_DeInit(SPI_OUT_MOSI_GPIO_PORT);
  
  /* Connect SPI pins to AF5 */  
  GPIO_PinAFConfig(SPI_OUT_SCK_GPIO_PORT, SPI_OUT_SCK_SOURCE, SPI_OUT_SCK_AF);
  GPIO_PinAFConfig(SPI_OUT_MISO_GPIO_PORT, SPI_OUT_MISO_SOURCE, SPI_OUT_MISO_AF);    
  GPIO_PinAFConfig(SPI_OUT_MOSI_GPIO_PORT, SPI_OUT_MOSI_SOURCE, SPI_OUT_MOSI_AF);

  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;

  /* SPI SCK pin configuration */
  GPIO_InitStructure.GPIO_Pin = SPI_OUT_SCK_PIN;
  GPIO_Init(SPI_OUT_SCK_GPIO_PORT, &GPIO_InitStructure);
  
  /* SPI  MISO pin configuration */
  GPIO_InitStructure.GPIO_Pin =  SPI_OUT_MISO_PIN;
  GPIO_Init(SPI_OUT_MISO_GPIO_PORT, &GPIO_InitStructure);  

  /* SPI  MOSI pin configuration */
  GPIO_InitStructure.GPIO_Pin =  SPI_OUT_MOSI_PIN;
  GPIO_Init(SPI_OUT_MOSI_GPIO_PORT, &GPIO_InitStructure);
 
  /* SPI configuration -------------------------------------------------------*/
  SPI_I2S_DeInit(SPI_OUT);
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_Init(SPI_OUT, &SPI_InitStructure);


  
  /* DMA configuration -------------------------------------------------------*/
  /* Deinitialize DMA Streams */
  DMA_DeInit(SPI_OUT_TX_DMA_STREAM);
  
  /* Configure DMA Initialization Structure */
  DMA_InitStructure.DMA_BufferSize = BUFFERSIZE ;
  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable ;
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull ;
  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single ;
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
  DMA_InitStructure.DMA_PeripheralBaseAddr =(uint32_t) (&(SPI_OUT->DR)) ;
  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  /* Configure TX DMA */
  DMA_InitStructure.DMA_Channel = SPI_OUT_TX_DMA_CHANNEL ;
  DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral ;
  DMA_InitStructure.DMA_Memory0BaseAddr =(uint32_t)SpiOutBuf; //SpiOutBuf
  DMA_Init(SPI_OUT_TX_DMA_STREAM, &DMA_InitStructure);
  DMA_Cmd(SPI_OUT_TX_DMA_STREAM,ENABLE);
  DMA_ITConfig(SPI_OUT_TX_DMA_STREAM,DMA_IT_TC,ENABLE);
  /* Configure RX DMA */

  NVIC_InitStructure.NVIC_IRQChannel = DMA1_Stream4_IRQn; //I2C1 connect to channel 7 of DMA1
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x05;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x05;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

/**
 * @}
 */


/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
